package com.pima.apps.ipima.v3.client;

import java.util.Locale;

import com.pima.apps.ipima.v3.network.PimaProtocol;
import com.pima.apps.ipima.v3.views.PanelSystem;


public class SpaClientHelper {

	public String serverUrl = null;
	public int retriesNumber = -1;
	public long retriesIntervalInSec = -1;
	
	public String masterCodeInd = null;
	public String sessionToken 	= null;
	public String systemId 		= null;

	public PanelSystem connectedSystem = null;
	
//	public String panelVersion=null;
	
	
	public boolean isBypassZonesFragment;
//	public boolean refreshZonesName=false;
//	private SpaClient mSpaClient;
	
	public String getLocalization() {
		return PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage());
	}

	
	public SpaClientHelper() {

	}
	

	

}
