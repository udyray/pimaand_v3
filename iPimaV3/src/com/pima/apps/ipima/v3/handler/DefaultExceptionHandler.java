package com.pima.apps.ipima.v3.handler;

import java.lang.Thread.UncaughtExceptionHandler;

public class DefaultExceptionHandler implements UncaughtExceptionHandler {

    
    // constructor
    public DefaultExceptionHandler()
    {
    	
    }
     
    // Default exception handler
    public void uncaughtException(Thread t, Throwable e) {
    	System.out.println("Uncaught Exception!");
    	e.printStackTrace();
    }
}