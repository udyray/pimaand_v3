package com.pima.apps.ipima.v3.handler;


public interface SpaHandlerCallbacks {
	public void closeConnection();
	public void onError(String error);
	public void onTimeout();
}
