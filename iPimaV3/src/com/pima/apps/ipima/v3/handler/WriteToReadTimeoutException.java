package com.pima.apps.ipima.v3.handler;

import org.jboss.netty.handler.timeout.TimeoutException;

public class WriteToReadTimeoutException extends TimeoutException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1173414169015770415L;

	/**
	 * Creates a new instance.
	 */
	public WriteToReadTimeoutException() {
	}
	
	/**
	 * Creates a new instance.
	 */
	public WriteToReadTimeoutException(String message, Throwable cause) {
	    super(message, cause);
	}
	
	/**
	 * Creates a new instance.
	 */
	public WriteToReadTimeoutException(String message) {
	    super(message);
	}
	
	/**
	 * Creates a new instance.
	 */
	public WriteToReadTimeoutException(Throwable cause) {
	    super(cause);
	}
		
}
