/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pima.apps.ipima.v3.gcm;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.SpaPreference;
import com.pima.apps.ipima.v3.network.HttpResponseHandler;
import com.pima.apps.ipima.v3.network.IntentLauncher;


public class RegistrationIntentService extends IntentService implements HttpResponseHandler {

	public static String EXTRAS_ACTION 						= "EXTRAS_ACTION";
	
	public static final int ACTION_REGISTER 				= 100;
	public static final int ACTION_UNREGISTER 				= 101;
	
    private static final String TAG 						= "RegIntentService";
//    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        
    	int action = intent.getIntExtra(EXTRAS_ACTION, -1);
    	InstanceID instanceID = InstanceID.getInstance(this);
        

	    	switch (action) {
				case ACTION_REGISTER:
					try 
					{
						// [START register_for_gcm]
				         // Initially this call goes out to the network to retrieve the token, subsequent calls
				         // are local.
				         // [START get_token]
						 String token = instanceID.getToken(	getString(R.string.gcm_defaultSenderId),
																GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
						// [END get_token]
				        
				         
					 
						 Log.i(TAG, "GCM Registration Token: " + token);
					
						 SpaPreference.setGCMToken(this, token);
						 
						 sendRegistrationToServer(token);
						 
						 boolean isFirstRun = true;
						 File firstRun = new File(getCacheDir(), "first_run_flag");
						 if(firstRun.exists() && firstRun.isFile())
						 {
							 isFirstRun = false;
						 }
						
						 if(isFirstRun)
						 {
							//set notifications on
							SpaPreference.setEnableNotifications(getApplicationContext(), true);
							
							//write first run flag
							FileOutputStream outputStream = new FileOutputStream(firstRun);
							outputStream.write("1".getBytes());
							outputStream.close();
						 }
					
						 // Subscribe to topic channels
						 //subscribeTopics(token);
						 
						 // [END register_for_gcm]
					} catch (Exception e) {
			            Log.d(TAG, "Failed to complete token refresh", e);
			            // If an exception happens while fetching the new token or updating our registration data
			            // on a third-party server, this ensures that we'll attempt the update at a later time.
			            SpaPreference.setSentTokenToServer(this, false);
			        }

					break;
					
				case ACTION_UNREGISTER:
					try
					{
						instanceID.deleteInstanceID();
						sendUnRegistrationToServer();
					}
					catch (Exception e) {
			            e.printStackTrace();
			        }
					break;
		
				default:
					break;
			}
       	
//        // Notify UI that registration has completed, so the progress indicator can be hidden.
//        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    	IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_REGISTER_FOR_NOTIFICATION, (SpaApplication)getApplication(), this);
		il.setDataParam(token);
		il.start();
    }
    
    /**
     * 
     */
    private void sendUnRegistrationToServer() {
        // Add custom implementation, as needed.
    	IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS, (SpaApplication)getApplication(), this);
    	Map<String, Object> data = new HashMap<String, Object>();
		data.put("systemId", ((SpaApplication)getApplication()).getConnectedSystem().getSystemId());
		JSONArray ids = new JSONArray();
		data.put("MessagesType", ids);
		il.setDataParams(data);
		il.start();
    }

    
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) throws Exception {

		if(pToken == IntentLauncher.TOKEN_REGISTER_FOR_NOTIFICATION)
		{
			// You should store a boolean that indicates whether the generated token has been
	        // sent to your server. If the boolean is false, send the token to your server,
	        // otherwise your server should have already received the token.
	        SpaPreference.setSentTokenToServer(this, true);
		}
		else if(pToken == IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS)
		{
			SpaPreference.setSentTokenToServer(this, false);
			SpaPreference.setGCMToken(this, null);
		}
		else
		{
			handleUncaughtHttpError(pToken, res, pIsBackground);
		}
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		System.out.println("handleUncaughtHttpError: " + pToken);
		System.out.println(res);
	}
	
	@Override
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground) {
		System.out.println("handleCaughtHttpError: " + pToken);
		System.out.println(res);
	}

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
//    private void subscribeTopics(String token) throws IOException {
//        GcmPubSub pubSub = GcmPubSub.getInstance(this);
//        for (String topic : TOPICS) {
//            pubSub.subscribe(token, "/topics/" + topic, null);
//        }
//    }
    // [END subscribe_topics]

}
