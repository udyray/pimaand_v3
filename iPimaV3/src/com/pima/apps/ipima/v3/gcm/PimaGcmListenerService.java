/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pima.apps.ipima.v3.gcm;

import java.util.Iterator;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.NewLoginActivity;
import com.pima.apps.ipima.v3.activity.SplashActivity;

public class PimaGcmListenerService extends GcmListenerService {

    private static final String TAG = "PimaGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
    	Iterator<String> iter = data.keySet().iterator();
    	while(iter.hasNext())
    	{
    		String key = iter.next();
    		try{
    			System.out.println(key + ": " + data.get(key) );
    		}
    		catch(Exception e){}
    		
    	}
    	String message = data.getString("message");
    	String systemId =  data.getString("systemId");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(message, systemId);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message, String systemId) {
    	String[] msg = message.split("\n");
    	String header = "";
    	String txt = "";
    	header = msg[0];
    	if(msg.length>1)
    	{
    		for(int i=1; i<msg.length; i++)
    		{
    			txt += ((SpaApplication)getApplication()).getLocalizedString(msg[i]) + "\n";
    		}
    	}
    	
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra(NewLoginActivity.EXTRAS_SYSTEM_ID, systemId);
        
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 
        														0 /* Request code */, 
        														intent,
        														PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
											                .setSmallIcon(R.drawable.ic_notification)
											                .setContentTitle(header)
											                .setContentText(txt)
											                .setAutoCancel(true)
											                .setSound(defaultSoundUri)
											                .setContentIntent(pendingIntent);
        
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(header);
        if(msg.length>1)
    	{
    		for(int i=1; i<msg.length; i++)
    		{
    			 inboxStyle.addLine(((SpaApplication)getApplication()).getLocalizedString(msg[i]));
    		}
    	}
        
        notificationBuilder.setStyle(inboxStyle);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}