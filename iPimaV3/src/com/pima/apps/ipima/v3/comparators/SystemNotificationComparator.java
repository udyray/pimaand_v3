package com.pima.apps.ipima.v3.comparators;

import java.util.Comparator;

import com.pima.apps.ipima.v3.views.SystemNotification;

public class SystemNotificationComparator implements java.io.Serializable ,Comparator<SystemNotification> 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5113953963609404018L;

	@Override
	public int compare(SystemNotification item0, SystemNotification item1) {

		if(null == item0)
		{
			if(null == item1)
			{
				return 0;
			}
			return 1;
		}
		if(null == item1)
		{
			return -1;
		}
		//Descent
		return item1.getDate().compareTo(item0.getDate());
	
	}

}
