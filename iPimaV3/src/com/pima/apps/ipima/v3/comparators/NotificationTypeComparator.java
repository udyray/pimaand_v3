package com.pima.apps.ipima.v3.comparators;

import java.util.Comparator;

import com.pima.apps.ipima.v3.views.NotificationType;

public class NotificationTypeComparator implements java.io.Serializable ,Comparator<NotificationType> 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5113953963609404018L;

	@Override
	public int compare(NotificationType item0, NotificationType item1) {

		if(null == item0)
		{
			if(null == item1)
			{
				return 0;
			}
			return 1;
		}
		if(null == item1)
		{
			return -1;
		}
		//Descent
		return Double.compare(item0.getNumber(), item1.getNumber()) ;
	
	}

}
