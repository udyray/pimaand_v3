package com.pima.apps.ipima.v3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONObject;

import com.pima.apps.ipima.v3.handler.DefaultExceptionHandler;
import com.pima.apps.ipima.v3.network.PimaProtocol;
import com.pima.apps.ipima.v3.views.PanelSystem;

import android.app.Application;

public class SpaApplication extends Application {

	private Map<String, String> mCurrLanguage;
//	private SpaClientHelper mSpaClientHelper;
//	private HandlerThread mSpaClientHanderThread;
	
//	public String serverUrl = null;
	public int retriesNumber = -1;
	public long retriesIntervalInSec = -1;
	
	public String masterCodeInd = null;
	public String sessionToken 	= null;
//	public String systemId 		= null;

	private PanelSystem connectedSystem = null;
	
//	public String panelVersion=null;
	
	private List<Integer> mSentRequests = null;
	
	public boolean isBypassZonesFragment;
	
	public boolean disconnectLock;
	
	public static enum CUR_ACTION {
		LOGOUT,
		CANCEL_LOGIN,
		NONE
	}
	
	public CUR_ACTION action = CUR_ACTION.NONE;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
//		mSpaClientHanderThread = new HandlerThread("SpaClient");
//        mSpaClientHanderThread.start();
//        mSpaClientHelper = new SpaClientHelper();//mSpaClientHanderThread.getLooper());
        mCurrLanguage = new HashMap<String, String>();
        mSentRequests = new ArrayList<Integer>();
        
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler());
	}
	
//	public SpaClientHelper getSpaClientHelper() {
//		return mSpaClientHelper;
//	}
	
	public void addSentRequest(Integer mRequest)
	{	
//		System.out.println("Add " + mRequest);
		if(null == mSentRequests)
		{
//			System.out.println("Creating new requests list");
			mSentRequests = new ArrayList<Integer>();
		}
		
		if(!mSentRequests.contains(mRequest))
		{
			mSentRequests.add(mRequest);
		}
//		else
//		{
//			System.out.println("Request " + mRequest + " already in list");
//		}
	}
	
	public void removeAllRequests()
	{
//		System.out.println("Clear all requests");
		if(null != mSentRequests)
		{
			mSentRequests.clear();
		}
	}
	
	public void removeSentRequest(Integer mRequest)
	{
		//System.out.println("Remove: " + mRequest);
		if(null != mSentRequests)
		{
			if(mSentRequests.contains(mRequest))
			{
				mSentRequests.remove(mRequest);
			}
//			else
//			{
//				System.out.println("Request " + mRequest + " not in list");
//			}
		}
	}
	
//	public String getSentRequests()
//	{
//		String s="";
//		if(null != mSentRequests)
//		{
//			for(int i=0; i<mSentRequests.size(); i++)
//			{
//				s+=mSentRequests.get(i);
//				if(i<mSentRequests.size()-1)
//				{
//					s+=", ";
//				}
//			}
//		}
//		return s;
//	}
	
	
	public int getNumberOfSentRequests()
	{
		if(null != mSentRequests)
		{
			return mSentRequests.size();
		}
		return 0;
	}
	
	public String getLocalizedString(String key) {
		
		if(null == key)
		{
			return "";
		}
		
		if(null != mCurrLanguage)
		{
			if(null != mCurrLanguage.get(key))
			{
				return mCurrLanguage.get(key);
			}
		}
		
		System.out.println("LABEL NOT FOUND " + key); 
		
		if(getLocalization() != null && getLocalization().equals("en")) {
			int identifier = getResources().getIdentifier(key+"_en", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("he")) {
			int identifier = getResources().getIdentifier(key+"_he", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("iw")) {
			int identifier = getResources().getIdentifier(key+"_he", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("ru")) {
			int identifier = getResources().getIdentifier(key+"_ru", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("it")) {
			int identifier = getResources().getIdentifier(key+"_it", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("es")) {
			int identifier = getResources().getIdentifier(key+"_es", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("pt")) {
			int identifier = getResources().getIdentifier(key+"_pt", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}else if(getLocalization() != null && getLocalization().equals("tr")) {
			int identifier = getResources().getIdentifier(key+"_tr", "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}
		else {
			//use device localization by default
			int identifier = getResources().getIdentifier(key, "string", "com.pima.apps.ipima.v3");
			if(identifier != 0)
				return getResources().getString(identifier);
		}
		
		int identifier = getResources().getIdentifier(key, "string", "com.pima.apps.ipima.v3");
		if(identifier != 0)
			return getResources().getString(identifier);
		
		return key;
	}
	
	
	/**
	 * 
	 * @param pLangJson
	 */
	public void setCurrLanguage(JSONObject pLangJson)
	{
		mCurrLanguage = new HashMap<String, String>();
		Iterator<String> iter = pLangJson.keys();
		while(iter.hasNext())
		{
			String key = iter.next();
			String value = pLangJson.optString(key);
			
			mCurrLanguage.put(key.trim(), value.trim());
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Map<String, String> getCurrLanguage()
	{
		return mCurrLanguage;
	}
	
	
	
//	public boolean refreshZonesName=false;
//	private SpaClient mSpaClient;
	
	public String getLocalization() {
		return PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage());
	}

	
	public String getServerUrl() {
		return SpaPreference.getServerUrl(getApplicationContext());
	}
	
	public void setServerUrl(String pServerUrl) {
		SpaPreference.setServerUrl(getApplicationContext(), pServerUrl);
	}
	
	
	/**
	 * 
	 * @return
	 */
//	public SparseArray<ZoneName> getAllZonesNames()
//	{
//		DbDataHelper db = new DbDataHelper(getApplicationContext());
//		db.open();
//		SparseArray<ZoneName> zoneNames = db.selectZoneNames(getSpaClientHelper().systemId, null, null);
//		db.close();
//		
//		String zoneLabel = getLocalizedString("label_zone");
//		for(int i=0; i<getSpaClientHelper().numOfZonesLatest; i++)
//		{
//			int zoneId = i+1;
//			ZoneName zn = zoneNames.get(zoneId);
//			if(null == zn)
//			{
//				zn = new ZoneName(zoneId, getSpaClientHelper().systemId, zoneId, zoneLabel + " " + zoneId);
//				zoneNames.put(zoneId, zn);
//			}
//		}
//		
//		return zoneNames;
//	}
	
	public PanelSystem getConnectedSystem()
	{
		if(null != connectedSystem)
		{
			return connectedSystem;
		}
		
//		connectedSystem = readSystemFromDisk();
		return connectedSystem;
	}
	
	
	public void setConnectedSystem(PanelSystem pPanelSystem)
	{
		connectedSystem = pPanelSystem;
//		writeSystemToDisk(pPanelSystem);
	}
	
	/**
	 * 
	 * @param pPanelSystem
	 */
	public void writeSystemToDisk(PanelSystem pPanelSystem)
	{
		File file = new File(getCacheDir(), "curr_system");
		if(null != pPanelSystem)
		{
			JSONObject json = null;
			try{
				json = pPanelSystem.toJson();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			
			if(null != json)
			{
				//save the language file
				FileOutputStream outputStream;
				try 
				{
				    outputStream = new FileOutputStream(file);
				    outputStream.write(json.toString().getBytes());
				    outputStream.close();
				   
				} catch (IOException e) {
				    e.printStackTrace();
				    boolean deleted = file.delete();
					if(!deleted)
					{
						file.deleteOnExit();
					}
				}
			}
			else
			{
				boolean deleted = file.delete();
				if(!deleted)
				{
					file.deleteOnExit();
				}
			}
			
		}
		else
		{
			boolean deleted = file.delete();
			if(!deleted)
			{
				file.deleteOnExit();
			}
		}
		
	}
	
	/**
	 * 
	 * @return
	 */
	public PanelSystem readSystemFromDisk()
	{
		PanelSystem system = null;
		//read language from file
		BufferedReader input = null;
		File file = null;
		try {
		    file = new File(getCacheDir(), "curr_system"); // Pass getFilesDir() and "MyFile" to read file
		 
		    if(file.exists())
		    {
		    	input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			    String line;
			    StringBuffer buffer = new StringBuffer();
			    while ((line = input.readLine()) != null) {
			        buffer.append(line);
			    }
				 
			    input.close();
			    
			    JSONObject jsonSystem = new JSONObject(  buffer.toString());
				system = PanelSystem.fromJson(jsonSystem);
		    }
		
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return system;
	}
}
