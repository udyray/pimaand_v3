package com.pima.apps.ipima.v3.actionbar;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;

import com.pima.apps.ipima.v3.R;



public class MoreActionProvider extends ActionProvider {

	private Context mContext;
	
	public MoreActionProvider(Context context) {
		super(context);

		mContext = context;
	}

	@Override
	public View onCreateActionView() {
	    // Inflate the action view to be shown on the action bar.
	    LayoutInflater layoutInflater = LayoutInflater.from(mContext);
	    
	    View view = layoutInflater.inflate(R.layout.actionbar_more, null);

	    return view;
	}

	@Override
	public void onPrepareSubMenu(SubMenu subMenu) {
		super.onPrepareSubMenu(subMenu);
	}

	@Override
	public boolean hasSubMenu() {
		return true;
	}
	
	
	
}
