package com.pima.apps.ipima.v3;

import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SpaPreference {

	private static final String	CONFIGURATION_SERVER		= "configurationServer";
	private static final String	SPA_PREFERENCE				= "spa_pref";

	
	private static final String	USER_NAME					= "userName";
	private static final String	USER_PHONE					= "userPhone";
	private static final String	SERVER_URL					= "serverUrl";
	private static final String	SERVER_PORT					= "serverPort";
	private static final String	ENABLE_NOTIFICATIONS		= "enableNotifications";
	private static final String	GCM_TOKEN					= "gcmToken";
	
	private static final String	SENT_TOKEN_TO_SERVER		= "sentTokenToServer";
	private static final String	LATEST_NOTIFICATIONS_FILTERS= "latestNotificationsFilter";
	
	
	public static final String	_DATE						= "_date";
	
	
	
	/**
	 * 
	 * @param context
	 * @param language
	 * @param date
	 */
	public static void setLanguageDate(Context context, String language, long date) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putLong(language + _DATE, date);
		editor.commit();
	}

	/**
	 * 
	 * @param context
	 * @return
	 */
	public static long getLanguageDate(Context context, String language) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		long result = sp.getLong(language + _DATE, -1);//if no value -1
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param configurationServer
	 */
	public static void setConfigurationServer(Context context, String configurationServer) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(CONFIGURATION_SERVER, configurationServer);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String getConfigurationServer(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		String result = sp.getString(CONFIGURATION_SERVER, null);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param userName
	 */
	public static void setUserName(Context context, String userName) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(USER_NAME, userName);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String getUserPhone(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		String result = sp.getString(USER_PHONE, null);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param userPhone
	 */
	public static void setUserPhone(Context context, String userPhone) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(USER_PHONE, userPhone);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String getUserName(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		String result = sp.getString(USER_NAME, null);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param serverUrl
	 */
	public static void setServerUrl(Context context, String serverUrl) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(SERVER_URL, serverUrl);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String getServerUrl(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		String result = sp.getString(SERVER_URL, null);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param serverPort
	 */
	public static void setServerPort(Context context, String serverPort) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(SERVER_PORT, serverPort);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String getServerPort(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		String result = sp.getString(SERVER_PORT, null);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param enable
	 */
	public static void setEnableNotifications(Context context, boolean enable) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(ENABLE_NOTIFICATIONS, enable);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getEnableNotifications(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		boolean result = sp.getBoolean(ENABLE_NOTIFICATIONS, false);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param sentToServer
	 */
	public static void setSentTokenToServer(Context context, boolean sentToServer) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(SENT_TOKEN_TO_SERVER, sentToServer);
		editor.commit();
	}

	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getSentTokenToServer(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		boolean result = sp.getBoolean(SENT_TOKEN_TO_SERVER, false);//if no value null
		return result;
	}
	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String getGCMToken(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		String result = sp.getString(GCM_TOKEN, null);//if no value null
		return result;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param token
	 */
	public static void setGCMToken(Context context, String token) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(GCM_TOKEN, token);
		editor.commit();
	}
	
	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static Set<String> getNotificationsFilters(Context context) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Set<String> result = sp.getStringSet(LATEST_NOTIFICATIONS_FILTERS, null);//if no value null
		return result;
	}
	
	/**
	 * 
	 * @param context
	 * @param filters
	 */
	public static void setNotificationsFilters(Context context, Set<String> filters) {
		SharedPreferences sp = context.getSharedPreferences(SPA_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putStringSet(LATEST_NOTIFICATIONS_FILTERS, filters);
		editor.commit();
	}
	
	
	
}
