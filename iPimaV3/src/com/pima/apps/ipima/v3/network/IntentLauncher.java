package com.pima.apps.ipima.v3.network;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import android.util.Log;

import com.pima.apps.ipima.v3.SpaApplication;

/**
 * 
 * @author Udy
 *
 */
public class IntentLauncher extends Thread 
{
	public static final String TAG 						= "IntentLauncher";
	
	public static final int TOKEN_DISCONNECT 						= -1;
	public static final int TOKEN_ABORT	 							= -2;
	public static final int TOKEN_GET_USER_SYSTEMS 					= 100;
	public static final int TOKEN_SYSTEM_INSERT 					= 101;
	public static final int TOKEN_SYSTEM_UPDATE 					= 102;
	public static final int TOKEN_DELETE_SYSTEM						= 103;
	
	public static final int TOKEN_AUTH 								= 200;
	
	public static final int TOKEN_GET_SYSTEM_STATUS 				= 300;
	public static final int TOKEN_GET_FAULTS 						= 301;
	public static final int TOKEN_GET_LOG							= 302;
	
	public static final int TOKEN_SET_ZONE_BYPASS					= 400;
	public static final int TOKEN_GET_ZONE_BYPASS					= 401;
	public static final int TOKEN_SET_LOOKIN_ZONE					= 402;
	
	public static final int TOKEN_SET_PARTITION_STATUS_DISARM		= 500;
	public static final int TOKEN_SET_PARTITION_STATUS_ARM_FULL		= 501;
	public static final int TOKEN_SET_PARTITION_STATUS_ARM_HOME_1	= 502;
	public static final int TOKEN_SET_PARTITION_STATUS_ARM_HOME_2	= 503;
	public static final int TOKEN_SET_PARTITION_STATUS_ARM_HOME_3	= 504;
	public static final int TOKEN_SET_PARTITION_STATUS_ARM_HOME_4	= 505;

	public static final int TOKEN_GET_OUTPUT_STATUS	 				= 600;
	public static final int TOKEN_SET_OUTPUT_STATUS_ACTIVE			= 601;
	public static final int TOKEN_SET_OUTPUT_ENABLE					= 602;
	
	public static final int TOKEN_ENABLE_REMOTE_ACCESS				= 700;
	public static final int TOKEN_STOP_SIREN						= 701;
	
	public static final int TOKEN_GET_NOTIFICATIONS_TYPES			= 800;
	public static final int TOKEN_GET_NOTIFICATIONS					= 801;
	public static final int TOKEN_GET_NOTIFICATIONS_FILTERS			= 802;
	public static final int TOKEN_SET_NOTIFICATIONS_FILTERS_ON		= 803;
	public static final int TOKEN_SET_NOTIFICATIONS_FILTERS_OFF		= 804;
	public static final int TOKEN_SET_NOTIFICATIONS_FILTERS			= 805;
	public static final int TOKEN_REGISTER_FOR_NOTIFICATION			= 806;
	public static final int TOKEN_GET_NOTIFICATIONS_NO_LOGIN		= 807;
	
	public static final int TOKEN_GET_CLIENTS						= 900;
	public static final int TOKEN_SET_CLIENT						= 901;
	public static final int TOKEN_DELETE_CLIENT						= 902;
	
	public static final int TOKEN_CONNECTION_STATE_CONNECT_SERVER 	= 998;
	public static final int TOKEN_CONNECTION_STATE_GET_TEXTS 		= 999;
	
	
	
	private int mToken = -1;
	private SpaApplication mApplication;
	private HttpResponseHandler mResponseHandler;
	
	private String mGetUrl = null;
	private Map<String, Object> mExtraHeadersParams;
	private Map<String, Object> mDataParamsMap;
	private List<Object> mDataParamsList;
	private Object mDataParam;
	private boolean mIsBackground;
	
	public IntentLauncher(int pToken, SpaApplication pApplication, HttpResponseHandler pResponseHandler) {
		super();
		mToken = pToken;
		mApplication = pApplication;
		mResponseHandler = pResponseHandler;
		
		mDataParamsMap = null;
		mDataParamsList = null;
		mGetUrl = null;
		mExtraHeadersParams = null;
		
		mIsBackground = false;
	}

	
	
	public void setExtraHeadersParamsy(Map<String, Object> pExtraHeadersParams )
	{
		mExtraHeadersParams = pExtraHeadersParams;
	}
	
	public void setDataParams(Map<String, Object> pDataParams )
	{
		mDataParamsMap = pDataParams;
	}
	
	public void setDataParams(List<Object> pDataParams )
	{
		mDataParamsList = pDataParams;
	}
	
	public void setDataParam(Object pDataParam )
	{
		mDataParam = pDataParam;
	}
	
	public void setIsBackground(boolean pIsBackground)
	{
		mIsBackground = pIsBackground;
	}
	
	public void setGetUrl(String pGetUrl)
	{
		mGetUrl = pGetUrl;
	}

	@Override
	public void run() {
		try
		{
			PimaProtocol pp = new PimaProtocol(mApplication);
    		String url = null;
    		
    		 
    		HttpManager httpManager = new HttpManager(mResponseHandler, mApplication);
    		httpManager.setToken(mToken);
    		httpManager.setIsBackground(mIsBackground);
    		
    		JSONObject header = pp.getRequestHeaderJSON();
    		if(null != mExtraHeadersParams)
    		{
    			Iterator<String> keys = mExtraHeadersParams.keySet().iterator();
    			while(keys.hasNext())
    			{
    				String key = keys.next();
    				Object value = mExtraHeadersParams.get(key);
    				header.put(key, value);
    			}
    		}
    		httpManager.setHeaders(header);
    		
    		if(mToken == TOKEN_CONNECTION_STATE_CONNECT_SERVER)
	    	{
    			url = pp.getConfigurationServerUrl();
	    		httpManager.setIsGet(false);
	    	}
    		else if(mToken == TOKEN_CONNECTION_STATE_GET_TEXTS)
	    	{
	    		 url = mGetUrl;
	    		 httpManager.setIsGet(true);
	    	}
    		else if(mToken == TOKEN_GET_USER_SYSTEMS)
    		{
    			 url = pp.getUserSystemsUrl();
    		}
    		else if(mToken == TOKEN_DELETE_SYSTEM)
    		{
    			url = pp.getDeleteSystemUrl();
    		}
    		else if(mToken == TOKEN_AUTH)
    		{
    			url = pp.getLoginSystemsUrl();
    		}
    		else if(mToken == TOKEN_GET_SYSTEM_STATUS)
    		{
    			 url = pp.getGetSystemStatusUrl();
    		}
    		else if(mToken == TOKEN_GET_NOTIFICATIONS_TYPES)
    		{
    			url = pp.getGetNotificationsTypesUrl();
    		}
    		else if(mToken == TOKEN_GET_NOTIFICATIONS)
    		{
    			url = pp.getGetNotificationsUrl();
    		}
    		else if(mToken == TOKEN_GET_NOTIFICATIONS_NO_LOGIN)
    		{
    			url = pp.getGetNotificationsNoLoginUrl();
    		}
    		else if(mToken == TOKEN_GET_NOTIFICATIONS_FILTERS)
    		{
    			url = pp.getGetNotificationsFiltersUrl();
    		}
    		else if(mToken == TOKEN_SET_NOTIFICATIONS_FILTERS_ON)
    		{
    			url = pp.getSetNotificationsFiltersUrl();
    		}
    		else if(mToken == TOKEN_SET_NOTIFICATIONS_FILTERS_OFF)
    		{
    			url = pp.getSetNotificationsFiltersUrl();
    		}
    		else if(mToken == TOKEN_SET_NOTIFICATIONS_FILTERS)
    		{
    			url = pp.getSetNotificationsFiltersUrl();
    		}
    		else if(mToken == TOKEN_REGISTER_FOR_NOTIFICATION)
    		{
    			url = pp.getGetRegisterForNotificationsUrl();
    		}
    		else if(mToken == TOKEN_GET_FAULTS)
    		{
    			url = pp.getGetSystemFaultsUrl();
    		}
    		else if(mToken == TOKEN_GET_LOG)
    		{
    			url = pp.getGetSystemLogsUrl();
    		}
    		else if(mToken == TOKEN_SET_PARTITION_STATUS_DISARM)
    		{
    			url = pp.getSetPartitionStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_PARTITION_STATUS_ARM_HOME_1)
    		{
    			url = pp.getSetPartitionStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_PARTITION_STATUS_ARM_HOME_2)
    		{
    			url = pp.getSetPartitionStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_PARTITION_STATUS_ARM_HOME_3)
    		{
    			url = pp.getSetPartitionStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_PARTITION_STATUS_ARM_HOME_4)
    		{
    			url = pp.getSetPartitionStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_PARTITION_STATUS_ARM_FULL)
    		{
    			url = pp.getSetPartitionStatusUrl();
    		}
    		else if(mToken == TOKEN_GET_ZONE_BYPASS)
    		{
    			url = pp.getGetZoneBypassUrl();
    		}
    		else if(mToken == TOKEN_SET_LOOKIN_ZONE)
    		{
    			url = pp.getGetLookinZoneUrl();
    		}
    		else if(mToken == TOKEN_SET_ZONE_BYPASS)
    		{
    			url = pp.getSetZoneBypassUrl();
    		}
    		else if(mToken == TOKEN_GET_OUTPUT_STATUS)
    		{
    			url = pp.getGetOutputStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_OUTPUT_STATUS_ACTIVE)
    		{
    			url = pp.getSetOutputStatusUrl();
    		}
    		else if(mToken == TOKEN_SET_OUTPUT_ENABLE)
    		{
    			url = pp.getSetOutputEnableUrl();
    		}
    		else if(mToken == TOKEN_ENABLE_REMOTE_ACCESS)
    		{
    			url = pp.getEnableRemoteAccessUrl();
    		}
    		else if(mToken == TOKEN_STOP_SIREN)
    		{
    			url = pp.getStopSirenUrl();
    		}
    		else if(mToken == TOKEN_SET_CLIENT)
    		{
    			url = pp.getSetClientUrl();
    		}
    		else if(mToken == TOKEN_GET_CLIENTS)
    		{
    			url = pp.getGetClientsUrl();
    		}
    		else if(mToken == TOKEN_DELETE_CLIENT)
    		{
    			url = pp.getUnPairClientUrl();
    		}
    		else if(mToken == TOKEN_SYSTEM_UPDATE)
    		{
    			url = pp.getUpdateSystemUrl();
    		}
    		else if(mToken == TOKEN_SYSTEM_INSERT)
    		{
    			url = pp.getAddSystemUrl();
    		}
    		else if(mToken == TOKEN_DISCONNECT)
    		{
    			url = pp.getDisconnectUrl();
    		}
    		else if(mToken == TOKEN_ABORT)
    		{
    			url = pp.getAbortUrl();
    		}
    		
    		if(null != mDataParamsMap)
    		{

				JSONObject data = new JSONObject();
    			Iterator<String> keys = mDataParamsMap.keySet().iterator();
    			while(keys.hasNext())
    			{
    				String key = keys.next();
    				Object value = mDataParamsMap.get(key);
    				data.put(key, value);
    			}
    			httpManager.setDataJsonObject(data);
			}
    		else if(null != mDataParamsList)
    		{

				JSONArray data = new JSONArray();
				for(int i=0; i<mDataParamsList.size(); i++)
    			{
    				Object value = mDataParamsList.get(i);
    				data.put(value);
    			}
    			httpManager.setDataJsonArray(data);
			
    		}
    		else if(null != mDataParam)
    		{
    			httpManager.setDataObject(mDataParam);
    		}
    		
    		Log.i(TAG, "IntentLauncher created for (" + mToken + ") " + url);
//    		String allRequests = mApplication.getSentRequests();
//    		System.out.println("All Requests: " + allRequests);
    		
    		boolean isNeedToRun = true;
    		if(mToken != TOKEN_ABORT)
     		{
    			mApplication.addSentRequest(mToken);
     		}
    		else
    		{
    			if(mApplication.getNumberOfSentRequests() > 0)
    			{
    				mApplication.removeAllRequests();
    			}
    			else
    			{
//    				System.out.println("Abort for nothing - dont run");
    				isNeedToRun = false;
    			}
    			
    		}
    		if(isNeedToRun)
    		{
    			httpManager.execute(url);
    		}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, e.getMessage(), e);
		}

	}
}
