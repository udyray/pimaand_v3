package com.pima.apps.ipima.v3.network;

import android.view.View;

public interface AdapterOnClickHandler {

	public void doOnItemClick(View v);

}
