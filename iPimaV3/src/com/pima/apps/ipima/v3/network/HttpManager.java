package com.pima.apps.ipima.v3.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.SpaApplication;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


public class HttpManager extends AsyncTask<String, String, String> {
	private static final String TAG 	= "HttpManager";
	private static final int TIMEOUT 	= 70;//70 seconds
	
	private boolean mIsTimeout;
	
	private int mToken = -1;
	private boolean mCaughtError = false;
	private boolean mUncaughtError = false;
	HttpResponseHandler mHandler;

	Context mContext;
	
	boolean mIsGet = false;
	JSONObject mHeaders = null;
	JSONObject mDataJsonObject 	= null;
	JSONArray mDataJsonArray	= null;
	Object mDataObject 	= null;
	boolean mIsBackground = false;
	
	public HttpManager(HttpResponseHandler handler, Context pContext) {
		mHandler = handler;
		mContext = pContext;
		
		mIsTimeout = false;
	}
	
	public void setHeaders(JSONObject pHeaders)
	{
		mHeaders = pHeaders;
	}
	
	public void setDataJsonObject(JSONObject pData)
	{
		mDataJsonObject = pData;
	}
	
	public void setDataJsonArray(JSONArray pData)
	{
		mDataJsonArray = pData;
	}
	
	public void setDataObject(Object pData)
	{
		mDataObject = pData;
	}
	
	public void setToken(int pToken)
	{
		mToken = pToken;
	}
	
	public void setIsGet(boolean pIsGet)
	{
		mIsGet = pIsGet;
	}
	
	public void setIsBackground(boolean pIsBackground)
	{
		mIsBackground = pIsBackground;
	}
	
	@Override
	protected String doInBackground(String... uri) 
	{
		HttpClient httpclient = new DefaultHttpClient();
		
		Log.i(TAG, "Connecting to " + uri[0]);
		
		HttpResponse response;
		final String[] responseString = new String[1];
		try 
		{
			mUncaughtError = false;
			mCaughtError = false;
			if(mIsGet)
			{
				final HttpGet httpGet = new HttpGet(uri[0]);
				
				TimerTask task = new TimerTask() {
				    @Override
				    public void run() {
				        if (httpGet != null) {
				        	
				        	mIsTimeout = true;
				        	httpGet.abort();
				        	
				        }
				    }
				};
				new Timer(true).schedule(task, TIMEOUT * 1000);
				response = httpclient.execute(httpGet);
			}
			else
			{
				final HttpPost httpPost = new HttpPost(uri[0]);
				TimerTask task = new TimerTask() {
				    @Override
				    public void run() {
				        if (httpPost != null) {
				        	
				        	mIsTimeout = true;
				        	httpPost.abort();
				        }
				    }
				};
				new Timer(true).schedule(task, TIMEOUT * 1000);
				
				httpPost.setHeader("Content-type", "application/json");
				
				JSONObject json = new JSONObject();
				if(null != mHeaders)
				{
					json.put("header", mHeaders);
				}
				else
				{
					json.put("header", new JSONObject());
				}
				
				if(null != mDataJsonObject) 
				{
					json.put("data", mDataJsonObject);
				}
				else if(null != mDataJsonArray) 
				{
					json.put("data", mDataJsonArray);
				}
				else if(null != mDataObject) 
				{
					json.put("data", mDataObject);
				}
				else
				{
					json.put("data", new JSONObject());
				}
				
				Log.i(TAG, mToken + " - Sending " + json.toString());
				
				httpPost.setEntity(new StringEntity(json.toString(), "UTF-8"));
				
				response = httpclient.execute(httpPost);
			}
			
			StatusLine statusLine = response.getStatusLine();
			if(response.getHeaders("Content-Type").length == 0)
			{
				Log.i(TAG, mToken + " - Answer Status Code: " + statusLine.getStatusCode() + "(NULL)");
			}
			else
			{
				Log.i(TAG, mToken + " - Answer Status Code: " + statusLine.getStatusCode() + "(" + response.getHeaders("Content-Type")[0] + ")");
			}
			
			if (statusLine.getStatusCode() == HttpStatus.SC_OK) 
			{
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				if (response.getHeaders("Content-Type").length == 0	||
					response.getHeaders("Content-Type")[0].getValue().contains("text/json") ||
					response.getHeaders("Content-Type")[0].getValue().contains("application/json")) 
				{
					responseString[0] = out.toString("UTF-8");
				} 
				else//error??  
				{
					// Closes the connection.
					try{
						response.getEntity().getContent().close();
						throw new IOException(response.getHeaders("Content-Type")[0].toString());
					}
					catch(Exception e){}
				}
			}  
			else if (statusLine.getStatusCode() == HttpStatus.SC_NO_CONTENT) 
			{
				responseString[0] = "";
			}
			else if (statusLine.getStatusCode() == HttpStatus. SC_INTERNAL_SERVER_ERROR) //server error message
			{
				Log.i(TAG, "Caught Error (" + mToken + ")");
				mCaughtError = true;
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString[0] = out.toString();
				if (response.getHeaders("Content-Type").length == 0	||
					response.getHeaders("Content-Type")[0].getValue().contains("text/json") ||
					response.getHeaders("Content-Type")[0].getValue().contains("application/json")) 
				{
					responseString[0] = out.toString("UTF-8");
				} 
				else//error??  
				{
					// Closes the connection.
					try{
						response.getEntity().getContent().close();
						throw new IOException(response.getHeaders("Content-Type")[0].toString());
					}
					catch(Exception e){}
				}
			
			}
			else  
			{
				Log.i(TAG, "UnCaught Error (" + mToken + ")");
				mUncaughtError = true;
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString[0] = out.toString();
				
				try{
					// Closes the connection.
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
				catch(Exception e){}
			}
			return responseString[0];
		} catch (Exception e) 
		{
			if(mIsTimeout)
			{
				mCaughtError = true;
				JSONObject json = new JSONObject();
	    		try
	    		{
	    			json.put("errorCode", 999);
	    			json.put("errorText", "Error-Timeout");
	    		}
	    		catch(Exception ex){
	    			e.printStackTrace();
	    		}
	    		return json.toString();
			}
			else
			{
				Log.e(TAG, "UnCaught Error (" + e.getMessage() + ")", e);
				mUncaughtError = true;
			}
			return null;
		}
		finally{
			if(null != httpclient)
			{
				httpclient.getConnectionManager().shutdown();
			}
		}
		
	}

	@Override
	public void onPostExecute(String result) {
		if(null != result)
		{
			result = result.replaceAll("\ufeff", "");
		}
		((SpaApplication)mContext).removeSentRequest(mToken);
		Log.i(TAG, "Server Response(" + mToken + "-" + mIsBackground + "):\n" + result);
		
		if(mCaughtError)
		{
			mHandler.handleCaughtHttpError(mToken, result, mIsBackground);
			return;
		}
		
		if(mUncaughtError)
		{
			mHandler.handleUncaughtHttpError(mToken, result, mIsBackground);
			return;
		}
		
		if(mToken != IntentLauncher.TOKEN_ABORT)
		{
			try{
				mHandler.handleHttpResponse(mToken, result, mIsBackground);
			}
			catch(Exception e){
				mHandler.handleUncaughtHttpError(mToken, result, mIsBackground);
			}
		}
	}
	
}