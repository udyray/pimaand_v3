package com.pima.apps.ipima.v3.network;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.SpaPreference;


public class PimaProtocol {
	
	private static final String DATE_FORMAT_FULL = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	private static final String DATE_FORMAT_SIMPLE = "dd/MM/yyyy";
	private static final String TIME_FORMAT_SIMPLE = "HH:mm";
	
	private SpaApplication 	mApplication = null;
	private String 			mDeviceId = null;
	
	public PimaProtocol(SpaApplication pApplication) {
		super();
		mApplication = pApplication;
		
		TelephonyManager tm = (TelephonyManager) mApplication.getSystemService(Context.TELEPHONY_SERVICE);
		mDeviceId = tm.getDeviceId();
		if(mDeviceId.equals("000000000000000"))
		{
			mDeviceId = "123456789012345";
		}
	}


	/**
	 * 
	 * @return
	 */
	public JSONObject getRequestHeaderJSON()
	{
		JSONObject header = new JSONObject();
		
		try{
			header.put("clientId", mDeviceId);
			header.put("clientPlatform", 2);
			header.put("platformVersion", android.os.Build.VERSION.RELEASE );
			
			String versionName = mApplication.getPackageManager().getPackageInfo(mApplication.getPackageName(), 0).versionName;
			header.put("appVersion", versionName);
			
			if(null != mApplication.getConnectedSystem())
			{
				if(null!=mApplication.getConnectedSystem().getSystemId())
				{
					header.put("systemId", mApplication.getConnectedSystem().getSystemId());
				}
			}
//			else if(null != mApplication.systemId)
//			{
//				header.put("systemId", mApplication.systemId);
//			}
			
			//sessionToken
			if(null != mApplication.sessionToken)
			{
				header.put("sessionToken", mApplication.sessionToken);
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return header;
	}
	
	private String getConfigurationServerBaseUrl() {
		String configurationServer = SpaPreference.getConfigurationServer(mApplication);
		if(null == configurationServer)
		{
			configurationServer = mApplication.getString(R.string.default_configuration_server);
		}
		return configurationServer;
	} 

	public String getConfigurationServerUrl() {
		String url = getConfigurationServerBaseUrl();
		
		String lang = getLanguageCode( Locale.getDefault().getLanguage());
		url += "api/Client/GetConfiguration/" + (lang!=null?lang:"");
		
		return url; 
	}

	public String getUserSystemsUrl() {
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/GetList";
		
		return url;
	}
	
	public String getUpdateSystemUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/SetName";
		
		return url;
	}
	
	
	public String getAddSystemUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/Add";
		
		return url;
	}
	
	public String getDisconnectUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/Disconnect";
		
		return url;
	}
	
	public String getAbortUrl() {
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/Abort";
		
		return url;
	}
	
	public String getDeleteSystemUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/Remove";
		
		return url;
	}
	
	public String getLoginSystemsUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/Authenticate";
		
		return url;
	}
	
	
	public String getGetSystemStatusUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/Get";
		
		return url;
	}
	
	
	public String getGetSystemLogsUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/GetLogs";
		
		return url;
	}
	
	
	public String getGetSystemFaultsUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/GetFaults";
		
		return url;
	}
	
	public String getSetPartitionStatusUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Partition/SetStatus";
		
		return url;
		
	}
	
	public String getGetZoneBypassUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Zone/GetStatus";
		
		return url;
		
	}
	
	public String getGetLookinZoneUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/LookIn";
		
		return url;
		
	}
	
	public String getSetZoneBypassUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Zone/SetStatus";
		
		return url;
		
	}
	
	public String getGetOutputStatusUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Output/GetStatus";
		
		return url;
		
	}
	
	public String getSetOutputStatusUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Output/SetStatus";
		
		return url;
		
	}
	
	
	public String getSetOutputEnableUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Output/SetEnable";
		
		return url;
		
	}
	
	public String getEnableRemoteAccessUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/RemoteAccess";
		
		return url;
		
	}
	
	public String getStopSirenUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/UserSystem/StopSiren";
		
		return url;
		
	}
	
	public String getGetClientsUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Client/Get";
		
		return url;
		
	}
	
	
	public String getSetClientUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Client/Set";
		
		return url;
		
	}
	
	public String getUnPairClientUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Client/Unpair";
		
		return url;
	}
	
	
	public String getGetRegisterForNotificationsUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Client/RegisterForNotifications";
		
		return url;
		 
	}
	
	public String getGetNotificationsFiltersUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Notification/GetFilter";
		
		return url;
		
	}
	
	public String getSetNotificationsFiltersUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Notification/SetFilter";
		
		return url;
		
	}
	
	
	public String getGetNotificationsUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Notification/GetMessages";
		
		return url;
		
	}
	
	public String getGetNotificationsNoLoginUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Notification/GetMessagesNoAuth";
		
		return url;
		
	}
	
	
	public String getGetNotificationsTypesUrl()
	{
		String url = mApplication.getServerUrl();
		
		url += "api/Notification/GetTypes";
		
		return url;
		
	}
	
	
	
	
	public static String getLanguageCode(String language) {
		if(null != language)
		{
			if(language.equalsIgnoreCase("en"))
			{
				return "en";
			}
			else if(language.equalsIgnoreCase("iw"))
			{
				return "he";
			}
		}
		return language;
	}

	
	/**
	 * 
	 * @param pDate
	 * @return
	 */
	public static Date parseDate(String pDate)
	{
		if(null == pDate)
		{
			return null;
		}
		
		try 
		{
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_FULL, Locale.US);
			return formatter.parse(pDate);
		} 
		catch(Exception e)
		{e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param pDate
	 * @return
	 */
	private static String formatDate(Date pDate, String pFormat)
	{
		if(null == pDate)
		{
			return null;
		}
		
		try 
		{
			SimpleDateFormat formatter = new SimpleDateFormat(pFormat, Locale.US);
			return formatter.format(pDate);
		} 
		catch(Exception e)
		{e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param pDate
	 * @return
	 */
	public static String formatDateSimple(Date pDate)
	{
		if(null == pDate)
		{
			return null;
		}
		
		try 
		{
			return formatDate(pDate, DATE_FORMAT_SIMPLE);
		} 
		catch(Exception e)
		{e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param pDate
	 * @return
	 */
	public static String formatDateFull(Date pDate)
	{
		if(null == pDate)
		{
			return null;
		}
		
		try 
		{
			return formatDate(pDate, DATE_FORMAT_FULL);
		} 
		catch(Exception e)
		{e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param pDate
	 * @return
	 */
	public static String formatTime(Date pDate)
	{
		if(null == pDate)
		{
			return null;
		}
		
		try 
		{
			SimpleDateFormat formatter = new SimpleDateFormat(TIME_FORMAT_SIMPLE, Locale.US);
			return formatter.format(pDate);
		} 
		catch(Exception e)
		{e.printStackTrace();
			return null;
		}
	}
}
