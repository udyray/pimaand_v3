package com.pima.apps.ipima.v3.network;

public interface HttpResponseHandler {

	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) throws Exception;
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground);
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground);

}
