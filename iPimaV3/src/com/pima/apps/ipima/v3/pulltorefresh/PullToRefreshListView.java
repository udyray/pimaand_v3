package com.pima.apps.ipima.v3.pulltorefresh;

/**
 * 
 * @author Yao Changwei(yaochangwei@gmail.com)
 * 
 *        Pull To  Refresh List View Demo, Including the arrow and text change.
 */

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PullToRefreshListView extends RefreshableListView {
	
	private Context ctx;

	public PullToRefreshListView(final Context context, AttributeSet attrs) {
		super(context, attrs);

		ctx = context;
		
		setContentView(R.layout.pull_to_refresh);		

		mListHeaderView.setBackgroundColor(Color.parseColor("#e0e0e0"));
		setOnHeaderViewChangedListener(new OnHeaderViewChangedListener() {

			@Override
			public void onViewChanged(View v, boolean canUpdate) {
				Log.d("View", "onViewChanged" + canUpdate);
				TextView tv = (TextView) v.findViewById(R.id.refresh_text);
				ImageView img = (ImageView) v.findViewById(R.id.refresh_icon);
				Animation anim;
				if (canUpdate) {
					anim = AnimationUtils.loadAnimation(context, R.anim.rotate_up);
					tv.setText(((SpaApplication)((Activity)ctx).getApplication()).getLocalizedString("release_to_refresh"));
				} else {
					tv.setText(((SpaApplication)((Activity)ctx).getApplication()).getLocalizedString("pull_down_to_refresh"));
					anim = AnimationUtils.loadAnimation(context, R.anim.rotate_down);
				}
				img.startAnimation(anim);
			}

			@Override
			public void onViewUpdating(View v) {
				Log.d("View", "onViewUpdating");
				TextView tv = (TextView) v.findViewById(R.id.refresh_text);
				ImageView img = (ImageView) v.findViewById(R.id.refresh_icon);
				ProgressBar pb = (ProgressBar) v.findViewById(R.id.refresh_loading);
				pb.setVisibility(View.VISIBLE);
				tv.setText(((SpaApplication)((Activity)ctx).getApplication()).getLocalizedString("LoadingOverlayText"));
				img.clearAnimation();
				img.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onViewUpdateFinish(View v) {
				Log.d("View", "onViewUpdateFinish");
				TextView tv = (TextView) v.findViewById(R.id.refresh_text);
				ImageView img = (ImageView) v.findViewById(R.id.refresh_icon);
				ProgressBar pb = (ProgressBar) v.findViewById(R.id.refresh_loading);

				tv.setText(((SpaApplication)((Activity)ctx).getApplication()).getLocalizedString("pull_down_to_refresh"));
				pb.setVisibility(View.INVISIBLE);
				tv.setVisibility(View.VISIBLE);
				img.setVisibility(View.VISIBLE);
			}

		});
	}

}
