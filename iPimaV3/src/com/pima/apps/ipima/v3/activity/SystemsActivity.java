package com.pima.apps.ipima.v3.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.fragment.SystemsListFragment;
import com.pima.apps.ipima.v3.fragment.SystemsListFragment.SystemsListCallbacks;
import com.pima.apps.ipima.v3.network.IntentLauncher;
import com.pima.apps.ipima.v3.views.PanelSystem;
import com.pima.apps.ipima.v3.views.SystemConfiguration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SystemsActivity extends BaseActivity
	implements SystemsListCallbacks {

	public static final int TOKEN_UPDATE_USER_NAME 			= 0;
	private boolean isEdit = false;
	public View moreListOverflow = null;
	
	SystemsListFragment fragmentSystems;
	
	private Button moreOverflow;
	
	public Button doneButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_systems);
		
//		mApplication.clearLocalization();
		
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout, null);
		
		
		TextView titleView = (TextView)v.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(mApplication.getLocalizedString("AlarmSystems"));
		}
		
		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setText(mApplication.getLocalizedString("Plus"));
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				displayMoreView();
				onSystemAdd();
			}
		});
		
		
		Button editView = (Button)v.findViewById(R.id.edit);
		if(editView != null) {
			editView.setText(mApplication.getLocalizedString("Edit"));
			editView.setOnClickListener(new Button.OnClickListener() {
				
				@Override
				public void onClick(View v) {

					if(isEdit)
					{
						isEdit = false;
						Button editView = (Button)v.findViewById(R.id.edit);
						editView.setText(mApplication.getLocalizedString("Edit"));
						
					}
					else
					{
						isEdit = true;
						Button editView = (Button)v.findViewById(R.id.edit);
						editView.setText(mApplication.getLocalizedString("Done"));
					}
					
					moreOverflow.setEnabled(!isEdit);
					fragmentSystems.setEdit(isEdit);
				}
			});
		}
		
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(v);

		if (savedInstanceState == null) {
			// First-time init; create fragment to embed in activity.
			fragmentSystems = SystemsListFragment.newInstance();
			
            
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(android.R.id.content, fragmentSystems);
            ft.commitAllowingStateLoss();
        }
	}
	
	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		mApplication.action = SpaApplication.CUR_ACTION.NONE;
	}
	
	

	@Override
	public void onInfoOkClick(String tag) {
		super.onInfoOkClick(tag);
		
		if (TAG_DIALOG_WARNING.equals(tag)){
        	finish();
		}
	}



	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {
		if(keyCode==KeyEvent.KEYCODE_BACK)  
        {
			Bundle bundle = new Bundle();
			bundle.putString("title", mApplication.getLocalizedString("confirm_quit"));
			showDialog(TAG_DIALOG_WARNING, bundle);
			
        }else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;
    }

	@Override
	public void onSystemAdd() {
		// user wants to add new system
		startActivity(new Intent(SystemsActivity.this, SystemEditActivity.class));
	}

	@Override
	public void onConnectToSystem(JSONObject systemJson, boolean isEdit) {
		clearCache();
	
		String systemId = systemJson.optString("systemId");
		String systemName = systemJson.optString("name");
		int systemType = systemJson.optInt("panelType");
		
		 
		JSONObject configJson = systemJson.optJSONObject("configuration");
		PanelSystem system = new PanelSystem(systemId, systemName, systemType);
		
		SystemConfiguration configuration = SystemConfiguration.fromJson(configJson);
		system.setConfiguration(configuration);
		mApplication.setConnectedSystem(system);
		
		Intent login = new Intent(SystemsActivity.this, NewLoginActivity.class);
		login.putExtra(NewLoginActivity.EXTRAS_IS_EDIT, isEdit);
		startActivity(login);
	}

	@Override
	public void onSpaConnected() {
		super.onSpaConnected();
		
	}
	
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void displayMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param pContactName
	 * @param pContactPhone
	 */
	public void swapToSettings(String pContactName, String pContactPhone)
	{
		fragmentSystems.dismissLoader();
		fragmentSystems.isDisconnected = true;
		
		// user wants to add new system
		Intent intent = new Intent(SystemsActivity.this, SettingsActivity.class);
		intent.putExtra(SettingsActivity.EXTRAS_CLIENT_NAME, pContactName);
		intent.putExtra(SettingsActivity.EXTRAS_CLIENT_PHONE, pContactPhone);
		startActivity(intent);
    }
	
	/**
	 * 
	 */
	public void swapToSystemsList()
	{
		fragmentSystems.isDisconnected = false;
		
        // First-time init; create fragment to embed in activity.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(android.R.id.content, fragmentSystems);
        ft.commitAllowingStateLoss();
    
	}
	
	public void getUserSystems()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_USER_SYSTEMS, mApplication, this);
		il.start();
	}
	
	
	public void deleteUserSystem(String pSystenId)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_DELETE_SYSTEM, mApplication, this);
		Map<String, Object> extragHeader = new HashMap<String, Object>();
		extragHeader.put("systemId", pSystenId);
		il.setExtraHeadersParamsy(extragHeader);
		il.start();
	}
	
	
	public void getClient()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_CLIENTS, mApplication, this);
		il.start();
	}
	
	
//	public void addClient(String pUserName, String pPhoneNumber)
//	{
//		Map<String, Object> data = new HashMap<String, Object>();
////		data.put("id", value);
//		data.put("Name", pUserName);
//		data.put("PhoneNumber", pPhoneNumber);
//
//		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_CLIENT, mApplication, this);
//		il.setDataParams(data);
//		il.start();
//		
//		
//	}
	
	
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {

		try
		{
			switch (pToken) {
				case IntentLauncher.TOKEN_GET_USER_SYSTEMS:
					JSONArray systemsArray = new JSONArray(res);
	    			fragmentSystems.setSystemsArray(systemsArray);
					break;
					
				case IntentLauncher.TOKEN_DELETE_SYSTEM:
					getUserSystems();
					moreOverflow.setEnabled(true);
					break;
					
				case IntentLauncher.TOKEN_GET_CLIENTS:
					
					JSONArray contactsArray = new JSONArray(res);
					if(contactsArray != null && contactsArray.length()==1)
					{
						JSONObject contactObj = contactsArray.getJSONObject(0);

						String name = contactObj.optString("name");
						String phoneNumber = contactObj.optString("phoneNumber");
						
						swapToSettings(name, phoneNumber);
					}
					else
					{
						swapToSettings("", "");
					}
					
				
					break;
					
				default:
					handleUncaughtHttpError(pToken, res, pIsBackground);
					break;
				}
			
		}
		catch(Exception e){
			handleUncaughtHttpError(pToken, e.getMessage() + ": " + res, pIsBackground);
		}
		
	
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		
		if(null != fragmentSystems)
		{
			fragmentSystems.dismissLoader();
		}
		onSpaError(res);
		
	}
}
