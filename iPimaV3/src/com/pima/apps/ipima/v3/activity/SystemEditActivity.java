package com.pima.apps.ipima.v3.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.SpaPreference;
import com.pima.apps.ipima.v3.fragment.BaseFragment;
import com.pima.apps.ipima.v3.fragment.ContactsListFragment;
import com.pima.apps.ipima.v3.fragment.NotificationsFiltersListFragment;
import com.pima.apps.ipima.v3.fragment.OutputsEnabledListFragment;
import com.pima.apps.ipima.v3.fragment.SystemEditFragment;
import com.pima.apps.ipima.v3.network.IntentLauncher;
import com.pima.apps.ipima.v3.views.Contact;
import com.pima.apps.ipima.v3.views.NotificationType;
import com.pima.apps.ipima.v3.views.PanelSystem;
import com.pima.apps.ipima.v3.views.SystemNotificationFilter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SystemEditActivity extends BaseActivity {

	private SystemEditFragment systemEditFragment;
	private OutputsEnabledListFragment outputsFragment;
	private NotificationsFiltersListFragment notificationsFragment;
	private ContactsListFragment contactsFragment;
	
	public static final String KEY_SYSTEM_EDIT = "systemEdit";
	public static final String KEY_SYSTEM_NAME = "systemName";
	
	public View moreListOverflow = null;
	private Button moreOverflow;

	private TextView titleView;
	public Button doneButton;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_screen);
		
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout_done, null);		
		
		titleView = (TextView)v.findViewById(R.id.titleView);
		
		
		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayMoreView();
			}
		});
		
		doneButton = (Button)v.findViewById(R.id.menu_done);
		if(doneButton != null) {
			doneButton.setText(mApplication.getLocalizedString("Done"));
		}
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(v);

		if (savedInstanceState == null) {
			outputsFragment = OutputsEnabledListFragment.newInstance(getIntent().getExtras(), true);
			outputsFragment.doneItem = doneButton;
			
			notificationsFragment = NotificationsFiltersListFragment.newInstance(getIntent().getExtras());
			notificationsFragment.doneItem = doneButton;
			
			contactsFragment = ContactsListFragment.newInstance(getIntent().getExtras());
			contactsFragment.doneItem = doneButton;
			
			// During initial setup, plug in the details fragment.
			systemEditFragment = SystemEditFragment.newInstance(getIntent().getExtras());
			systemEditFragment.doneItem = doneButton;
			 
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

			fragmentTransaction.add(R.id.fragment_container, systemEditFragment);
			fragmentTransaction.commitAllowingStateLoss();
			
        }
		
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
		if(keyCode==KeyEvent.KEYCODE_BACK)  
        {
			disconnect();
        }
		else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;
    }
	
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void displayMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void onInfoOkClick(String tag) {
		super.onInfoOkClick(tag);
		
		if( TAG_DIALOG_WARNING.equals(tag) ||
			TAG_DIALOG_INFO_DISCONNECT.equals(tag))
		{
			if(null != mApplication.getConnectedSystem())
			{
				disconnect();
			}
			else
			{
				this.finish();
			}
		}
		else if(TAG_DIALOG_INFO.equals(tag))
		{
			notificationsFragment.startLoader();
			getNotificationsFilters();
		}
		else if(TAG_DIALOG_INFO_OK.equals(tag))
		{
			swapToEdit();
		}
		else
		{
			if(null != mApplication.getConnectedSystem())
			{
				disconnect();
			}
			else
			{
				this.finish();
			}
		}
	}
	
	
	
//	@Override
//	public void finish() {
//		disconnect();
//		super.finish();
//	}

	public void addSystem(String pSystemName, String pPairCode)
	{
		Map<String, Object> data = new HashMap<String, Object>();
//		data.put("id", value);
		data.put("name", pSystemName);
		data.put("pairingCode", pPairCode);
		data.put("clientName", android.os.Build.MODEL);
		
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SYSTEM_INSERT, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	
	/**
	 * 
	 * @param pSystemName
	 */
	public void updateSystemName(String pSystemName)
	{

//		Map<String, Object> data = new HashMap<String, Object>();
//		data.put("systemId", pSystemId);
//		data.put("name", pSystemName);
		
//		List<Object> data = new ArrayList<Object>();
//		data.add(pSystemName);
		
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SYSTEM_UPDATE, mApplication, this);
		il.setDataParam(pSystemName);
		il.start();
	}
	
	public void setTitle(String pTitle)
	{
		if(titleView != null) {
			titleView.setText(mApplication.getLocalizedString(pTitle));
		}
	}
	
	public void getNotificationsFilters()
	{
		if(null != mApplication.getConnectedSystem().getSystemNotificationsTypesMap())
		{
			IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_NOTIFICATIONS_FILTERS, mApplication, this);
			il.setDataParam(mApplication.getConnectedSystem().getSystemId());
			il.start();
		}
		else
		{
			IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_NOTIFICATIONS_TYPES, mApplication, this);
			il.setDataParam(mApplication.getConnectedSystem().getSystemId());
			il.start();
		}
	}
	
	
	public void getContacts()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_CLIENTS, mApplication, this);
//		il.setDataParam(mApplication.connectedSystem.getSystemId());
		il.start();
	}
	
	public void deleteContact(String pContactId)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_DELETE_CLIENT, mApplication, this);
		il.setDataParam(pContactId);
		il.start();
	}
	
	public void swapToOutputs()
	{
		notificationsFragment.dismissLoader();
		systemEditFragment.dismissLoader();
		contactsFragment.dismissLoader();
		
		notificationsFragment.isDisconnected=true;
		systemEditFragment.isDisconnected=true;
		contactsFragment.isDisconnected=true;
		
		outputsFragment.isDisconnected=false;
		swapFragments(outputsFragment);
	}
	
	public void swapToNotifications()
	{
		outputsFragment.dismissLoader();
		systemEditFragment.dismissLoader();
		contactsFragment.dismissLoader();
		
		outputsFragment.isDisconnected=true;
		systemEditFragment.isDisconnected=true;
		contactsFragment.isDisconnected=true;
		
		notificationsFragment.isDisconnected=false;
		swapFragments(notificationsFragment);
	}
	
	public void swapToEdit()
	{
		outputsFragment.dismissLoader();
		notificationsFragment.dismissLoader();
		contactsFragment.dismissLoader();
		
		outputsFragment.isDisconnected=true;
		notificationsFragment.isDisconnected=true;
		contactsFragment.isDisconnected=true;
		
		systemEditFragment.isDisconnected=false;
		swapFragments(systemEditFragment);
	}
	
	public void swapToContacts()
	{
		outputsFragment.dismissLoader();
		notificationsFragment.dismissLoader();
		systemEditFragment.dismissLoader();
		
		outputsFragment.isDisconnected=true;
		notificationsFragment.isDisconnected=true;
		systemEditFragment.isDisconnected=true;
		
		contactsFragment.isDisconnected=false;
		swapFragments(contactsFragment);
	}
	
	
	
	
	/**
	 * 
	 * @param fragment
	 */
	public void swapFragments(BaseFragment fragment){
		
		latestFragment = fragment;
		
		swap_handler.removeCallbacks(swap);
		swap_handler.postDelayed(swap, 250);
		
	}
	
	/**
	 * 
	 * @param pOutputIds
	 */
	public void setOutputStatusEnable(List<String> pOutputIds)
	{
		List<Object> data = new ArrayList<Object>();
		for(int i=0; i<pOutputIds.size(); i++)
		{
			data.add(pOutputIds.get(i));
		}
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_OUTPUT_ENABLE, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	/**
	 * 
	 * @param pFilters
	 */
	public void setNotificationsFiltersEnable(List<Integer> pFilters)
	{
		Set<String> filters = new TreeSet<String>();
		JSONArray ids = new JSONArray();
		for(int i=0; i<pFilters.size(); i++)
		{
			ids.put(pFilters.get(i));
			filters.add("" + pFilters.get(i));
		}
		SpaPreference.setNotificationsFilters(getApplicationContext(), filters);
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("systemId", mApplication.getConnectedSystem().getSystemId());
		data.put("MessagesType", ids);

		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	/**
	 * 
	 */
	public void sendRegistrationToServer() {
        IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_REGISTER_FOR_NOTIFICATION, (SpaApplication)getApplication(), this);
		il.setDataParam(SpaPreference.getGCMToken(getApplicationContext()));
		il.start();
    }
	 
	/**
	 * 
	 */
	public void setNotificationsFiltersOn()
	{
		JSONArray ids = new JSONArray();
		Set<String> latestFilters = SpaPreference.getNotificationsFilters(getApplicationContext());
		if(null == latestFilters)
		{
			if(null != mApplication.getConnectedSystem().getSystemNotificationsTypes())
			{
				for(int i=0; i<mApplication.getConnectedSystem().getSystemNotificationsTypes().length; i++)
				{
					ids.put(mApplication.getConnectedSystem().getSystemNotificationsTypes()[i]);
				}
				
				Map<String, Object> data = new HashMap<String, Object>();
				data.put("systemId", mApplication.getConnectedSystem().getSystemId());
				data.put("MessagesType", ids);
	
				IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS_ON, mApplication, this);
				il.setDataParams(data);
				il.start();
			}
			else
			{
				getNotificationsFilters();
			}
		}
		else
		{
			Iterator<String> iter = latestFilters.iterator();
			while(iter.hasNext())
			{
				String id = iter.next();
				ids.put(Integer.parseInt(id));
			}
			
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("systemId", mApplication.getConnectedSystem().getSystemId());
			data.put("MessagesType", ids);

			IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS_ON, mApplication, this);
			il.setDataParams(data);
			il.start();
		}
		
		
	}
	
	
	public void setNotificationsFiltersOff()
	{
		JSONArray ids = new JSONArray();
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("systemId", mApplication.getConnectedSystem().getSystemId());
		data.put("MessagesType", ids);

		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS_OFF, mApplication, this);
		il.setDataParams(data);
		il.start(); 
		
	}
	
	public void enableRemoteAccess()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_ENABLE_REMOTE_ACCESS, mApplication, this);
		il.start();
	}
	
	public void getSystemStatus(boolean isBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_SYSTEM_STATUS, mApplication, this);
		il.setIsBackground(isBackground);
		il.start();
	}
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) throws Exception
	{
		switch (pToken) {
			case IntentLauncher.TOKEN_GET_SYSTEM_STATUS:
				JSONObject json = new JSONObject(res);
				
				if(null != mApplication.getConnectedSystem())
				{
					PanelSystem ps = mApplication.getConnectedSystem();
					ps.setFromJSON(json);
					mApplication.setConnectedSystem(ps);
				}
				
				Bundle outputUpdate = new Bundle();
				outputUpdate.putString("text", mApplication.getLocalizedString("UpdateSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_OK, outputUpdate);
				
				break;
			
			case IntentLauncher.TOKEN_SYSTEM_INSERT:
				systemEditFragment.dismissLoader();
				
				Bundle pairSuccess = new Bundle();
				pairSuccess.putString("text", mApplication.getLocalizedString("PairingSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_DISCONNECT, pairSuccess);
				break;
				
			case IntentLauncher.TOKEN_SYSTEM_UPDATE:
				
				systemEditFragment.dismissLoader();
				
				Bundle sysUpdate = new Bundle();
				sysUpdate.putString("text", mApplication.getLocalizedString("UpdateSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_DISCONNECT, sysUpdate);
				break;
				
			case IntentLauncher.TOKEN_SET_OUTPUT_ENABLE:
				getSystemStatus(false);//refresh system outputs
				break;
		
			case IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS:
				
				Bundle filtersUpdate = new Bundle();
				filtersUpdate.putString("text", mApplication.getLocalizedString("UpdateSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_OK, filtersUpdate);
				break;
				
			case IntentLauncher.TOKEN_REGISTER_FOR_NOTIFICATION:
				 SpaPreference.setSentTokenToServer(this, true);
				 setNotificationsFiltersOn();
				 break;
				
			case IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS_ON:
				Bundle filtersOnUpdate = new Bundle();
				filtersOnUpdate.putString("text", mApplication.getLocalizedString("UpdateSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_OK, filtersOnUpdate);
				break;
				
			case IntentLauncher.TOKEN_SET_NOTIFICATIONS_FILTERS_OFF:
				
				Bundle filtersOffUpdate = new Bundle();
				filtersOffUpdate.putString("text", mApplication.getLocalizedString("UpdateSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_OK, filtersOffUpdate);
				break;
	
			case IntentLauncher.TOKEN_GET_NOTIFICATIONS_TYPES:
				JSONObject jObj = new JSONObject(res);
				
				SparseArray<NotificationType> notificationsTypesMap = new SparseArray<NotificationType>();
				Iterator<String> keys = jObj.keys();
				while(keys.hasNext())
				{
					String key = keys.next();
					String value = jObj.optString(key);
					
					NotificationType nt = new NotificationType(Integer.parseInt(key), value);
					notificationsTypesMap.put(Integer.parseInt(key), nt);
				}
				
				PanelSystem ps = mApplication.getConnectedSystem();
				ps.setSystemNotificationsTypesMap(notificationsTypesMap);
				mApplication.setConnectedSystem(ps);
				
				if(mApplication.getConnectedSystem().getSystemNotifications() == null)
				{
					getNotificationsFilters();
				}
			
				break;
				
			case IntentLauncher.TOKEN_GET_NOTIFICATIONS_FILTERS:
				JSONArray activeFilters = new JSONArray(res);
				NotificationType[] filtersArray = mApplication.getConnectedSystem().getSystemNotificationsTypes();
				SystemNotificationFilter[] filters = new SystemNotificationFilter[filtersArray.length];

				int index = 0;
				for(int i=0; i<filtersArray.length; i++)
				{
					NotificationType nt = filtersArray[i];
					String filterName = nt.getName();

					boolean active = false;
					for(int j=0; j<activeFilters.length(); j++)
					{
						int id_ = activeFilters.getInt(j);
						if(nt.getNumber()==id_)
						{
							active = true;
							break;
						}
					}
					SystemNotificationFilter filter = new SystemNotificationFilter(nt.getNumber(), filterName, active);
					filters[index] = filter;
					index++;
				}
				notificationsFragment.setFilters(filters);
				break;
				
			case IntentLauncher.TOKEN_GET_CLIENTS:
				
				JSONArray contactsArray = new JSONArray(res);
				Contact[] contacts = new Contact[contactsArray.length()];

				for(int i=0; i<contactsArray.length(); i++)
				{
					JSONObject contactObj = contactsArray.getJSONObject(i);

					String id = contactObj.optString("id");
					String name = contactObj.optString("name");
					String phoneNumber = contactObj.optString("phoneNumber");
					
					Contact contact = new Contact(id, name, phoneNumber);
					contacts[i] = contact;
				}
				contactsFragment.setContacts(contacts);
			
				break;
				
			case IntentLauncher.TOKEN_DELETE_CLIENT:
				Bundle clientDelete = new Bundle();
				clientDelete.putString("text", mApplication.getLocalizedString("DeleteSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO_OK, clientDelete);
				break;
				
			case IntentLauncher.TOKEN_ENABLE_REMOTE_ACCESS:
				systemEditFragment.dismissLoader();
				break;
				
				
			case IntentLauncher.TOKEN_DISCONNECT:
				super.onSpaDisconnected();
				
				systemEditFragment.dismissLoader();
				systemEditFragment.isDisconnected=true;
				
				break;
				
			default:
				handleUncaughtHttpError(pToken, res, pIsBackground);
				break;
		}
		
	}

	
	@Override
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground) {
		
		systemEditFragment.dismissLoader();
		outputsFragment.dismissLoader();
		notificationsFragment.dismissLoader();
		contactsFragment.dismissLoader();
		
		systemEditFragment.isDisconnected = true;
		outputsFragment.isDisconnected = true;
		notificationsFragment.isDisconnected = true;
		contactsFragment.isDisconnected = true;
		
		super.handleCaughtHttpError(pToken, res, pIsBackground);
	}
	
	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		
		systemEditFragment.dismissLoader();
		outputsFragment.dismissLoader();
		notificationsFragment.dismissLoader();
		contactsFragment.dismissLoader();
		
		systemEditFragment.isDisconnected=true;
		outputsFragment.isDisconnected=true;
		notificationsFragment.isDisconnected=true;
		contactsFragment.isDisconnected=true;
		
		if(!pIsBackground)
		{
			Bundle bundle = new Bundle();
			bundle.putString("text", mApplication.getLocalizedString("Error"));
			showDialog(BaseActivity.TAG_DIALOG_INFO_ERROR, bundle);
		}
	}

	
}
