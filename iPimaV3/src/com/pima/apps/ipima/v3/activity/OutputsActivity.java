package com.pima.apps.ipima.v3.activity;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.fragment.OutputEditFragment;
import com.pima.apps.ipima.v3.fragment.OutputsListFragment;
import com.pima.apps.ipima.v3.fragment.OutputsListFragment.OutputsListCallbacks;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class OutputsActivity extends BaseActivity implements OutputsListCallbacks {

	public View moreListOverflow = null;
	private Button moreOverflow;
	public View loaderOverlay = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		Bundle extras = getIntent().getExtras();
		
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout, null);
		
		TextView titleView = (TextView)v.findViewById(R.id.titleView);
		if(titleView != null) {
			if(extras != null && extras.getBoolean(OutputEditFragment.KEY_IS_LOGGED)) {
				titleView.setText(mApplication.getLocalizedString("Outputs"));
			}else {
				titleView.setText(mApplication.getLocalizedString("AlarmSystem_Add_ManageOutput"));
			}
		}
		
		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayMoreView();
			}
		});
		
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(v);

		
		if (savedInstanceState == null) {
			OutputsListFragment fragment = OutputsListFragment.newInstance(extras);
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commitAllowingStateLoss();
		}
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
		if(keyCode==KeyEvent.KEYCODE_BACK)  
        {
        	finish();
        }else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;
    }  

	@Override
	public void onOutputAdd(String systemId) {
		Intent intent = new Intent(OutputsActivity.this, OutputEditActivity.class);
		intent.putExtra(OutputEditFragment.KEY_SYSTEM_ID, systemId);
		startActivity(intent);
	}

	@Override
	public void onOutputSelect(String systemId, long id) {
		Intent intent = new Intent(OutputsActivity.this, OutputEditActivity.class);
		intent.putExtra(OutputEditFragment.KEY_SYSTEM_ID, systemId);
		intent.putExtra(OutputEditFragment.KEY_OUTPUT_EDIT, id);
		startActivity(intent);
	}
	
	@Override	
	public void replaceLoader(View moreView) {
		dismissLoader();
		try {
			loaderOverlay = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void displayLoader() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && loaderOverlay != null) {
				
				if(loaderOverlay.getParent() == fl) {
					//fl.removeView(loaderOverlay);
				}else {
					fl.addView(loaderOverlay);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override	
	public void dismissLoader() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(loaderOverlay != null && loaderOverlay.getParent() == fl) {
					fl.removeView(loaderOverlay);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void displayMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {

	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		
	}
}
