package com.pima.apps.ipima.v3.activity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.fragment.BaseFragment;
import com.pima.apps.ipima.v3.fragment.LoginFragment;
import com.pima.apps.ipima.v3.fragment.NotificationViewNoLoginFragment;
import com.pima.apps.ipima.v3.fragment.NotificationsListNoLoginFragment;
import com.pima.apps.ipima.v3.network.IntentLauncher;
import com.pima.apps.ipima.v3.views.NotificationType;
import com.pima.apps.ipima.v3.views.PanelSystem;
import com.pima.apps.ipima.v3.views.SystemNotification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class NewLoginActivity extends BaseActivity {
	
	public static final String EXTRAS_IS_EDIT 	= "EXTRAS_IS_EDIT";
	public static final String EXTRAS_SYSTEM_ID = "EXTRAS_SYSTEM_ID"; 
	
	BaseFragment latestFragment;
	
	boolean isLogin;
	
	public LoginFragment mLoginFragment;
	public NotificationsListNoLoginFragment fragmentNotifications;
	
	private String mSystemId = null;
	private boolean mIsEditAction = false;
	public Button notificationsButton;
	private TextView titleView;
	
	public void showHelpDialog(String textToShow)
	{
		Bundle bundle = new Bundle();
		bundle.putString("text", textToShow);
		showDialog(TAG_DIALOG_HELP, bundle);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_screen);
		
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
//		Bundle extras = getIntent().getExtras();
		mIsEditAction = getIntent().getBooleanExtra(EXTRAS_IS_EDIT, false); 
		mSystemId = getIntent().getStringExtra(EXTRAS_SYSTEM_ID); 
		
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout_login, null);
		
		
		notificationsButton = (Button)v.findViewById(R.id.menu_notifications);
		notificationsButton.setText(((SpaApplication)getApplication()).getLocalizedString("Notifications"));
		
		notificationsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(isLogin)
				{
					swapFragments(fragmentNotifications); 
					notificationsButton.setText(((SpaApplication)getApplication()).getLocalizedString("Login"));
					titleView.setVisibility(View.INVISIBLE);
				}
				else
				{
					swapFragments(mLoginFragment); 
					notificationsButton.setText(((SpaApplication)getApplication()).getLocalizedString("Notifications"));
					titleView.setVisibility(View.VISIBLE);
				}
				isLogin = !isLogin;
			}
		});
		titleView = (TextView)v.findViewById(R.id.titleView);
		titleView.setText(((SpaApplication)getApplication()).getLocalizedString("Login"));
		
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(v);
		
		fragmentNotifications = new NotificationsListNoLoginFragment();
		
		if (savedInstanceState == null) {
			
			mLoginFragment = LoginFragment.newInstance(getIntent().getExtras());
			if(null == mSystemId)
			{
				isLogin = true;
				latestFragment = mLoginFragment;
			}
			else
			{
				isLogin = false;
				PanelSystem ps = new PanelSystem(mSystemId, "", -1);
				mApplication.setConnectedSystem(ps);
				latestFragment = fragmentNotifications;
			}
			
			setFragmentsDisconnect();
			getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, latestFragment).commit();
		}
	}
	
	public void login(String pPassword) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("code", pPassword);
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_AUTH, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	
	public void getSystemNotificationsTypes()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_NOTIFICATIONS_TYPES, mApplication, this);
		il.start();
	}
	
	public void getSystemNotificationsNoLogin(boolean pIsBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_NOTIFICATIONS_NO_LOGIN, mApplication, this);
		il.setIsBackground(pIsBackground);
		il.start();
	}
	
	@Override
	public void onSpaConnected() {
		
		android.util.Log.v("iPIMA", "onSpaConnected");
		
		if(mLoginFragment.canceledConnection) {
			mLoginFragment.dismissLoader();
			fragmentNotifications.dismissLoader();
			
			mLoginFragment.isDisconnected = true;
			fragmentNotifications.isDisconnected = true;
			
			return;
		}

		if(mIsEditAction)
		{
			IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_SYSTEM_STATUS, mApplication, this);
			il.start();
		}
		else
		{
			mLoginFragment.dismissLoader();
			fragmentNotifications.dismissLoader();
			
			mLoginFragment.isDisconnected = true;
			fragmentNotifications.isDisconnected = true;
			
			
			Intent intent=new Intent(getApplicationContext(), MainScreenActivity.class);
			startActivity(intent);
			
			finish();
		}
	}
	
	@Override
	public void onSpaTimeout() {

		mLoginFragment.dismissLoader();
		fragmentNotifications.dismissLoader();
		
//		mLoginFragment.isDisconnected = true;
		fragmentNotifications.isDisconnected = true;
		
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(!mApplication.disconnectLock) {

					mApplication.disconnectLock = true;
					Bundle dialogBundle = new Bundle();
					dialogBundle.putString("text", mApplication.getLocalizedString("APIError.RetriesLimitReached"));
					showDialog(TAG_DIALOG_INFO_WRONGPASSWORD, dialogBundle); //Dont go back to system list
//					mApplication.close();
					
				}
			}
		});

	}

	@Override
	public void onSpaError(String error) {

		mLoginFragment.dismissLoader();
		fragmentNotifications.dismissLoader();
		
//		mLoginFragment.isDisconnected = true;
		fragmentNotifications.isDisconnected = true;
		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(mApplication.disconnectLock == false) {
					mApplication.disconnectLock = true;
					Bundle bundle = new Bundle();
					bundle.putString("text", mApplication.getLocalizedString("APIError.NoConnection"));
					showDialog(TAG_DIALOG_INFO_ERROR, bundle);
				}
			
			}
		});
		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				mLoginFragment.connecting = false;
				mLoginFragment.canceledConnection = true;
				
				mLoginFragment.setEnabled(true);
				
			}
		});
		android.util.Log.v("iPIMA", "onSpaError --- error");
	}
	
	
    @Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
        //replaces the default 'Back' button action  
        if(keyCode==KeyEvent.KEYCODE_BACK)  
        {  
        	if(mLoginFragment.connecting) {
				mApplication.action = SpaApplication.CUR_ACTION.CANCEL_LOGIN;
//				mApplication.close();
				
				mLoginFragment.connecting = false;
				mLoginFragment.canceledConnection = true;
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
				
				mLoginFragment.setEnabled(true);
				
				

			}
        	Intent intent = new Intent(NewLoginActivity.this, SystemsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			
			NewLoginActivity.this.finish();
        }  
        return true;  
    }
    
    @Override
	public void onInfoOkClick(String tag) {
		super.onInfoOkClick(tag);
		
		mLoginFragment.dismissLoader();
		fragmentNotifications.dismissLoader();
		
//		mLoginFragment.isDisconnected = true;
		fragmentNotifications.isDisconnected = true;
		
		if(isLogin)
		{
			mLoginFragment.setEnabled(true);
		}
	}
    
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
    	try
    	{
    		
    		switch (pToken) {
				case IntentLauncher.TOKEN_AUTH:
					JSONObject auth = new JSONObject(res);
					String masterCodeInd = auth.getString("masterCodeInd");
		    		String sessionToken = auth.getString("sessionToken");
		    		
		    		mApplication.sessionToken = sessionToken;
		    		mApplication.masterCodeInd = masterCodeInd;
		    		
		    		onSpaConnected(); 
					break;
		
				case IntentLauncher.TOKEN_GET_SYSTEM_STATUS:
					JSONObject system = new JSONObject(res);
	    			PanelSystem ps = mApplication.getConnectedSystem();
	    			ps.setFromJSON(system);
	    			mApplication.setConnectedSystem(ps);
	    			
	    			if(null != latestFragment && latestFragment instanceof NotificationViewNoLoginFragment)
	    			{
	    				((NotificationViewNoLoginFragment)latestFragment).dismissLoader();
	    			}
	    			
	    			mLoginFragment.dismissLoader();
	    			fragmentNotifications.dismissLoader();
	    			
	    			mLoginFragment.isDisconnected = true;
	    			fragmentNotifications.isDisconnected = true;
	    			
	    			if(mIsEditAction)
	    			{
	    				Intent intent = new Intent(this, SystemEditActivity.class);
	    				
	    				intent.putExtra(SystemEditActivity.KEY_SYSTEM_EDIT, mApplication.getConnectedSystem().getSystemId());
	    				intent.putExtra(SystemEditActivity.KEY_SYSTEM_NAME, mApplication.getConnectedSystem().getSystemName());
	    				startActivity(intent);
	    				
	    			}
	    			else
	    			{
	    				
	    				Intent intent=new Intent(getApplicationContext(), MainScreenActivity.class);
	        			startActivity(intent);
	    			}
	    			
	    			
	    			
	    			finish();
	    			
	    		
					break;
					
				case IntentLauncher.TOKEN_GET_NOTIFICATIONS_TYPES:
		    		
					JSONObject jObj = new JSONObject(res);
					SparseArray<NotificationType> notificationsTypesMap = new SparseArray<NotificationType>();
					Iterator<String> keys = jObj.keys();
					while(keys.hasNext())
					{
						String key = keys.next();
						String value = jObj.optString(key);
					
						NotificationType nt = new NotificationType(Integer.parseInt(key), value);
						notificationsTypesMap.put(Integer.parseInt(key), nt);
					}
					
					fragmentNotifications.setNotificationsTypes(notificationsTypesMap);
					
					getSystemNotificationsNoLogin(false);
				
					break;
					
				case IntentLauncher.TOKEN_GET_NOTIFICATIONS_NO_LOGIN:
					
	    			JSONArray notificationsArray = new JSONArray(res);
	    			
	    			SystemNotification [] systemNotifications = null;
	    			if(null != notificationsArray)
	    			{
	    				systemNotifications = new SystemNotification[notificationsArray.length()];
	    				for(int i=0; i<notificationsArray.length(); i++)
	    				{
	    					JSONObject notificationJson = notificationsArray.optJSONObject(i);
	    					SystemNotification sn = SystemNotification.fromJson(notificationJson);
	    					systemNotifications[i] = sn;
	    				}
	    			}
//	    			Arrays.sort(systemNotifications, new SystemNotificationComparator());
	    			fragmentNotifications.setNotifications(systemNotifications);
	    			
	    			if(null != latestFragment && latestFragment instanceof NotificationViewNoLoginFragment)
	    			{
	    				((NotificationViewNoLoginFragment)latestFragment).dismissLoader();
	    			}
	    			
    				mLoginFragment.dismissLoader();
    				fragmentNotifications.dismissLoader();
	    		
					break;
					
				default:
					break;
			}
    		
	    	
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		handleUncaughtHttpError(pToken, res, pIsBackground);
    	}
		
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		if(pToken == IntentLauncher.TOKEN_AUTH)
		{
			mLoginFragment.connecting = false;
		}
		
		if(null != latestFragment && latestFragment instanceof NotificationViewNoLoginFragment)
		{
			((NotificationViewNoLoginFragment)latestFragment).dismissLoader();
		}
		
		mLoginFragment.dismissLoader();
		fragmentNotifications.dismissLoader();
	}
	
	@Override
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground) {
		mLoginFragment.connecting = false;
		
		if(null != latestFragment && latestFragment instanceof NotificationViewNoLoginFragment)
		{
			((NotificationViewNoLoginFragment)latestFragment).dismissLoader();
		}
		mLoginFragment.dismissLoader();
		fragmentNotifications.dismissLoader();
		
		super.handleCaughtHttpError(pToken, res, pIsBackground);
	}
	
	/**
	 * 
	 * @param fragment
	 */
	public void swapFragments(BaseFragment fragment){
		
		if(latestFragment == null) {
			latestFragment = fragment;
		}else if(fragment == latestFragment) {
			return;
		}
		latestFragment = fragment;
		setFragmentsDisconnect();
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_container, fragment);
		fragmentTransaction.commitAllowingStateLoss();
		
	}
	
	/**
	 * 
	 */
	private void setFragmentsDisconnect() {
		
		latestFragment.isDisconnected = false;
		
		if(!latestFragment.equals(fragmentNotifications) )
		{
			fragmentNotifications.isDisconnected = true;
		}
		
		if(!latestFragment.equals(mLoginFragment) )
		{
			mLoginFragment.isDisconnected = true;
		}
		
	}

}
