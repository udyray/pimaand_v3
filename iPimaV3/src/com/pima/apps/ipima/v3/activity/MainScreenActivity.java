package com.pima.apps.ipima.v3.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.fragment.BaseFragment;
import com.pima.apps.ipima.v3.fragment.BypassZonesFragment;
import com.pima.apps.ipima.v3.fragment.FaultsListFragment;
import com.pima.apps.ipima.v3.fragment.LogListFragment;
import com.pima.apps.ipima.v3.fragment.LookinFragment;
import com.pima.apps.ipima.v3.fragment.NotificationViewFragment;
import com.pima.apps.ipima.v3.fragment.NotificationViewNoLoginFragment;
import com.pima.apps.ipima.v3.fragment.NotificationsListFragment;
import com.pima.apps.ipima.v3.fragment.OutputEditFragment;
import com.pima.apps.ipima.v3.fragment.OutputsListFragment;
import com.pima.apps.ipima.v3.fragment.PartitionsFragment;
import com.pima.apps.ipima.v3.fragment.SystemStateFragment;
import com.pima.apps.ipima.v3.fragment.ZonesFragment;
import com.pima.apps.ipima.v3.fragment.OutputsListFragment.OutputsListCallbacks;
import com.pima.apps.ipima.v3.network.IntentLauncher;
import com.pima.apps.ipima.v3.views.NotificationType;
import com.pima.apps.ipima.v3.views.PanelSystem;
import com.pima.apps.ipima.v3.views.PanelSystem.TYPES_SYSTEM_STATUS;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

public class MainScreenActivity extends BaseActivity implements OutputsListCallbacks {
	public static final String LOG_TAG = "MainScreenActivity";
	
//	public static final long writeDelay = 100;
	
	ActionBar actionBar;
	public SystemStateFragment fragmentSystemState;
	public LogListFragment logsFragment;
	public ZonesFragment zonesFragment;
	public NotificationsListFragment fragmentNotifications;
	public FaultsListFragment fragmentFaults;
	public OutputsListFragment outputsFragment;
	public BypassZonesFragment bypassZonesFragment;
	public LookinFragment lookinFragment;
	public PartitionsFragment partitionsFragment;
	
	public View moreListOverflow = null;
	
	public View loaderOverlay = null;
	
	private TextView system;
	private TextView zones;
	private TextView notifications;
	private TextView outputs;
	private Button moreOverflow;
	
	private HorizontalScrollView actionBarScroll;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_screen);
		actionBar=getSupportActionBar();
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		width = (int)Math.round((width*0.75)/(4));
		
//		DisplayMetrics metrics = new DisplayMetrics();
//		getWindowManager().getDefaultDisplay().getMetrics(metrics);
//		float logicalDensity = metrics.density;
		
//		int screen_width = getResources().getDisplayMetrics().widthPixels;
//		double screen_width_dp = ((double) screen_width) / logicalDensity;
		
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.menu_layout, null);
		
		actionBarScroll = (HorizontalScrollView)v.findViewById(R.id.horozontalView);
		
		if(	mApplication.getLocalization() != null && 
			mApplication.getLocalization().equals("he")) {
			
//			llab = (LinearLayout)v.findViewById(R.id.llab);
			
			if(actionBarScroll.getWidth() < 400) {
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						if(	mApplication.getLocalization() != null && 
							mApplication.getLocalization().equals("he")) {
								actionBarScroll.scrollTo(400, 0);
						}
					}
				}, 1000);
			}
		}
		
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(v);
		
		
		system = (TextView)v.findViewById(R.id.system_item);
		system.setSelected(true);
		system.setText(mApplication.getLocalizedString("System"));
		LayoutParams params = system.getLayoutParams();
		params.width = width;
		system.setLayoutParams(params);
		system.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {
					system.setSelected(true);
					zones.setSelected(false);
					notifications.setSelected(false);
					//!!faults.setSelected(false);
					//!!logs.setSelected(false);
					
					outputs.setSelected(false);

					system.setTextColor(getResources().getColor(R.color.tab_selected_color));
					zones.setTextColor(getResources().getColor(R.color.black));
					notifications.setTextColor(getResources().getColor(R.color.black));
					//!!faults.setTextColor(getResources().getColor(R.color.black));
					//!!logs.setTextColor(getResources().getColor(R.color.black));
					outputs.setTextColor(getResources().getColor(R.color.black));
					fragmentNotifications.dismissLoader();
					fragmentFaults.dismissLoader();
					zonesFragment.dismissLoader();
					bypassZonesFragment.dismissLoader();
					partitionsFragment.dismissLoader();
					logsFragment.dismissLoader();
					outputsFragment.dismissLoader();
					lookinFragment.dismissLoader();
					onActionBarItemSelected(system);
				}
			}
		});
		
		zones = (TextView)v.findViewById(R.id.zones_item);
		zones.setText(mApplication.getLocalizedString("Zones"));
		params = zones.getLayoutParams();
		params.width = width;
		zones.setLayoutParams(params);
	    zones.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {

					system.setSelected(false);
					zones.setSelected(true);
					notifications.setSelected(false);
					//!!faults.setSelected(false);
					//!!logs.setSelected(false);
					outputs.setSelected(false);

					system.setTextColor(getResources().getColor(R.color.black));
					zones.setTextColor(getResources().getColor(R.color.tab_selected_color));
					notifications.setTextColor(getResources().getColor(R.color.black));
					//!!faults.setTextColor(getResources().getColor(R.color.black));
					//!!logs.setTextColor(getResources().getColor(R.color.black));
					outputs.setTextColor(getResources().getColor(R.color.black));
					fragmentNotifications.dismissLoader();
					fragmentFaults.dismissLoader();
					logsFragment.dismissLoader();
					outputsFragment.dismissLoader();
					lookinFragment.dismissLoader();
					fragmentSystemState.dismissLoader();
					onActionBarItemSelected(zones);
				}
			}
		});
	    
	    notifications = (TextView)v.findViewById(R.id.notifications_item);
	    notifications.setText(mApplication.getLocalizedString("Notifications"));
	    
	    params = notifications.getLayoutParams();
		params.width = width;
		notifications.setLayoutParams(params);
	    notifications.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {

					system.setSelected(false);
					zones.setSelected(false);
					notifications.setSelected(true);
					//!!faults.setSelected(false);
					//!!logs.setSelected(false);
					outputs.setSelected(false);

					system.setTextColor(getResources().getColor(R.color.black));
					zones.setTextColor(getResources().getColor(R.color.black));
					notifications.setTextColor(getResources().getColor(R.color.tab_selected_color));
					//!!faults.setTextColor(getResources().getColor(R.color.black));
					//!!logs.setTextColor(getResources().getColor(R.color.black));
					outputs.setTextColor(getResources().getColor(R.color.black));
					zonesFragment.dismissLoader();
					bypassZonesFragment.dismissLoader();
					partitionsFragment.dismissLoader();
					logsFragment.dismissLoader();
					outputsFragment.dismissLoader();
					lookinFragment.dismissLoader();
					fragmentSystemState.dismissLoader();
					onActionBarItemSelected(notifications);
				}
			}
		});
		/*** //!!
		faults = (TextView)v.findViewById(R.id.faults_item);
		faults.setText(mApplication.getLocalizedString("Faults"));
		faults.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {

					system.setSelected(false);
					zones.setSelected(false);
					notifications.setSelected(false);
					faults.setSelected(true);
					logs.setSelected(false);
					outputs.setSelected(false);

					system.setTextColor(getResources().getColor(R.color.black));
					zones.setTextColor(getResources().getColor(R.color.black));
					notifications.setTextColor(getResources().getColor(R.color.black));
					faults.setTextColor(getResources().getColor(R.color.tab_selected_color));
					logs.setTextColor(getResources().getColor(R.color.black));
					outputs.setTextColor(getResources().getColor(R.color.black));
					zonesFragment.dismissLoader();
					bypassZonesFragment.dismissLoader();
					partitionsFragment.dismissLoader();
					logsFragment.dismissLoader();
					outputsFragment.dismissLoader();
					lookinFragment.dismissLoader();
					fragmentSystemState.dismissLoader();
					onActionBarItemSelected(faults);
				}
			}
		});//!!
		
		logs = (TextView)v.findViewById(R.id.log_item);
		logs.setText(mApplication.getLocalizedString("Log"));
		logs.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {

					system.setSelected(false);
					zones.setSelected(false);
					notifications.setSelected(false);
					faults.setSelected(false);
					logs.setSelected(true);
					outputs.setSelected(false);

					system.setTextColor(getResources().getColor(R.color.black));
					zones.setTextColor(getResources().getColor(R.color.black));
					notifications.setTextColor(getResources().getColor(R.color.black));
					faults.setTextColor(getResources().getColor(R.color.black));
					logs.setTextColor(getResources().getColor(R.color.tab_selected_color));
					outputs.setTextColor(getResources().getColor(R.color.black));
					fragmentNotifications.dismissLoader();
					fragmentFaults.dismissLoader();
					zonesFragment.dismissLoader();
					bypassZonesFragment.dismissLoader();
					partitionsFragment.dismissLoader();
					outputsFragment.dismissLoader();
					lookinFragment.dismissLoader();
					fragmentSystemState.dismissLoader();
					onActionBarItemSelected(logs);
				}
			}
		});//!!
		***/
		outputs = (TextView)v.findViewById(R.id.outputs_item);
		outputs.setText(mApplication.getLocalizedString("Outputs"));
		params = outputs.getLayoutParams();
		params.width = width;
		outputs.setLayoutParams(params);
		outputs.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {

					system.setSelected(false);
					zones.setSelected(false);
					notifications.setSelected(false);
					//!!faults.setSelected(false);
					//!!logs.setSelected(false);
					outputs.setSelected(true);

					system.setTextColor(getResources().getColor(R.color.black));
					zones.setTextColor(getResources().getColor(R.color.black));
					notifications.setTextColor(getResources().getColor(R.color.black));
					//!!faults.setTextColor(getResources().getColor(R.color.black));
					//!!logs.setTextColor(getResources().getColor(R.color.black));
					outputs.setTextColor(getResources().getColor(R.color.tab_selected_color));
					fragmentNotifications.dismissLoader();
					fragmentFaults.dismissLoader();
					zonesFragment.dismissLoader();
					bypassZonesFragment.dismissLoader();
					partitionsFragment.dismissLoader();
					logsFragment.dismissLoader();
					fragmentSystemState.dismissLoader();
					onActionBarItemSelected(outputs);
				}
			}
		});
		
		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickBarrier(v)) {
					displayMoreView();
				}
			}
		});
		
		
		logsFragment = new LogListFragment();
		zonesFragment = new ZonesFragment();
		bypassZonesFragment = new BypassZonesFragment();
		partitionsFragment = new PartitionsFragment();
		fragmentNotifications = new NotificationsListFragment();
		fragmentFaults = new FaultsListFragment();
		outputsFragment = OutputsListFragment.newInstance(getIntent().getExtras());
		lookinFragment = new LookinFragment();
		fragmentSystemState=new SystemStateFragment(this);
		
		//mApplication.sendGeneral(new SendSystemStatus());
		
		//get the notifications Types
		if(mApplication.getConnectedSystem().getSystemNotificationsTypesMap() == null)
		{
			getSystemNotificationsTypes();
		}
	}
	
	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		latestFragment = fragmentSystemState;
		setFragmentsDisconnect();
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		fragmentTransaction.add(R.id.fragment_container, fragmentSystemState);
		fragmentTransaction.commitAllowingStateLoss();
	}



	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
        //replaces the default 'Back' button action  
        if(keyCode==KeyEvent.KEYCODE_BACK)  
        {  
        	if(latestFragment instanceof BypassZonesFragment)
        	{
        		swapFragments(zonesFragment);
        		mApplication.isBypassZonesFragment = false;
        	}
        	else if(latestFragment instanceof NotificationViewFragment || 
        			latestFragment instanceof NotificationViewNoLoginFragment)
        	{
        		swapFragments(fragmentNotifications);
        	}
        	else
        	{
				Bundle bundle = new Bundle();
				bundle.putString("title", mApplication.getLocalizedString("confirm_disconnect"));
				showDialog(TAG_DIALOG_WARNING, bundle);
        	}
        	
        }else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;  
    }  
    
    
	
	@Override
	public void onInfoOkClick(String tag) {
		super.onInfoOkClick(tag);
		
		if (TAG_DIALOG_WARNING.equals(tag)) {
			mApplication.action = SpaApplication.CUR_ACTION.LOGOUT;
			disconnect();
		}
		else if(TAG_DIALOG_INFO_ERROR.equals(tag)) {
			mApplication.action = SpaApplication.CUR_ACTION.LOGOUT;
			disconnect();
		}
		else if(TAG_DIALOG_INFO_OK.equals(tag)) 
		{
			getSystemNotifications(true);
		}
	}

	
	@Override
	public void onPause() {
		super.onPause();
		
		mApplication.action = SpaApplication.CUR_ACTION.LOGOUT;
		disconnect();
		
	}

	/**
	 * 
	 * @param item
	 */
	public void onActionBarItemSelected(View item) {
		switch (item.getId()) {
		case R.id.system_item:
			swapFragments(fragmentSystemState);
			return;
		case R.id.zones_item:
			swapFragments(zonesFragment);
			return;
		case R.id.notifications_item:
			swapFragments(fragmentNotifications);
			return;
//!!	case R.id.faults_item:
//!!		swapFragments(fragmentFaults);
//!!		return;
//!!	case R.id.log_item:
//!!		swapFragments(logsFragment);
//!!		return;
		case R.id.outputs_item:
			swapFragments(outputsFragment);
			return;
			
		}
	}
	
	@Override	
	public void replaceLoader(View moreView) {
		dismissLoader();
		try {
			loaderOverlay = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void displayLoader() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && loaderOverlay != null) {
				
				if(loaderOverlay.getParent() == fl) {
					//fl.removeView(loaderOverlay);
				}else {
					fl.addView(loaderOverlay);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override	
	public void dismissLoader() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(loaderOverlay != null && loaderOverlay.getParent() == fl) {
					fl.removeView(loaderOverlay);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void displayMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	
	@Override
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void onOutputAdd(String systemId) {
		Intent intent = new Intent(MainScreenActivity.this, OutputEditActivity.class);
		intent.putExtra(OutputEditFragment.KEY_SYSTEM_ID, systemId);
		intent.putExtra(OutputEditFragment.KEY_IS_LOGGED, true);
		startActivity(intent);
	}

	@Override
	public void onOutputSelect(String systemId, long id) {
		Intent intent = new Intent(MainScreenActivity.this, OutputEditActivity.class);
		intent.putExtra(OutputEditFragment.KEY_SYSTEM_ID, systemId);
		intent.putExtra(OutputEditFragment.KEY_OUTPUT_EDIT, id);
		intent.putExtra(OutputEditFragment.KEY_IS_LOGGED, true);
		startActivity(intent);
	}
	
	@Override
	public void onSpaDisconnected() {
		super.onSpaDisconnected();
		
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		fragmentSystemState.dismissLoader();
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		logsFragment.dismissLoader();
		
		setFragmentsDisconnect();
	}
	
	@Override
	public void onSpaError(String error) {
		Log.v("iPIMA", "------ error: " + error);
		
		hideDialog(TAG_DIALOG_LOADING);
		if(null != dialog)
		{
			dismissLoader();
		}
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		fragmentSystemState.dismissLoader();
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		logsFragment.dismissLoader();

		setFragmentsDisconnect();
		
		if(mApplication.disconnectLock == false) {
			mApplication.disconnectLock = true;
			Bundle bundle = new Bundle();
			bundle.putString("text", mApplication.getLocalizedString("NetworkError"));
			showDialog(TAG_DIALOG_INFO_ERROR, bundle);
			
		}
		else
		{
			disconnect();
		}
		
		
		
	}
	
	@Override
	public void onSpaTimeout() {
		super.onSpaTimeout();
		
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		fragmentSystemState.dismissLoader();
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		logsFragment.dismissLoader();
		
		setFragmentsDisconnect();

	}

	/**
	 * 
	 * @param fragment
	 */
	public void swapFragments(BaseFragment fragment){
		
		if(latestFragment == null) {
			latestFragment = fragment;
		}else if(fragment == latestFragment) {
			return;
		}
		latestFragment = fragment;
		
		//send abort request
		abort();
		
		setFragmentsDisconnect();
		
		swap_handler.removeCallbacks(swap);
		swap_handler.postDelayed(swap, 250);
	
		
	}
	

	/**
	 * 
	 */
	private void setFragmentsDisconnect() {
		
		latestFragment.dismissLoader();
		latestFragment.isDisconnected = false;
		
		if(!latestFragment.equals(fragmentNotifications) )
		{
			fragmentNotifications.isDisconnected = true;
		}
		
		if(!latestFragment.equals(fragmentFaults) )
		{
			fragmentFaults.isDisconnected = true;
		}
		
		if(!latestFragment.equals(fragmentSystemState) )
		{
			fragmentSystemState.isDisconnected = true;
		}
		
		if(!latestFragment.equals(zonesFragment) )
		{
			zonesFragment.isDisconnected = true;
		}
		
		if(!latestFragment.equals(bypassZonesFragment) )
		{
			bypassZonesFragment.isDisconnected = true;
		}
		
		if(!latestFragment.equals(partitionsFragment) )
		{
			partitionsFragment.isDisconnected = true;
		}
		
		if(!latestFragment.equals(outputsFragment) )
		{
			outputsFragment.isDisconnected = true;
		}
		
		if(!latestFragment.equals(logsFragment) )
		{
			logsFragment.isDisconnected = true;
		}
		
		if(!latestFragment.equals(lookinFragment) )
		{
			lookinFragment.isDisconnected = true;
		}
		
		
	}


	public void loadBypassZonesFragment()
	{
		if(null == bypassZonesFragment)
		{
			bypassZonesFragment = new BypassZonesFragment();
		}
		swapFragments(bypassZonesFragment);
	}

	public void getSystemStatus(boolean isBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_SYSTEM_STATUS, mApplication, this);
		il.setIsBackground(isBackground);
		il.start();
	}
	
	public void getSystemLog(boolean pIsBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_LOG, mApplication, this);
		il.setIsBackground(pIsBackground);
		il.start();
	}
	
	public void getSystemNotificationsTypes()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_NOTIFICATIONS_TYPES, mApplication, this);
		il.start();
	}
	
	
	public void getSystemNotifications(boolean pIsBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_NOTIFICATIONS, mApplication, this);
		il.setIsBackground(pIsBackground);
		il.start();
	}
	
	public void getSystemFaults(boolean pIsBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_FAULTS, mApplication, this);
		il.setIsBackground(pIsBackground);
		il.start();
	}
	
	public void getOutputsStatus(boolean pIsBackground)
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_OUTPUT_STATUS, mApplication, this);
		il.setIsBackground(pIsBackground);
		il.start();
	}
	
	
	public void stopSiren()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_STOP_SIREN, mApplication, this);
		il.start();
	}
	
	
	public void setPartitionStatus(String pPartitionId, TYPES_SYSTEM_STATUS pStatus)
	{
		abort();
		
		int token = -1;
		if(pStatus.ordinal() == TYPES_SYSTEM_STATUS.Disarm.ordinal())
		{
			token = IntentLauncher.TOKEN_SET_PARTITION_STATUS_DISARM;
		}
		else if(pStatus.ordinal() == TYPES_SYSTEM_STATUS.Full.ordinal())
		{
			token = IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_FULL;
		}
		else if(pStatus.ordinal() == TYPES_SYSTEM_STATUS.Home1.ordinal())
		{
			token = IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_1;
		}
		else if(pStatus.ordinal() == TYPES_SYSTEM_STATUS.Home2.ordinal())
		{
			token = IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_2;
		}
		else if(pStatus.ordinal() == TYPES_SYSTEM_STATUS.Home3.ordinal())
		{
			token = IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_3;
		}
		else if(pStatus.ordinal() == TYPES_SYSTEM_STATUS.Home4.ordinal())
		{
			token = IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_4;
		}
		
		
		List<Object> data = new ArrayList<Object>();
		
		JSONObject obj = new JSONObject();
		try
		{
			obj.put("id", pPartitionId);
			obj.put("status", pStatus.ordinal());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		data.add(obj);
		IntentLauncher il = new IntentLauncher(token, mApplication, this);
		il.setDataParams(data);
		
		il.start();
	}
	
	/**
	 * 
	 * @param pOutputId
	 * @param pActive
	 */
	public void setOutputStatus(String pOutputId, boolean pActive)
	{
		List<Object> data = new ArrayList<Object>();
		try
		{
			JSONObject obj = new JSONObject();
			obj.put("id", pOutputId);
			obj.put("active", pActive);
			
			data.add(obj);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_OUTPUT_STATUS_ACTIVE, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	/**
	 * 
	 * @param zoneNumber
	 * @param bypass
	 */
	public void bypassZone(int[] zoneNumber, boolean[] bypass) {
		
		bypassZonesFragment.startLoader();
		
		List<Object> data = new ArrayList<Object>();
		try
		{
			for(int i=0; i<zoneNumber.length; i++)
			{
				int id = zoneNumber[i];
				boolean bp = bypass[i];
				
				JSONObject obj = new JSONObject();
				obj.put("number", id);
				obj.put("bypass", bp);
				
				data.add(obj);
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		bypassZonesFragment.selectedRow = zoneNumber;
		
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_ZONE_BYPASS, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	public void lookinZone(int zoneNumber) {
		
		lookinFragment.startLoader();
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_LOOKIN_ZONE, mApplication, this);
		il.setDataParam(zoneNumber);
		il.start();
	}
	
	
	/**
	 * 
	 */
	public void selectFaults()
	{
		system.setSelected(false);
		zones.setSelected(false);
		notifications.setSelected(false);
		outputs.setSelected(false);

		system.setTextColor(getResources().getColor(R.color.black));
		zones.setTextColor(getResources().getColor(R.color.black));
		notifications.setTextColor(getResources().getColor(R.color.black));
		outputs.setTextColor(getResources().getColor(R.color.black));
		
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		logsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		fragmentSystemState.dismissLoader();
		
		swapFragments(fragmentFaults);
	}
	
	/**
	 * 
	 */
	public void selectLogs()
	{
		system.setSelected(false);
		zones.setSelected(false);
		notifications.setSelected(false);
		outputs.setSelected(false);

		system.setTextColor(getResources().getColor(R.color.black));
		zones.setTextColor(getResources().getColor(R.color.black));
		notifications.setTextColor(getResources().getColor(R.color.black));
		outputs.setTextColor(getResources().getColor(R.color.black));
		
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		fragmentSystemState.dismissLoader();
		
		swapFragments(logsFragment);
	}
	
	
	/**
	 * 
	 */
	public void selectLookin()
	{
		system.setSelected(false);
		zones.setSelected(false);
		notifications.setSelected(false);
		outputs.setSelected(false);

		system.setTextColor(getResources().getColor(R.color.black));
		zones.setTextColor(getResources().getColor(R.color.black));
		notifications.setTextColor(getResources().getColor(R.color.black));
		outputs.setTextColor(getResources().getColor(R.color.black));
		
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		zonesFragment.dismissLoader(); 
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		logsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		fragmentSystemState.dismissLoader();
		
		swapFragments(lookinFragment);
	}
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
		try
    	{
			switch (pToken) {
				case IntentLauncher.TOKEN_GET_SYSTEM_STATUS:
	    			JSONObject json = new JSONObject(res);
	    			
	    			if(null != mApplication.getConnectedSystem())
	    			{
	    				PanelSystem ps = mApplication.getConnectedSystem();
	    				ps.setFromJSON(json);
	    				mApplication.setConnectedSystem(ps);
	    						
	//    				system.setSystemFaults(mApplication.connectedSystem.getSystemFaults());
	//    				system.setSystemLog(mApplication.connectedSystem.getSystemLog());
	    			}
	    			
	    			fragmentSystemState.setPanelSystem(mApplication.getConnectedSystem());
	    			
	    			fragmentSystemState.refreshScreen();
	    			zonesFragment.refreshScreen();
	    			outputsFragment.refreshScreen();
	    			bypassZonesFragment.refreshScreen();
	    			
	    			break;
	
				case IntentLauncher.TOKEN_GET_NOTIFICATIONS_TYPES:
	
	    			JSONObject jObj = new JSONObject(res);
	    			
	    			SparseArray<NotificationType> notificationsTypesMap = new SparseArray<NotificationType>();
	    			Iterator<String> keys = jObj.keys();
	    			while(keys.hasNext())
	    			{
	    				String key = keys.next();
	    				String value = jObj.optString(key);
	    			
	    				NotificationType nt = new NotificationType(Integer.parseInt(key), value);
	    				notificationsTypesMap.put(Integer.parseInt(key), nt);
	    			}
	    			
	    			PanelSystem ps = mApplication.getConnectedSystem();
	    			if(null != ps)
	    			{
	    				ps.setSystemNotificationsTypesMap(notificationsTypesMap);
	    				mApplication.setConnectedSystem(ps);
	    				
		    			if(mApplication.getConnectedSystem().getSystemNotifications() == null)
		    			{
		    				getSystemNotifications(false);
		    			}
	    			}
					break;
					
				case IntentLauncher.TOKEN_GET_NOTIFICATIONS:

	    			JSONArray notificationsArray = new JSONArray(res);
	    			
	    			ps = mApplication.getConnectedSystem();
	    			if(null != ps)
	    			{
	    				ps.setSystemNotifications(notificationsArray);
	    				mApplication.setConnectedSystem(ps);
	    			}
	    			fragmentNotifications.refreshScreen();
	    			break;
					
				case IntentLauncher.TOKEN_GET_FAULTS:
	
	    			JSONArray faultsArray = new JSONArray(res);
	    			
	    			ps = mApplication.getConnectedSystem();
	    			if(null != ps)
	    			{
	    				ps.setSystemFaults(faultsArray);
	    				mApplication.setConnectedSystem(ps);
	    				
	    			}
	    			fragmentFaults.refreshScreen();
	    			break;
	    			
				case IntentLauncher.TOKEN_GET_OUTPUT_STATUS:
	
	    			JSONArray outputsArray = new JSONArray(res);
	    			ps = mApplication.getConnectedSystem();
	    			if(null != ps)
	    			{
		    			for(int i=0; i<outputsArray.length(); i++)
		    			{
		    				JSONObject output = outputsArray.getJSONObject(i);
		    				String id_ = output.optString("id");
		    				int id = Integer.parseInt(id_);
		    				boolean active = output.optBoolean("active");
		    				ps.setSystemOutputActive(id, active);
		    			}
		    			mApplication.setConnectedSystem(ps);
	    			}
	    			outputsFragment.refreshScreen();
	    			break;
	    			
				case IntentLauncher.TOKEN_GET_LOG:
	
	    			JSONArray logArray = new JSONArray(res);
	    			
	    			ps = mApplication.getConnectedSystem();
	    			if(null != ps)
	    			{
	    				ps.setSystemLog(logArray);
	    				mApplication.setConnectedSystem(ps);
	    			}
	    			logsFragment.refreshScreen();
	    			break;
					
				case IntentLauncher.TOKEN_SET_PARTITION_STATUS_DISARM:
				case IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_1:
				case IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_2:
				case IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_3:
				case IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_HOME_4:
				case IntentLauncher.TOKEN_SET_PARTITION_STATUS_ARM_FULL:
				case IntentLauncher.TOKEN_SET_ZONE_BYPASS:
				case IntentLauncher.TOKEN_STOP_SIREN:
					
					getSystemStatus(false);
	    			break;
	    			
				case IntentLauncher.TOKEN_SET_OUTPUT_STATUS_ACTIVE:
					getOutputsStatus(false);
					break;
					
				case IntentLauncher.TOKEN_SET_LOOKIN_ZONE:
					lookinFragment.dismissLoader();
					
					Bundle bundle = new Bundle();
					bundle.putString("text", mApplication.getLocalizedString("OK"));
					showDialog(TAG_DIALOG_INFO_OK, bundle);
					break;
					
				case IntentLauncher.TOKEN_DISCONNECT:
					onSpaDisconnected();
					break;
				
				default:
					System.out.println("DEFAULT????");
					handleUncaughtHttpError(pToken, res, pIsBackground);
					break;
			}
			
    		
    	}
    	catch(Exception e){e.printStackTrace();
    		handleUncaughtHttpError(pToken, res, pIsBackground);
    	}
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		fragmentSystemState.dismissLoader();
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		logsFragment.dismissLoader();
		lookinFragment.dismissLoader();
		
		fragmentNotifications.isDisconnected = true;
		fragmentFaults.isDisconnected = true;
		fragmentSystemState.isDisconnected = true;
		zonesFragment.isDisconnected = true;
		bypassZonesFragment.isDisconnected = true;
		partitionsFragment.isDisconnected = true;
		outputsFragment.isDisconnected = true;
		logsFragment.isDisconnected = true;
		lookinFragment.isDisconnected = true;
		
		onSpaError(res);
	}
	
	@Override
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground) {
		
		fragmentNotifications.dismissLoader();
		fragmentFaults.dismissLoader();
		fragmentSystemState.dismissLoader();
		zonesFragment.dismissLoader();
		bypassZonesFragment.dismissLoader();
		partitionsFragment.dismissLoader();
		outputsFragment.dismissLoader();
		logsFragment.dismissLoader();
		lookinFragment.dismissLoader();
		
//		fragmentNotifications.isDisconnected = true;
//		fragmentFaults.isDisconnected = true;
//		fragmentSystemState.isDisconnected = true;
//		zonesFragment.isDisconnected = true;
//		bypassZonesFragment.isDisconnected = true;
//		partitionsFragment.isDisconnected = true;
//		outputsFragment.isDisconnected = true;
//		logsFragment.isDisconnected = true;
//		lookinFragment.isDisconnected = true;
		
		super.handleCaughtHttpError(pToken, res, pIsBackground);
		
		
//		if(	pToken == IntentLauncher.TOKEN_SET_OUTPUT_STATUS_ACTIVE ||
//			pToken == IntentLauncher.TOKEN_GET_SYSTEM_STATUS)
//		{
//			getSystemStatus();
//		}
		
	}
	
}
