package com.pima.apps.ipima.v3.activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.SpaPreference;
import com.pima.apps.ipima.v3.gcm.RegistrationIntentService;
import com.pima.apps.ipima.v3.network.IntentLauncher;
import com.pima.apps.ipima.v3.network.PimaProtocol;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SplashActivity extends BaseActivity {
	
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static String TAG = SplashActivity.class.getName();
	
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	
	private String mSystemId = null;
	Dialog dialog = null;
	
	Runnable dismiss = new Runnable() {
		
		@Override
		public void run() {
			if(dialog != null)
				try{
					dialog.dismiss();
				}
				catch(Exception e){}
		}
	}; 
	
	Handler dismiss_handler = new Handler();
	
	public void dismissLoader() {
		dismiss_handler.removeCallbacks(dismiss);
		dismiss_handler.postDelayed(dismiss, 500);
	}
	
	
	public void startLoader() {
		if(dialog != null) {
			try{
				dialog.show();
			}
			catch(Exception e){}
			dismiss_handler.removeCallbacks(dismiss);
		}
	}
	
	LinearLayout layout = null;
	TextView titleNetworkError = null;
	TextView reconnect = null;
	
	private String mTextURL = null;
	private long mTextsTimestamp = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);    // Removes title bar
		
		mSystemId = getIntent().getStringExtra(NewLoginActivity.EXTRAS_SYSTEM_ID); 
		
		super.onCreate(savedInstanceState);
		
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               
                boolean sentToken = SpaPreference.getSentTokenToServer(context);
                if (sentToken) {
                	System.out.println("Token Was sent to Server");
                } else {
                    System.out.println("Token Was NOT sent to Server");
                }
            }
        };
        
       
	 // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);    // Removes notification bar

		setContentView(R.layout.splash);
		
		layout = (LinearLayout)findViewById(R.id.networkError);
		titleNetworkError = (TextView)layout.findViewById(R.id.titleNetworkError);
		reconnect = (TextView)layout.findViewById(R.id.reconnect);
		OnClickListener ocl = new OnClickListener() {
		
			@Override
			public void onClick(View v) {
				layout.setVisibility(View.GONE);
				
				if(dialog != null)
				{
					startLoader();
				}
				
				// Start timer and launch main activity
				IntentLauncher launcher = new IntentLauncher(IntentLauncher.TOKEN_CONNECTION_STATE_CONNECT_SERVER, mApplication, SplashActivity.this);
				launcher.start();
			}
		};
		reconnect.setOnClickListener(ocl);
	
	}

	 

	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
		
		if(dialog != null)
		{
			dismissLoader();
		}
		
		switch (pToken) {
			case IntentLauncher.TOKEN_REGISTER_FOR_NOTIFICATION:
				SpaPreference.setSentTokenToServer(this, true);
				break;
	
			case IntentLauncher.TOKEN_CONNECTION_STATE_CONNECT_SERVER:
				 
				try
				{
					JSONObject json = new JSONObject(res);
					mTextURL = json.optString("texts");
					if(null != mTextURL)
					{
						String textsTimestamp = json.optString("textsTimestamp");
						if(null != textsTimestamp)
						{
							Date d = PimaProtocol.parseDate(textsTimestamp);
							if(null != d)
							{
								mTextsTimestamp = d.getTime();
							}
						}
					}
				
					
					boolean isNeedToDownload = true;
					//check if we need to download the text file
					if(null != mTextURL && -1 != mTextsTimestamp)
					{
						long langDate = SpaPreference.getLanguageDate(getApplicationContext(), PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage()));
						if(mTextsTimestamp <= langDate)
						{
							File file = null;
							try 
							{
							    file = new File(getCacheDir(), PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage()));
							    if(file.exists() && file.isFile())
							    {
							    	isNeedToDownload = false;
							    }
							    
							}
							catch(Exception e){
								isNeedToDownload = true;
							}
							
						}
					}
					
					
					String applicationServer = json.optString("applicationServer");
					int retriesNumber = json.optInt("retriesNumber", -1);
					long retriesIntervalInSec = json.optLong("retriesIntervalInSec", -1);
					
					if(null != applicationServer)
					{
						mApplication.setServerUrl(applicationServer);
					}
					
					if(retriesNumber>0)
					{
						mApplication.retriesNumber = retriesNumber;
					}
					
					if(retriesIntervalInSec>0)
					{
						mApplication.retriesIntervalInSec = retriesIntervalInSec;
					}
					
					String configurationServer = json.optString("configurationServer");
					if(null != configurationServer)
					{
						SpaPreference.setConfigurationServer(getApplicationContext(), configurationServer);
					}
					
					if(isNeedToDownload)
					{
						if(null != dialog)
						{
							startLoader();
						}
						
						IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_CONNECTION_STATE_GET_TEXTS, mApplication, this);
						il.setGetUrl(mTextURL);
						il.start();
					}
					else
					{
						try
						{
							//read language from file
							BufferedReader input = null;
							File file = null;
							try {
							    file = new File(getCacheDir(), PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage())); // Pass getFilesDir() and "MyFile" to read file
							 
							    input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
							    String line;
							    StringBuffer buffer = new StringBuffer();
							    while ((line = input.readLine()) != null) {
							        buffer.append(line);
							    }
							    input.close();
							    JSONObject jsonLang = new JSONObject(  buffer.toString());
								((SpaApplication)getApplication()).setCurrLanguage(jsonLang);
							
							} catch (IOException e) {
							    e.printStackTrace();
							}
							
							//start the application
							startApplication();
						}
						catch(Exception e){
							e.printStackTrace();
							
							if(null != dialog)
							{
								startLoader();
							}
							
							IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_CONNECTION_STATE_GET_TEXTS, mApplication, this);
							il.setGetUrl(mTextURL);
							il.start();
						}
						
						
					}
				}
				catch(Exception e){
					e.printStackTrace();
					handleUncaughtHttpError(pToken, res, pIsBackground);
				}
			
				break;
				
			case IntentLauncher.TOKEN_CONNECTION_STATE_GET_TEXTS:

				//save the language file
				File file;
				FileOutputStream outputStream;
				try 
				{
				    file = new File(getCacheDir(), PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage()));
				 
				    outputStream = new FileOutputStream(file);
				    outputStream.write(res.getBytes());
				    outputStream.close();
				    
				    //save the date of the language file
					SpaPreference.setLanguageDate(getApplicationContext(), PimaProtocol.getLanguageCode( Locale.getDefault().getLanguage()), mTextsTimestamp);
					
				} catch (IOException e) {
				    e.printStackTrace();
				}
				
				
				//load the language from result
				try{
					JSONObject json = new JSONObject(res);
					((SpaApplication)getApplication()).setCurrLanguage(json);
					
				}
				catch(Exception e){
					e.printStackTrace();
				}
				
				startApplication();
			
				break;
				
			default:
				break;
		}
		
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		if(null != dialog)
		{
			dismissLoader();
		}
		
		//show reconnect button
		if(titleNetworkError != null) { 
			titleNetworkError.setText(mApplication.getLocalizedString("NetworkError"));
		}
		
		if(reconnect != null) {
			reconnect.setText(mApplication.getLocalizedString("TapToConnect"));
		}
		
		layout.setVisibility(View.VISIBLE);
	}
	
	private void startApplication()
	{
		if(!checkAVD())
		{

			if (checkPlayServices()) {
				

				String currToken = SpaPreference.getGCMToken(getApplicationContext());
				if(null == currToken)
				{
					SpaPreference.setSentTokenToServer(this, false);
    				//start GCM register service
    				Intent intent = new Intent(this, RegistrationIntentService.class);
    				intent.putExtra(RegistrationIntentService.EXTRAS_ACTION, RegistrationIntentService.ACTION_REGISTER);
    	            startService(intent);
				}
				else
				{
					boolean isSentToServer = SpaPreference.getSentTokenToServer(this);
					if(!isSentToServer)
					{
						IntentLauncher launcher = new IntentLauncher(IntentLauncher.TOKEN_REGISTER_FOR_NOTIFICATION, mApplication, this);
						launcher.setDataParam(currToken);
		    			launcher.start();
					}
				}
			}
		}
		
		// Start main activity
		Intent intent = null;
		if(null == mSystemId)
		{
			intent = new Intent(SplashActivity.this, SystemsActivity.class);
		}
		else
		{
			intent = new Intent(SplashActivity.this, NewLoginActivity.class);
			intent.putExtra(NewLoginActivity.EXTRAS_SYSTEM_ID, mSystemId);
		}
		
		SplashActivity.this.startActivity(intent);
		SplashActivity.this.finish();
		
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        
        if(dialog == null) {
			LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View fl = (View)vi.inflate(R.layout.loader_overlay, null);
			
			dialog = new Dialog(this, R.style.NewDialog);
			dialog.setContentView(fl);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			
		}
        
        LocalBroadcastManager.getInstance(this).
        	registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter("registrationComplete"));
        
        if(null != dialog)
		{
			startLoader();
		}
		
     // Start timer and launch main activity
		IntentLauncher launcher = new IntentLauncher(IntentLauncher.TOKEN_CONNECTION_STATE_CONNECT_SERVER, mApplication, this);
		launcher.start();
	}

    @Override
    protected void onPause() {
//    	if(null != dialog)
//		{System.out.println("DISMISS1");
//			dismissLoader();
//		}
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
       
    }
    

	/**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
    	GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
        	if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @return
     */
    private boolean checkAVD()
    {
    	SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
    	List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);
    	if (sensors.isEmpty()) {
    		// running on an emulator
    		return true;
    	} else {
    		for(int i=0; i<sensors.size(); i++)
    		{
    			Sensor sensor = sensors.get(i);
    			if(sensor.getName().contains("Genymotion"))
    			{
    				return true;
    			}

    		}
    	    // running on a device
    		return false;
    	}
    }

}
