package com.pima.apps.ipima.v3.activity;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.fragment.OutputEditFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class OutputEditActivity extends BaseActivity {
	
	public View moreListOverflow = null;
	private Button moreOverflow;
	
	public Button doneButton;
	
	public void showHelpDialog(String textToShow)
	{
		Bundle bundle = new Bundle();
		bundle.putString("text", textToShow);
		showDialog(TAG_DIALOG_HELP, bundle);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
//		Bundle extras = getIntent().getExtras();
		
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout_done, null);
		
//		TextView titleView = (TextView)v.findViewById(R.id.titleView);
//		if(titleView != null) {
//			if(extras != null && extras.getBoolean(OutputEditFragment.KEY_IS_LOGGED) == true) {
//				titleView.setText(mApplication.getLocalizedString("tabbar_Outputs"));
//			}else {
//				titleView.setText(mApplication.getLocalizedString("AlarmSystem_Add_ManageOutput"));
//			}
//		}

		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayMoreView();
			}
		});
		
		Button doneButton = (Button)v.findViewById(R.id.menu_done);
		doneButton.setText(((SpaApplication)getApplication()).getLocalizedString("button_Done"));
		
		TextView titleView = (TextView)v.findViewById(R.id.titleView);
		titleView.setText(((SpaApplication)getApplication()).getLocalizedString("Outputs.OutputSettings"));
		
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(v);
		
		
		if (savedInstanceState == null) {
			OutputEditFragment fragment = OutputEditFragment.newInstance(getIntent().getExtras());
			fragment.doneItem = doneButton;
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commitAllowingStateLoss();
		}
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
        if(keyCode==KeyEvent.KEYCODE_BACK)  
        {
        	finish();
        }else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;  
    }  
	
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void displayMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {

	}
}
