package com.pima.apps.ipima.v3.activity;

import java.util.HashMap;
import java.util.Map;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.fragment.SettingsFragment;
import com.pima.apps.ipima.v3.network.IntentLauncher;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SettingsActivity extends BaseActivity {

	public static final String EXTRAS_CLIENT_NAME 	= "EXTRAS_CLIENT_NAME";
	public static final String EXTRAS_CLIENT_PHONE 	= "EXTRAS_CLIENT_PHONE";
	
	public View moreListOverflow = null;
	private Button moreOverflow;

	private TextView titleView;
	public Button doneButton;
	
	private SettingsFragment settingsFragment;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout_done, null);
		
		titleView = (TextView)v.findViewById(R.id.titleView);
		titleView.setText(mApplication.getLocalizedString("Settings"));
		
		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayMoreView();
			}
		});
		
		doneButton = (Button)v.findViewById(R.id.menu_done);
		if(doneButton != null) {
			doneButton.setText(mApplication.getLocalizedString("Done"));
		}
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(v);

		String clientName = null;
		String clientPhone = null;
		if(null != getIntent() && null != getIntent().getExtras())
		{
			clientName = getIntent().getStringExtra(EXTRAS_CLIENT_NAME);
			clientPhone = getIntent().getStringExtra(EXTRAS_CLIENT_PHONE);
		}
		
		if (savedInstanceState == null) {
			
			settingsFragment = new SettingsFragment(clientName, clientPhone);
			settingsFragment.doneItem = doneButton;
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, settingsFragment).commitAllowingStateLoss();
		}
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
		if(keyCode==KeyEvent.KEYCODE_BACK)  
        {
        	finish();
        }else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;
    }
	
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void displayMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void onInfoOkClick(String tag) {
		finish();
	}
	
	
	public void addContact(String pUserName, String pPhoneNumber)
	{
		Map<String, Object> data = new HashMap<String, Object>();
		
		data.put("name", pUserName);
		data.put("phoneNumber", pPhoneNumber);
		
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_SET_CLIENT, mApplication, this);
		il.setDataParams(data);
		il.start();
	}
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
		
		switch (pToken) {
	
			case IntentLauncher.TOKEN_SET_CLIENT:
				Bundle bundle = new Bundle();
				bundle.putString("text", mApplication.getLocalizedString("AddSuccesful"));
				
				showDialog(BaseActivity.TAG_DIALOG_INFO, bundle);
				
				break;
				
			default:
				handleUncaughtHttpError(pToken, res, pIsBackground);
				break;
		}

	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		super.handleCaughtHttpError(pToken, res, pIsBackground);
	}
}
