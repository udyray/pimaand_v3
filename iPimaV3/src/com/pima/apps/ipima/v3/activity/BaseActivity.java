package com.pima.apps.ipima.v3.activity;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.fragment.BaseFragment;
import com.pima.apps.ipima.v3.fragment.InfoDialogFragment;
import com.pima.apps.ipima.v3.fragment.LoadingDialogFragment;
import com.pima.apps.ipima.v3.fragment.InfoDialogFragment.InfoDialogCallbacks;
import com.pima.apps.ipima.v3.network.HttpResponseHandler;
import com.pima.apps.ipima.v3.network.IntentLauncher;

abstract public class BaseActivity extends FragmentActivity implements InfoDialogCallbacks, HttpResponseHandler //,SpaClientCallbacks 
{
	protected BaseFragment latestFragment;
	protected Runnable swap = new Runnable() {
		
		@Override
		public void run() {
			if(null != latestFragment)
			{
				try{
					FragmentManager fragmentManager = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.fragment_container, latestFragment);
					fragmentTransaction.commitAllowingStateLoss();
				}
				catch(IllegalStateException e){
					e.printStackTrace();
				}
			}
		}
	};
	protected Handler swap_handler = new Handler();
	
	Dialog dialog = null;
	
	Runnable dismiss = new Runnable() {
		
		@Override
		public void run() {
			if(dialog != null && dialog.isShowing())
				try
				{
					dialog.dismiss();
				}
				catch(Exception e){}
		}
	};
	
	Handler dismiss_handler = new Handler();
	
	public void dismissLoader() {
		dismiss_handler.removeCallbacks(dismiss);
		dismiss_handler.postDelayed(dismiss, 500);
	}
	
	public void startLoader() {
		if(dialog != null) {
			dialog.show();
			dismiss_handler.removeCallbacks(dismiss);
		}
	}
	
	public SpaApplication mApplication;

	public LoadingDialogFragment mLoadginDlg;
	
	public final static int ERROR_CODE_REGISTRATION_FAILED 		= 1;
	public final static int ERROR_CODE_NOT_FOUND 				= 2;
	public final static int ERROR_CODE_TIMEOUT	 				= 7;
	public final static int ERROR_CODE_AUTHENTICATION_FAILED 	= 8;
	public final static int ERROR_CODE_OPERATION_FAILED			= 9;
	public final static int ERROR_CODE_UNEXPECTED_REPLY			= 10;
	public final static int ERROR_CODE_SESSION_TIMEOUT			= 11;
	public final static int ERROR_CODE_SESSION_BUSY				= 12;
	      
	 
	public final static String TAG_DIALOG_LOADING 			= "dialogLoading";
	public final static String TAG_DIALOG_WARNING 			= "dialogLoadingWarning";
	public final static String TAG_DIALOG_INFO 				= "dialogInfo";
	public final static String TAG_DIALOG_HELP 				= "dialogHelp";
	public final static String TAG_DIALOG_INFO_DISCONNECT 	= "dialogInfoDisconnect";
	public final static String TAG_DIALOG_INFO_ERROR 		= "dialogInfoError";
	public final static String TAG_DIALOG_INFO_OK 			= "dialogInfoOK";

	//Used on login screen instead of error or disconnect tag
	//The dialog with this tag wont return to the system list screen
	public final static String TAG_DIALOG_INFO_WRONGPASSWORD = "dialogInfoWrongPassword"; 

	public final static String TAG_DIALOG_RETRIES_LIMIT_REACHED = "retriesLimitReached";

	String errorDialogTag = null;
	Bundle errorArgs = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mApplication = (SpaApplication)getApplication();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if(null != errorDialogTag && null != errorArgs) 
		{
			showDialog(errorDialogTag, errorArgs);
		}
		
		if(dialog == null) {
			LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View fl = (View)vi.inflate(R.layout.loader_overlay, null);
			
			dialog = new Dialog(this, R.style.NewDialog);
			dialog.setContentView(fl);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			
		}
	}

	
	public void onSpaConnected() {
		hideDialog(TAG_DIALOG_LOADING);
		Log.v("iPIMA", "------ now connected");
	}

	
	public void onSpaDisconnected() {
		
		hideDialog(TAG_DIALOG_LOADING);

		clearCache();
		
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(	mApplication.action != SpaApplication.CUR_ACTION.LOGOUT && 
					mApplication.action != SpaApplication.CUR_ACTION.CANCEL_LOGIN && 
					mApplication.action != SpaApplication.CUR_ACTION.NONE && 
					mApplication.disconnectLock == false)
				{
					mApplication.disconnectLock = true;
					Bundle bundle = new Bundle();
					bundle.putString("text",  mApplication.getLocalizedString("APIError.RetriesLimitReached"));
					showDialog(TAG_DIALOG_INFO_DISCONNECT, bundle);
				}
				mApplication.action = SpaApplication.CUR_ACTION.NONE;
			}
		});

		Log.v("iPIMA", "------ conneciton closed");
	}
	
	
	public void onSpaDisconnectedRemotely() {
		hideDialog(TAG_DIALOG_LOADING);

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(	mApplication.action != SpaApplication.CUR_ACTION.LOGOUT && 
					mApplication.action != SpaApplication.CUR_ACTION.CANCEL_LOGIN && 
					mApplication.disconnectLock == false) 
				{
					mApplication.disconnectLock = true;
					Bundle bundle = new Bundle();
					bundle.putString("text",  mApplication.getLocalizedString("APIError.ConnectionClosedByRemotePeer"));
					showDialog(TAG_DIALOG_INFO_DISCONNECT, bundle);
				}
				mApplication.action = SpaApplication.CUR_ACTION.NONE;
			}
		});

		Log.v("iPIMA", "------ conneciton closed remotely");
	}

	
//	public void onSpaMessageReceived(final SpaMessageGet spaMessage) {
//		hideDialog(TAG_DIALOG_LOADING);
//		Log.v("iPIMA", "------ message received");
////		Log.v("iPIMA", "LAST=" + ((SpaApplication)getApplication()).getSendMessage());
//		
//		if(spaMessage instanceof GetDisconnectResult) {
////			mApplication.close();
//			mApplication.getClose();
//		}else if(spaMessage instanceof GetErrorMessage) {
//			
//			runOnUiThread(new Runnable() {
//				
//				@Override
//				public void run() {
//					GetErrorMessage obj = (GetErrorMessage)spaMessage;
//					if(mApplication.disconnectLock == false) {
//						mApplication.disconnectLock = true;
//						Bundle bundle = new Bundle();
//						bundle.putString("text", obj.text);
//						showDialog(TAG_DIALOG_INFO_ERROR, bundle);
//					}
//				}
//			});
//			
//		}
//	}

	public void onSpaError(String error) {
		hideDialog(TAG_DIALOG_LOADING);


		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				if(mApplication.disconnectLock == false) {
					Bundle bundle = new Bundle();
					bundle.putString("text", mApplication.getLocalizedString("APIError.NoConnection"));
					showDialog(TAG_DIALOG_INFO_ERROR, bundle);

					mApplication.disconnectLock = true;
				}
//				mApplication.close();
			}
		});


		Log.v("iPIMA", "------ error: " + error);
	}
	
	public void onSpaTimeout() {
		hideDialog(TAG_DIALOG_LOADING);


		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(mApplication.disconnectLock == false) {

					mApplication.disconnectLock = true;
					Bundle dialogBundle = new Bundle();
					dialogBundle.putString("text", mApplication.getLocalizedString("APIError.RetriesLimitReached"));
					showDialog(TAG_DIALOG_RETRIES_LIMIT_REACHED, dialogBundle);
//					mApplication.close();
				}
			}
		});

	}
	

	public void showDialog(String dialogTag, Bundle args) {
		try{
			if (TAG_DIALOG_LOADING.equals(dialogTag)) {
				// Create and show the dialog.
				hideAllDialogs();
				
				LoadingDialogFragment newFragment = LoadingDialogFragment.newInstance("");
				mLoadginDlg = newFragment;
				mLoadginDlg.parentActivity = this;
				newFragment.show(getSupportFragmentManager(), dialogTag);
			}
			else if (TAG_DIALOG_INFO.equals(dialogTag) || TAG_DIALOG_INFO_OK.equals(dialogTag) || TAG_DIALOG_INFO_ERROR.equals(dialogTag)
					|| TAG_DIALOG_INFO_DISCONNECT.equals(dialogTag)|| TAG_DIALOG_RETRIES_LIMIT_REACHED.equals(dialogTag) ||
					TAG_DIALOG_INFO_WRONGPASSWORD.equals(dialogTag) || TAG_DIALOG_HELP.equals(dialogTag)) {
	 
				hideAllDialogs();
	
				String title = args != null ? args.getString("title") : null;
				String text = args != null ? args.getString("text") : null;
				InfoDialogFragment newFragment = InfoDialogFragment.newInstance(dialogTag, title, text);
				newFragment.show(getSupportFragmentManager(), "tag");
			}else if(TAG_DIALOG_WARNING.equals(dialogTag)) {
				hideAllDialogs();
	
				String title = args != null ? args.getString("title") : null;
				String text = args != null ? args.getString("text") : null;
				InfoDialogFragment newFragment = InfoDialogFragment.newInstance(dialogTag, title, text);
				newFragment.show(getSupportFragmentManager(), "tag");
			}
			
			errorDialogTag = null;
			errorArgs = null;
		}
		catch(Exception e){
			errorDialogTag = dialogTag;
			errorArgs = args;
			
			String title = args != null ? args.getString("title") : null;
			String text = args != null ? args.getString("text") : null;
			System.out.println("ERROR INFO " + dialogTag + ": " + title + " - " + text);
			e.printStackTrace();
		}
	}

	public void hideDialog(String dialogTag) {
		Fragment frag = getSupportFragmentManager().findFragmentByTag(dialogTag);
		if (frag != null) {
			getSupportFragmentManager().beginTransaction().remove(frag).commitAllowingStateLoss();
		}
	}

	protected void hideAllDialogs() {
		Fragment frag;
		while ((frag = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG_INFO)) != null) {
			getSupportFragmentManager().beginTransaction().remove(frag).commitAllowingStateLoss();
		}

		while ((frag = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG_INFO_ERROR)) != null) {
			getSupportFragmentManager().beginTransaction().remove(frag).commitAllowingStateLoss();
		}

		while ((frag = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG_INFO_DISCONNECT)) != null) {
			getSupportFragmentManager().beginTransaction().remove(frag).commitAllowingStateLoss();
		}
		while ((frag = getSupportFragmentManager().findFragmentByTag(TAG_DIALOG_RETRIES_LIMIT_REACHED)) != null) {
			getSupportFragmentManager().beginTransaction().remove(frag).commitAllowingStateLoss();
		}
	}

	@Override
	public void onInfoOkClick(String tag) {
		if (TAG_DIALOG_INFO_DISCONNECT.equals(tag) ||
			TAG_DIALOG_RETRIES_LIMIT_REACHED.equals(tag) ||
			TAG_DIALOG_INFO_ERROR.equals(tag)) {
			
			disconnect();
			
		}
	}

	
	long lastClick = 0;
	View lastClickView = null;
	
	public Boolean clickBarrier(View v) {
		
		long curClick = System.currentTimeMillis();
		if(curClick - lastClick > 400 || lastClickView != v) {
			lastClick = curClick;
			lastClickView = v;
			return true;
		}
		lastClick = curClick;
		lastClickView = v;
		
		return false;
	}
	
	public void clearCache() {
		((SpaApplication)mApplication).setConnectedSystem(null);

		((SpaApplication)mApplication).isBypassZonesFragment = false;
		
		((SpaApplication)mApplication).masterCodeInd = null;
		((SpaApplication)mApplication).sessionToken = null;
//		((SpaApplication)mApplication).systemId = null;
		
//		((SpaApplication)mApplication).refreshZonesName = false;
		
//		((SpaApplication)mApplication).panelVersion = null;

	}
	
	/**
	 * 
	 */
	public void disconnect()
	{
		if(null != mApplication.getConnectedSystem())
		{
			if(null!=mApplication.getConnectedSystem().getSystemId())
			{System.out.println("Send Disconnect");
				IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_DISCONNECT, mApplication, this);
				il.start();
			}
		}
		
		
		// go to the systems list activity
//		Intent intent = new Intent(BaseActivity.this, SystemsActivity.class);
//		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		startActivity(intent);
		
		finish();
	}
	
	
	/**
	 * 
	 */
	public void abort()
	{
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_ABORT, mApplication, this);
		il.start();
		
	}
	
	/**
	 * 
	 * @return
	 */
	public ActionBar getSupportActionBar()
	{
		return getActionBar();
	}
	
	@Override
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground) {
		try
    	{
			if(null != res)
			{
	    		JSONObject json = new JSONObject(res);
	    		int errorCode = json.getInt("errorCode");
	    		String errorText = json.getString("errorText");
	    		
	    		Bundle dialogBundle = new Bundle();
	    		
				dialogBundle.putString("text", mApplication.getLocalizedString(errorText) + " (" + errorCode + ")");
	//			dialogBundle.putString("title", mApplication.getLocalizedString("Error"));
				
				if(!pIsBackground)
				{
					switch(errorCode)
					{
						case ERROR_CODE_SESSION_TIMEOUT:
						case ERROR_CODE_TIMEOUT:
							showDialog(TAG_DIALOG_WARNING, dialogBundle);	
							break;
						
						default:
							showDialog(TAG_DIALOG_INFO_OK, dialogBundle);	
							break;
					}
				}
			}
		}
		catch(Exception e){
    		e.printStackTrace();
    		handleUncaughtHttpError(pToken, res, pIsBackground);
    	}
		
	}
}
