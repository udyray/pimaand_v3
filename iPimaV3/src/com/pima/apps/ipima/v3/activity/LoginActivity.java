package com.pima.apps.ipima.v3.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.network.IntentLauncher;
import com.pima.apps.ipima.v3.views.PanelSystem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class LoginActivity extends BaseActivity implements OnClickListener {
	
	public static final String LOG_TAG 			= "LoginActivity";
	public static final String EXTRAS_IS_EDIT 	= "EXTRAS_IS_EDIT";
	
	private boolean mIsEditAction = false;
	private EditText mEtPassword;
	private String systemName="";
	
	private Button okButton;
	
	private ProgressBar progressDialog = null;
	
	private Boolean connecting = false;
	private Boolean canceledConnection = false;
	
//	private Handler h;
//	private Runnable r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);    // Removes title bar
		//this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);    // Removes notification bar

//		getSupportActionBar().setDisplayShowHomeEnabled(false);
//		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
		
		mIsEditAction = getIntent().getBooleanExtra(EXTRAS_IS_EDIT, false); 
		
		systemName = mApplication.getConnectedSystem().getSystemName();
		setContentView(R.layout.activity_login);
		
		okButton = (Button)findViewById(R.id.btn_ok);
		okButton.setOnClickListener(LoginActivity.this);
		okButton.setEnabled(false);
		okButton.setText(mApplication.getLocalizedString("Connect"));
		
		Button cancelButton = (Button)findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(LoginActivity.this);
		cancelButton.setText(mApplication.getLocalizedString("Cancel"));
		
		mEtPassword = (EditText)findViewById(R.id.et_password);
		mEtPassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(mEtPassword.getText().toString().length() > 0) {
					if(progressDialog == null) {
						okButton.setEnabled(true);
					}else if(progressDialog != null && progressDialog.getVisibility() == View.INVISIBLE) {
						okButton.setEnabled(true);
					}
				}else {
					okButton.setEnabled(false);
				}
			}
		});
		mEtPassword.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE) {
					if (mEtPassword.getText().length() > 0) {
						
						mApplication.disconnectLock = false;
						
						if(progressDialog == null) {
							progressDialog = new ProgressBar(LoginActivity.this);
							
							LinearLayout ll = (LinearLayout)findViewById(R.id.loginLayout);
							LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							lp.gravity = Gravity.CENTER_HORIZONTAL;
							progressDialog.setLayoutParams(lp);
							ll.addView(progressDialog);
							progressDialog.setIndeterminate(true);
							okButton.setEnabled(false);
							mEtPassword.setEnabled(false);
							mEtPassword.setFocusable(false);
							mEtPassword.setFocusableInTouchMode(false);

						}else {
							progressDialog.setVisibility(View.VISIBLE);
							okButton.setEnabled(false);
							mEtPassword.setEnabled(false);
							mEtPassword.setFocusable(false);
							mEtPassword.setFocusableInTouchMode(false);

						}
						
						canceledConnection = false;

//						if(connecting == false) {
//							mApplication.run(mApplication.pairLatest);
//						}
						
						connecting = true;
						
//						if(h == null) {
//							h = new Handler();
//						}
//						if(r == null) {
//							r = new Runnable() {
//								
//								@Override
//								public void run() {
//									
//									if(canceledConnection) {
//										return;
//									}
//									
//									int password = Integer.valueOf(mEtPassword.getText().toString());
//									mApplication.sendGeneral(new SendPassword(password));
//								}
//							};
//						}
//
//						h.postDelayed(r, 100);
						
					}
				}
				return false;
			}
		});
		
		TextView hintTextView = (TextView)findViewById(R.id.hintTextView);
		hintTextView.setText(mApplication.getLocalizedString("Enter-Code"));
		
		TextView txtAddition = (TextView)findViewById(R.id.connectToTextAddition);
		if(txtAddition == null)
		{
			TextView txt = (TextView)findViewById(R.id.connectToText);
			txt.setText(mApplication.getLocalizedString("Connectingto") + " " + systemName);
		}else {
			txtAddition.setText(mApplication.getLocalizedString("Connectingto") + " " + systemName + " ");

		}
		
		setTitle(mApplication.getLocalizedString("Connect"));
	}
	
	

	@Override
	protected void onResume() {
		
		super.onResume();

		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				mEtPassword.requestFocus();
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(mEtPassword, InputMethodManager.SHOW_FORCED);
			}
		}, 800);
		
		//imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
	}
	
	



	@Override
	protected void onPause() {
		super.onPause();
		
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			
			if (mEtPassword.getText().length() > 0) {
				
				mApplication.disconnectLock = false;
				
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
				
				if(progressDialog == null) {
					progressDialog = new ProgressBar(this);
					
					LinearLayout ll = (LinearLayout)findViewById(R.id.loginLayout);
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
					lp.gravity = Gravity.CENTER_HORIZONTAL;
					progressDialog.setLayoutParams(lp);
					ll.addView(progressDialog);
					progressDialog.setIndeterminate(true);
					okButton.setEnabled(false);
					mEtPassword.setFocusable(false);
					mEtPassword.setFocusableInTouchMode(false);
					mEtPassword.setEnabled(false);
				}else {
					progressDialog.setVisibility(View.VISIBLE);
					okButton.setEnabled(false);
					mEtPassword.setFocusable(false);
					mEtPassword.setFocusableInTouchMode(false);
					mEtPassword.setEnabled(false);
				}
				
				canceledConnection = false;

				login();
//				if(mApplication.isChannelOpen()) { 
//					//in case session is still open after invalid password 
//					//we are just sending pass again
//					onSpaConnected();
//				}
//				else if(connecting == false) {
//					mApplication.run(mApplication.pairLatest);
//				}
				
				connecting = true;
				
//				if(h == null) {
//					h = new Handler();
//				}
//				if(r == null) {
//					r = new Runnable() {
//						
//						@Override
//						public void run() {
//							
//							if(canceledConnection) {
//								return;
//							}
//							
//							int password = Integer.valueOf(mEtPassword.getText().toString());
//							mApplication.sendGeneral(new SendPassword(password));
//						}
//					};
//				}

//				h.postDelayed(r, 5100);
				
			}
			break;
		case R.id.btn_cancel:
			if(connecting) {
				
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);

				
				mApplication.action = SpaApplication.CUR_ACTION.CANCEL_LOGIN;
//				mApplication.close();
				
				connecting = false;
				canceledConnection = true;
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
				
				if(progressDialog != null) {
					progressDialog.setVisibility(View.INVISIBLE);
					okButton.setEnabled(true);
					mEtPassword.setEnabled(true);
					mEtPassword.setFocusable(true);
					mEtPassword.setFocusableInTouchMode(true);

				}

			}else {
				
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
				
				Intent intent = new Intent(LoginActivity.this, SystemsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			
			}
			break;

		default:
			break;
		}
	}
	
	private void login() {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("code", mEtPassword.getText().toString());
		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_AUTH, mApplication, this);
		il.setDataParams(data);
		il.start();
	}

	
 	@Override
	public void onSpaDisconnected() {
		//super.onSpaDisconnected();
 		
 		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				canceledConnection = true;
				connecting = false;

				if(progressDialog != null) {
					progressDialog.setVisibility(View.INVISIBLE);
					okButton.setEnabled(true);
					mEtPassword.setEnabled(true);
					mEtPassword.setFocusable(true);
					mEtPassword.setFocusableInTouchMode(true);

				}
				
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
			}
		});
		android.util.Log.v("iPIMA", "onSpaDisconnected");
		
	}
	
	@Override
	public void onSpaConnected() {
		
		android.util.Log.v("iPIMA", "onSpaConnected");
		
		if(canceledConnection) {
			return;
		}

		IntentLauncher il = new IntentLauncher(IntentLauncher.TOKEN_GET_SYSTEM_STATUS, mApplication, this);
		il.start();
		
		
	}
	
	@Override
	public void onSpaTimeout() {


		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(!mApplication.disconnectLock) {

					mApplication.disconnectLock = true;
					Bundle dialogBundle = new Bundle();
					dialogBundle.putString("text", mApplication.getLocalizedString("APIError.RetriesLimitReached"));
					showDialog(TAG_DIALOG_INFO_WRONGPASSWORD, dialogBundle); //Dont go back to system list
//					mApplication.close();
					
				}
			}
		});

	}

	@Override
	public void onSpaError(String error) {

		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				if(mApplication.disconnectLock == false) {
					mApplication.disconnectLock = true;
					Bundle bundle = new Bundle();
					bundle.putString("text", mApplication.getLocalizedString("APIError.NoConnection"));
					showDialog(TAG_DIALOG_INFO_ERROR, bundle);
				}
			
			}
		});
		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				connecting = false;
				canceledConnection = true;
				
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
				
				if(progressDialog != null) {
					progressDialog.setVisibility(View.INVISIBLE);
					okButton.setEnabled(true);
					mEtPassword.setEnabled(true);
					mEtPassword.setFocusable(true);
					mEtPassword.setFocusableInTouchMode(true);

				}
			}
		});
		android.util.Log.v("iPIMA", "onSpaError --- error");
	}
	
	
    @Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
        //replaces the default 'Back' button action  
        if(keyCode==KeyEvent.KEYCODE_BACK)  
        {  
        	if(connecting) {
				mApplication.action = SpaApplication.CUR_ACTION.CANCEL_LOGIN;
//				mApplication.close();
				
				connecting = false;
				canceledConnection = true;
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
				
				if(progressDialog != null) {
					progressDialog.setVisibility(View.INVISIBLE);
					okButton.setEnabled(true);
					mEtPassword.setEnabled(true);
					mEtPassword.setFocusable(true);
					mEtPassword.setFocusableInTouchMode(true);

				}
				
				Intent intent = new Intent(LoginActivity.this, SystemsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

			}else {
				
//				if(h != null && r != null) {
//					h.removeCallbacks(r);
//				}
				
				Intent intent = new Intent(LoginActivity.this, SystemsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			
			}
        }  
        return true;  
    }
    
    
    @Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
    	try
    	{
    		JSONObject json = new JSONObject(res);
    		if(pToken == IntentLauncher.TOKEN_AUTH)
    		{
    			//success login
//    			mApplication.systemId = mApplication.connectedSystem.getSystemId();
    			
	    		String masterCodeInd = json.getString("masterCodeInd");
	    		String sessionToken = json.getString("sessionToken");
	    		
	    		mApplication.sessionToken = sessionToken;
	    		mApplication.masterCodeInd = masterCodeInd;
	    		
	    		onSpaConnected(); 
    		}
    		else if(pToken == IntentLauncher.TOKEN_GET_SYSTEM_STATUS)
    		{
    			PanelSystem ps = mApplication.getConnectedSystem();
    			ps.setFromJSON(json);
    			mApplication.setConnectedSystem(ps);
    			
    			if(mIsEditAction)
    			{
    				Intent intent = new Intent(this, SystemEditActivity.class);
    				
    				intent.putExtra(SystemEditActivity.KEY_SYSTEM_EDIT, mApplication.getConnectedSystem().getSystemId());
    				intent.putExtra(SystemEditActivity.KEY_SYSTEM_NAME, mApplication.getConnectedSystem().getSystemName());
    				startActivity(intent);
    				
    				finish();
    			}
    			else
    			{
    				
    				Intent intent=new Intent(getApplicationContext(), MainScreenActivity.class);
        			startActivity(intent);
    				
    			}
    			
    			finish();
    			
    		}
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		handleUncaughtHttpError(pToken, res, pIsBackground);
    	}
		
	}
    
    @Override
	public void onInfoOkClick(String tag) {
		super.onInfoOkClick(tag);
		
		if(progressDialog != null) {
			progressDialog.setVisibility(View.INVISIBLE);
			okButton.setEnabled(true);
			mEtPassword.setEnabled(true);
			mEtPassword.setFocusable(true);
			mEtPassword.setFocusableInTouchMode(true);

		}
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		if(pToken == IntentLauncher.TOKEN_AUTH)
		{
			connecting = false;
		}
	}
	
	@Override
	public void handleCaughtHttpError(int pToken, String res, boolean pIsBackground) {
		connecting = false;
		
		super.handleCaughtHttpError(pToken, res, pIsBackground);
	}
}
