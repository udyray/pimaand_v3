package com.pima.apps.ipima.v3.activity;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.fragment.PartitionsNamesFragment;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class PartitionsNamesEditActivity extends BaseActivity {

	public View moreListOverflow = null;
	private Button moreOverflow;
	 
	public Button doneButton;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setActionBar();

		if (savedInstanceState == null) {
			// During initial setup, plug in the details fragment.
            PartitionsNamesFragment fragment = PartitionsNamesFragment.newInstance(getIntent().getExtras());
            fragment.doneItem = doneButton;
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commitAllowingStateLoss();
		}
	}
	
	private void setActionBar() {
		ActionBar actionBar = getSupportActionBar();
		
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
	
		LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.icon_layout_done, null);
		
		TextView titleView = (TextView)v.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(mApplication.getLocalizedString("button_partitions_names"));
		}

		
		moreOverflow = (Button)v.findViewById(R.id.moreOverflowButton);
		moreOverflow.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayMoreView();
			}
		});
		
		doneButton = (Button)v.findViewById(R.id.menu_done);
		
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(v);
		
	}

	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
		if(keyCode==KeyEvent.KEYCODE_BACK)  
        {
        	finish();
        }else if(keyCode==KeyEvent.KEYCODE_MENU)  
        {
        	displayMoreView();
        }
        return true;
    }
	
	public void replaceMoreView(View moreView) {
		dismissMoreView();
		try {
			moreListOverflow = moreView;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void displayMoreView() {
		try {			
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null && moreListOverflow != null) {
				if(moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}else {
					fl.addView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void dismissMoreView() {
		try {
			FrameLayout fl = (FrameLayout)findViewById(R.id.fragment_container);
			if(fl != null) {
				if(moreListOverflow != null && moreListOverflow.getParent() == fl) {
					fl.removeView(moreListOverflow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void handleHttpResponse(int pToken, String res, boolean pIsBackground) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleUncaughtHttpError(int pToken, String res, boolean pIsBackground) {
		// TODO Auto-generated method stub
		
	}
	
}
