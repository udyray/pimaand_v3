package com.pima.apps.ipima.v3.views;

public class Contact {

	
	private String mId = null;
	private String mName = null;
	private String mPhone = null;
	
	public Contact(String pId, String pName, String pPhone) {
		mId = pId;
		mName = pName;
		
		if(pPhone == null || pPhone.equals("null"))
		{
			pPhone = "";
		}
		mPhone = pPhone;
	}

	
	public String getId()
	{
		return mId;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public String getPhone()
	{
		return mPhone;
	}
	
}
