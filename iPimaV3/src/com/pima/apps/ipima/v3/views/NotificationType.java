package com.pima.apps.ipima.v3.views;

import org.json.JSONObject;

public class NotificationType {

	public static String ATTRIBUTE_NUMBER					= "number";
	public static String ATTRIBUTE_NAME						= "name";
	
	private int mNumber = -1;
	private String mName = null;
	
	public NotificationType(int pNumber, String pName) {
		mNumber = pNumber;
		mName = pName;
	}

	public int getNumber() {
		return mNumber;
	}

	public void setId(int pNumber) {
		mNumber = pNumber;
	}

	public String getName() {
		return mName;
	}

	public void setmName(String pName) {
		mName = pName;
	}



	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_NUMBER, getNumber());
		json.put(ATTRIBUTE_NAME, getName());
		
		return json;
	}
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static NotificationType fromJson(JSONObject pJson)
	{
		int number = pJson.optInt(ATTRIBUTE_NUMBER);
		String name = pJson.optString(ATTRIBUTE_NAME);
		
		NotificationType nt = new NotificationType(number, name);
		
		return nt;
		
	}
	
	
}
