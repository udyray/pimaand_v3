package com.pima.apps.ipima.v3.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.comparators.NotificationTypeComparator;


import android.util.SparseArray;

public class PanelSystem {

	private static final String ATTRIBUTE_ID 						=	"id";
	private static final String ATTRIBUTE_NAME 						=	"name";
	private static final String ATTRIBUTE_TYPE 						=	"type";
	private static final String ATTRIBUTE_CONFIGURATION				=	"configuration";
	private static final String ATTRIBUTE_PARTITIONS				=	"partitions";
	private static final String ATTRIBUTE_SYSTEM_STATUS				=	"system_status";
	private static final String ATTRIBUTE_ZONES						=	"zones";
	private static final String ATTRIBUTE_OUTPUTS					=	"outputs";
	private static final String ATTRIBUTE_NOTIFICATIONS				=	"notifications";
	private static final String ATTRIBUTE_FAULTS					=	"faults";
	private static final String ATTRIBUTE_LOGS						=	"logs";
	private static final String ATTRIBUTE_NOTIFICATIONS_TYPES		=	"notifications_types";
	
	public static enum TYPES_SYSTEM_STATUS {
		None,
		Disarm,
		Full,
		Home1,
		Home2,
		Home3,
		Home4,
		SaturdayOff,
		SaturdayOn,
		Special;
		
		public int getIndex() {
	        return ordinal();
	    }
		
		public static TYPES_SYSTEM_STATUS getAtIndex(int index) {
	    	for(TYPES_SYSTEM_STATUS tz : values()) {
	    		if (tz.getIndex() == index) {
	    			return tz;
	    		}
	    	}
	    	
	    	return null;
	    }
	}
	
	private String mSystemId = null;
	private String mSystemName = null;
	private int mPanelType;
	
	private SystemConfiguration mConfiguration = null;
	private SystemPartition[] mPartitions = null;
	private TYPES_SYSTEM_STATUS mSystemStatusType = null;
	private SystemZone[] mZones = null;
	private SparseArray<SystemOutput> mOutputs = null;
//	private SystemOutput[] mEnabledOutputs = null;
	private SystemNotification[] mSystemNotifications = null;
	private SystemFault[] mFaults = null;
	private SystemLog[] mLog = null;
	
	private SparseArray<NotificationType> mSystemNotificationsTypesMap;
	
	public PanelSystem(	String pSystemId,  String pSystemName, int pPanelType){
		mPanelType = pPanelType;
		mSystemId = pSystemId;
		mSystemName = pSystemName;
		mPartitions = null;
		mSystemStatusType = null;
		mZones = null;
		mOutputs = null;
//		mEnabledOutputs = null;
		mSystemNotifications = null;
		mFaults = null;
		mLog = null;
		
		mSystemNotificationsTypesMap = null;
	}

	public void setSystemPartitions(SystemPartition[] pPartitions) {
		mPartitions = pPartitions;
	}
	
	
	public void setSystemZones(SystemZone[] pZones) {
		mZones = pZones;
	}
	
	public void setSystemOutputs(SparseArray<SystemOutput> pOutputs) {
		mOutputs = pOutputs;
		
	}
	
	public String getSystemId() {
		return mSystemId;
	}


	public String getSystemName() {
		return mSystemName;
	}

	public int getPanelType() {
		return mPanelType;
	}

	public void setConfiguration(SystemConfiguration pConfiguration)
	{
		mConfiguration = pConfiguration;
	}
	
	public SystemConfiguration getConfiguration()
	{
		return mConfiguration;
	}
	
	public void setSystemNotificationsTypesMap(SparseArray<NotificationType> pSystemNotificationsTypesMap)
	{
		mSystemNotificationsTypesMap = pSystemNotificationsTypesMap;
	}
	
	public SparseArray<NotificationType> getSystemNotificationsTypesMap()
	{
		return mSystemNotificationsTypesMap;
	}
	
	public void setmSystemStatusType(TYPES_SYSTEM_STATUS pStatus)
	{
		mSystemStatusType = pStatus;
	}
	
	public NotificationType[] getSystemNotificationsTypes()
	{
		if(null == mSystemNotificationsTypesMap)
		{
			return null;
		}
		
		NotificationType[] types = new NotificationType[mSystemNotificationsTypesMap.size()];
		for(int i=0; i<mSystemNotificationsTypesMap.size(); i++)
		{
			NotificationType type = mSystemNotificationsTypesMap.valueAt(i);
			types[i] = type;
		}
		Arrays.sort(types, new NotificationTypeComparator());
		return types;
	}
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public void setFromJSON(JSONObject pJson) {
		
		boolean isDisarmed = false;
		boolean isPart = false;
		
		mSystemStatusType = null;
		mPartitions = null; 
		
		JSONArray partArr = pJson.optJSONArray("partitions");
		if(null != partArr)
		{
			mPartitions = new SystemPartition[partArr.length()];
			for(int i=0; i<partArr.length(); i++)
			{
				JSONObject part = partArr.optJSONObject(i);
				if(null != part)
				{
					String id = part.optString("id");
					String name = part.optString("name");
					int status = part.optInt("status");
					TYPES_SYSTEM_STATUS statusType = TYPES_SYSTEM_STATUS.getAtIndex(status);
					if(status>=0 && status<=5)
					{
						if(status==0) 
						{
							isDisarmed = true;
						}
					
						SystemPartition partition = new SystemPartition(id, name, statusType);
						mPartitions[i] = partition;
					}
					
					
				}
			}
			

			if(mPartitions.length>1)
			{
				isPart = true;
			}
			
			
			if(!isPart)//not partitioned
			{
				mSystemStatusType = mPartitions[0].getStatus();
			}
			else//partitioned
			{
				if(isDisarmed)
				{
					mSystemStatusType = TYPES_SYSTEM_STATUS.Disarm;
				}
				else
				{
					mSystemStatusType = TYPES_SYSTEM_STATUS.Full;
				}
				
			}
		}
		
		mZones = null; 
		JSONArray zonesArr = pJson.optJSONArray("zones");
		if(null != zonesArr)
		{
			mZones = new SystemZone[zonesArr.length()];
			for(int i=0; i<zonesArr.length(); i++)
			{
				JSONObject zone = zonesArr.optJSONObject(i);
				if(null != zone)
				{
					int number = zone.optInt("number");
					String name = zone.optString("name");
					
					mZones[i] = new SystemZone(number, name);
					JSONObject status = zone.optJSONObject("status");
					if(null != status)
					{
						try
						{
							boolean open = status.getBoolean("open");
							boolean alarm = status.getBoolean("alarm");
							boolean bypass = status.getBoolean("bypass");
							
							mZones[i].setOpen(open);
							mZones[i].setAlarm(alarm);
							mZones[i].setBypass(bypass);
							
							
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}
					
				}
				
			}
		}
		
		
		mOutputs = null; 
		
		JSONArray outputsArr = pJson.optJSONArray("outputs");
		if(null != outputsArr)
		{
			mOutputs = new SparseArray<SystemOutput>();
			for(int i=0; i<outputsArr.length(); i++)
			{
				JSONObject output = outputsArr.optJSONObject(i);
				if(null != output)
				{
					int number = output.optInt("id");
					String name = output.optString("name");
					if(name == null || name.equals("null"))
					{
						name = null;
					}
					boolean active = output.optBoolean("active");
					boolean enable = output.optBoolean("enable");
					
					SystemOutput so = new SystemOutput(number, name, active, enable);
					mOutputs.put(number, so);
				}
				
			}
			
		}
		
	}

	public boolean getIsPartioned() {
		return mPartitions!=null && mPartitions.length > 1;
	}
	
	public int getNumOfPartiones() {
		if(!getIsPartioned())
		{
			return 1;
		}
		
		return mPartitions.length;
	}
	
	public SystemPartition getPartition(int pNum)
	{
		if(getNumOfPartiones() > pNum)
		{
			return mPartitions[pNum];
		}
		return null;
	}
	
	public SystemPartition[] getAllPartitions()
	{
		return mPartitions;
	}
	
	public TYPES_SYSTEM_STATUS getSystemStatusType() {
		return mSystemStatusType;
	}
	
	public SystemZone[] getAllSystemZones() {
		return mZones;
	}
	
//	public int getNumOfZones() {
//		if(null == mZones)
//		{
//			return 0;
//		}
//		return mZones.length;
//	}
	
	/**
	 * 
	 * @return
	 */
	public SystemZone[] getActiveSystemZones() {

		SystemZone[] zones = null;
		if(null != mZones)
		{
			ArrayList<SystemZone> tmp = new ArrayList<SystemZone>();;
			for(int i=0; i<mZones.length; i++)
			{
				SystemZone sz = mZones[i];
				if(sz.getIsBypass() )
				{
					tmp.add(sz);
				}
				else if(sz.getIsAlarm() )
				{
					tmp.add(sz);
				}
				else if(sz.getIsOpen() )
				{
					tmp.add(sz);
				}
			}
			
			if(tmp.size()>0)
			{
				zones = new SystemZone[tmp.size()];
				for(int i=0; i<tmp.size(); i++)
				{
					zones[i] = tmp.get(i);
				}
			}
		}
		else
		{
			zones = new SystemZone[0];
		}
		return zones;
	
	}
	
	/**
	 * 
	 * @return
	 */
	public SystemZone[] getBypassedSystemZones() {

		SystemZone[] zones = null;
		if(null != mZones)
		{
			ArrayList<SystemZone> tmp = new ArrayList<SystemZone>();;
			for(int i=0; i<mZones.length; i++)
			{
				SystemZone sz = mZones[i];
				if(sz.getIsBypass() )
				{
					tmp.add(sz);
				}
			}
			
			if(tmp.size()>0)
			{
				zones = new SystemZone[tmp.size()];
				for(int i=0; i<tmp.size(); i++)
				{
					zones[i] = tmp.get(i);
				}
			}
		}
		return zones;
	
	}
	
	/**
	 * 
	 * @return
	 */
	public SystemZone[] getOpenSystemZones() {

		SystemZone[] zones = null;
		if(null != mZones)
		{
			ArrayList<SystemZone> tmp = new ArrayList<SystemZone>();;
			for(int i=0; i<mZones.length; i++)
			{
				SystemZone sz = mZones[i];
				if(sz.getIsOpen() )
				{
					tmp.add(sz);
				}
			}
			
			if(tmp.size()>0)
			{
				zones = new SystemZone[tmp.size()];
				for(int i=0; i<tmp.size(); i++)
				{
					zones[i] = tmp.get(i);
				}
			}
		}
		return zones;
	
	
	}
	
	/**
	 * 
	 * @return
	 */
	public SystemZone[] getAlarmedSystemZones() {
		SystemZone[] zones = null;
		if(null != mZones)
		{
			ArrayList<SystemZone> tmp = new ArrayList<SystemZone>();;
			for(int i=0; i<mZones.length; i++)
			{
				SystemZone sz = mZones[i];
				if(sz.getIsAlarm())
				{
					tmp.add(sz);
				}
			}
			
			if(tmp.size()>0)
			{
				zones = new SystemZone[tmp.size()];
				for(int i=0; i<tmp.size(); i++)
				{
					zones[i] = tmp.get(i);
				}
			}
		}
		return zones;
	
	
	}
	
	public void setSystemOutputActive(int pId, boolean pActive) {
		SystemOutput output = mOutputs.get(pId);
		if(null != output)
		{
			output.setIsActive(pActive);
		}
	}
		
	/**
	 * 
	 * @param pJsonArray
	 *
	public void setSystemOutputs(JSONArray outputsArr) {
		if(null != outputsArr)
		{
			if(null != mOutputs)
			{
				for(int i=0; i<mOutputs.size(); i++)
				{
					int number = mOutputs.keyAt(i);
					SystemOutput so = mOutputs.get(number);
					if(null != so)
					{
						so.setIsActive(false);
						so.setIsEnabled(false);
					}
				}
				
			}
			
		}
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	public SparseArray<SystemOutput> getSystemOutputs() {
		return mOutputs;
	}
	
	
	public SystemOutput[] getEnabledSystemOutputs() {
		List<SystemOutput> enabledOutputs = new ArrayList<SystemOutput>();
		for(int i=0; i<mOutputs.size(); i++)
		{
			int key = mOutputs.keyAt(i);
			SystemOutput so = mOutputs.get(key);
					
			if(so.isEnabled())
			{
				enabledOutputs.add(so);
			}
		}
		
		SystemOutput[] mEnabledOutputs = new SystemOutput[enabledOutputs.size()];
		for(int i=0; i<enabledOutputs.size(); i++)
		{
			mEnabledOutputs[i] = enabledOutputs.get(i);
		}
		return mEnabledOutputs;
	}
	
	
	public SystemNotification[] getSystemNotifications() {
		return mSystemNotifications;
	}
	
	public SystemFault[] getSystemFaults() {
		return mFaults;
	}
	
	public SystemLog[] getSystemLog() {
		return mLog;
	}
	
	
	/**
	 * 
	 * @param pNotifications
	 */
	public void setSystemNotifications(SystemNotification[] pNotifications) {
//		Arrays.sort(pNotifications, new SystemNotificationComparator()); 
		mSystemNotifications = pNotifications;
	}
	
	
	/**
	 * 
	 * @param pFaults
	 */
	public void setSystemFaults(SystemFault[] pFaults) {
		mFaults = pFaults;
	}
	
	
	
	/**
	 * 
	 * @param pJsonArray
	 */
	public void setSystemNotifications(JSONArray pJsonArray) {
		SystemNotification [] systemNotifications = null;
		if(null != pJsonArray)
		{
			systemNotifications = new SystemNotification[pJsonArray.length()];
			for(int i=0; i<pJsonArray.length(); i++)
			{
				JSONObject notificationJson = pJsonArray.optJSONObject(i);
				SystemNotification sn = SystemNotification.fromJson(notificationJson);
				systemNotifications[i] = sn;
			}
		}
		setSystemNotifications(systemNotifications);
		
	}
	
//	public void setSystemNotifications(SystemNotification[] pSystemNotifications) {
//		mSystemNotifications = pSystemNotifications;
//	}
	
	public void setSystemFaults(JSONArray pJsonArray) {
		if(null != pJsonArray)
		{
			mFaults = new SystemFault[pJsonArray.length()];
			for(int i=0; i<pJsonArray.length(); i++)
			{
				String text = pJsonArray.optString(i);
				SystemFault sf = new SystemFault(text);
				mFaults[i] = sf;
			}
		}
		
	}
	
//	public void setSystemFaults(SystemFault[] pFaults) {
//		mFaults = pFaults;
//	}
	
	public void setSystemLog(JSONArray pJsonArray) {
		if(null != pJsonArray)
		{
			mLog = new SystemLog[pJsonArray.length()];
			for(int i=0; i<pJsonArray.length(); i++)
			{
				String text = pJsonArray.optString(i);
				SystemLog sl = new SystemLog(text);
				mLog[i] = sl;
			}
		}
	}
	
	public void setSystemLog(SystemLog[] pLog) {
		mLog = pLog;
	}
	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_ID, getSystemId());
		json.put(ATTRIBUTE_NAME, getSystemName());
		json.put(ATTRIBUTE_TYPE, getPanelType());
		
		if(null != getConfiguration())
		{
			json.put(ATTRIBUTE_CONFIGURATION, getConfiguration().toJson());
		}
		
		
		if(null != getAllPartitions())
		{
			JSONArray partitions = new JSONArray();
			for(int i=0; i<getAllPartitions().length; i++)
			{
				partitions.put(getPartition(i).toJson());
			}
			json.put(ATTRIBUTE_PARTITIONS, partitions);
		}
		
		if(null != getSystemStatusType())
		{
			json.put(ATTRIBUTE_SYSTEM_STATUS, getSystemStatusType().getIndex());
		}
		
		if(null != getAllSystemZones())
		{
			JSONArray zones = new JSONArray();
			for(int i=0; i<getAllSystemZones().length; i++)
			{
				zones.put(getAllSystemZones()[i].toJson());
			}
			json.put(ATTRIBUTE_ZONES, zones);
		}
		
		if(null != getSystemOutputs())
		{
			JSONArray outputs = new JSONArray();
			for(int i=0; i<getSystemOutputs().size(); i++)
			{
				int key = getSystemOutputs().keyAt(i);
				SystemOutput so = getSystemOutputs().get(key);
				outputs.put(so.toJson());
			}
			json.put(ATTRIBUTE_OUTPUTS, outputs);
		}
		
		
		if(null != getSystemNotifications())
		{
			JSONArray notifications = new JSONArray();
			for(int i=0; i<getSystemNotifications().length; i++)
			{
				SystemNotification sn = getSystemNotifications()[i];
				notifications.put(sn.toJson());
			}
			json.put(ATTRIBUTE_NOTIFICATIONS, notifications);
		}
		
		
		if(null != getSystemFaults())
		{
			JSONArray faults = new JSONArray();
			for(int i=0; i<getSystemFaults().length; i++)
			{
				SystemFault sf = getSystemFaults()[i];
				faults.put(sf.toJson());
			}
			json.put(ATTRIBUTE_FAULTS, faults);
		}
		
		
		if(null != getSystemLog())
		{
			JSONArray logs = new JSONArray();
			for(int i=0; i<getSystemLog().length; i++)
			{
				SystemLog sl = getSystemLog()[i];
				logs.put(sl.toJson());
			}
			json.put(ATTRIBUTE_LOGS, logs);
		}
		
		
		if(null != getSystemNotificationsTypesMap())
		{
			JSONArray notificationsTypes = new JSONArray();
			for(int i=0; i<getSystemNotificationsTypesMap().size(); i++)
			{
				NotificationType nt = getSystemNotificationsTypesMap().get(i);
				if(null != nt)
				{
					notificationsTypes.put(nt.toJson());
				}
			}
			json.put(ATTRIBUTE_NOTIFICATIONS_TYPES, notificationsTypes);
		}
		
		
		return json;
	}
	
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static PanelSystem fromJson(JSONObject pJson)
	{
		String id = pJson.optString(ATTRIBUTE_ID);
		String name = pJson.optString(ATTRIBUTE_NAME);
		int type = pJson.optInt(ATTRIBUTE_TYPE);
		
		PanelSystem ps = new PanelSystem(id, name, type);
		
		
		JSONObject configuration = pJson.optJSONObject(ATTRIBUTE_CONFIGURATION);
		if(null != configuration)
		{
			ps.setConfiguration(SystemConfiguration.fromJson(configuration));
		}
		
		JSONArray partitions = pJson.optJSONArray(ATTRIBUTE_PARTITIONS);
		if(null != partitions)
		{
			SystemPartition[] part_ = new SystemPartition[partitions.length()];
			for(int i=0; i<partitions.length(); i++)
			{
				JSONObject p = null;
				try
				{
					p = partitions.getJSONObject(i);
					SystemPartition sp = SystemPartition.fromJson(p);
					
					part_[i] = sp;
				}
				catch(Exception e){
					part_[i] = null;
					e.printStackTrace();
				}
			}
			ps.setSystemPartitions(part_);
		}
		
		int status = pJson.optInt(ATTRIBUTE_SYSTEM_STATUS);
		ps.setmSystemStatusType(TYPES_SYSTEM_STATUS.getAtIndex(status));
		
		JSONArray zones = pJson.optJSONArray(ATTRIBUTE_ZONES);
		if(null != zones)
		{
			SystemZone[] zones_ = new SystemZone[zones.length()];
			for(int i=0; i<zones.length(); i++)
			{
				JSONObject z = null;
				try
				{
					z = zones.getJSONObject(i);
					SystemZone sz = SystemZone.fromJson(z);
					
					zones_[i] = sz;
				}
				catch(Exception e){
					zones_[i] = null;
					e.printStackTrace();
				}
			}
			ps.setSystemZones(zones_);
		}
		
		JSONArray outputs = pJson.optJSONArray(ATTRIBUTE_OUTPUTS);
		if(null != outputs)
		{
			
			SparseArray<SystemOutput> outputs_ = new SparseArray<SystemOutput>();
			for(int i=0; i<outputs.length(); i++)
			{
				JSONObject o = null;
				try
				{
					o = outputs.getJSONObject(i);
					SystemOutput so = SystemOutput.fromJson(o);
					
					outputs_.put(so.getId(), so);
					
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			ps.setSystemOutputs(outputs_);
				
		}

		
		JSONArray notifications = pJson.optJSONArray(ATTRIBUTE_NOTIFICATIONS);
		if(null != notifications)
		{
			SystemNotification[] notifications_ = new SystemNotification[notifications.length()];
			for(int i=0; i<notifications.length(); i++)
			{
				JSONObject n = null;
				try
				{
					n = notifications.getJSONObject(i);
					SystemNotification sn = SystemNotification.fromJson(n);
					
					notifications_[i] = sn;
				}
				catch(Exception e){
					notifications_[i] = null;
					e.printStackTrace();
				}
			}
			ps.setSystemNotifications(notifications_);
		}
		
		JSONArray faults = pJson.optJSONArray(ATTRIBUTE_FAULTS);
		if(null != faults)
		{
			SystemFault[] faults_ = new SystemFault[faults.length()];
			for(int i=0; i<faults.length(); i++)
			{
				JSONObject f = null;
				try
				{
					f = faults.getJSONObject(i);
					SystemFault sf = SystemFault.fromJson(f);
					
					faults_[i] = sf;
				}
				catch(Exception e){
					faults_[i] = null;
					e.printStackTrace();
				}
			}
			ps.setSystemFaults(faults_);
		}
		
		JSONArray logs = pJson.optJSONArray(ATTRIBUTE_LOGS);
		if(null != logs)
		{
			SystemLog[] logs_ = new SystemLog[logs.length()];
			for(int i=0; i<logs.length(); i++)
			{
				JSONObject l = null;
				try
				{
					l = logs.getJSONObject(i);
					SystemLog sl = SystemLog.fromJson(l);
					
					logs_[i] = sl;
				}
				catch(Exception e){
					logs_[i] = null;
					e.printStackTrace();
				}
			}
			ps.setSystemLog(logs_);
		}
		
		JSONArray notificationsTypes = pJson.optJSONArray(ATTRIBUTE_NOTIFICATIONS_TYPES);
		if(null != notificationsTypes)
		{
			SparseArray<NotificationType> notificationsTypes_ = new SparseArray<NotificationType>();
			for(int i=0; i<notificationsTypes.length(); i++)
			{
				JSONObject n = null;
				try
				{
					n = notificationsTypes.getJSONObject(i);
					NotificationType nt = NotificationType.fromJson(n);
					
					notificationsTypes_.put(nt.getNumber(), nt);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			ps.setSystemNotificationsTypesMap(notificationsTypes_);
		}
		
		return ps;
	
	}
}
