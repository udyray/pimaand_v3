package com.pima.apps.ipima.v3.views;

import org.json.JSONObject;
import com.pima.apps.ipima.v3.views.PanelSystem.TYPES_SYSTEM_STATUS;


public class SystemPartition {

	public static String ATTRIBUTE_ID						= "id";
	public static String ATTRIBUTE_NAME						= "name";
	public static String ATTRIBUTE_STATUS					= "status";
	
	private String mId = null;
	private String mName = null;
	private TYPES_SYSTEM_STATUS mStatus = null;
	
	public SystemPartition(String pId, String pName, TYPES_SYSTEM_STATUS pStatus) {
		mId = pId;
		mName = pName;
		mStatus = pStatus;
	}

	public String getId() {
		return mId;
	}

	public void setId(String pId) {
		mId = pId;
	}

	public String getName() {
		return mName;
	}

	public void setmName(String pName) {
		mName = pName;
	}

	public TYPES_SYSTEM_STATUS getStatus() {
		return mStatus;
	}

	public void setStatus(TYPES_SYSTEM_STATUS pStatus) {
		mStatus = pStatus;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_ID, getId());
		json.put(ATTRIBUTE_NAME, getName());
		json.put(ATTRIBUTE_STATUS, getStatus().getIndex());
		
		
		return json;
	}
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemPartition fromJson(JSONObject pJson)
	{
		String id = pJson.optString(ATTRIBUTE_ID);
		String name = pJson.optString(ATTRIBUTE_NAME);
		int status_ = pJson.optInt(ATTRIBUTE_STATUS);
		TYPES_SYSTEM_STATUS status = TYPES_SYSTEM_STATUS.getAtIndex(status_);
		
		
		SystemPartition partition = new SystemPartition(id, name, status);
		
		return partition;
		
	}
	
	
}
