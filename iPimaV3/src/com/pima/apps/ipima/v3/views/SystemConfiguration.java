package com.pima.apps.ipima.v3.views;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.SparseArray;

public class SystemConfiguration {

	public static String ATTRIBUTE_LOOKIN 						= "lookIn";
	public static String ATTRIBUTE_NUMBER_OF_HOMES				= "numberOfHomes";
	public static String ATTRIBUTE_PANEL_TYPE 					= "panelType";
	public static String ATTRIBUTE_SYSTEM_STATUS_KEYS 			= "SystemStatusTextKeys";
	public static String ATTRIBUTE_HOME_BUTTONS_TEXT_KEYS 		= "homeButtonsTextKeys";
	public static String ATTRIBUTE_REMOTE_LOOKIN_ZONES	 		= "remoteLookInZones";
	
	private boolean mLookIn;
	private int mNumberOfHomes;
	private int mPanelType;
	
	private SparseArray<String> mSystemStatusTextKeys;
	private Map<String, String> mHomeButtonsTextKeys;
	private int[] mRemoteLookInZones;
	
	
	public SystemConfiguration(	boolean pLookIn, int pNumberOfHomes,
								int pPanelType, SparseArray<String> pSystemStatusTextKeys,
								Map<String, String> pHomeButtonsTextKeys, int[] pRemoteLookInZones) {
		super();
		mLookIn = pLookIn;
		mNumberOfHomes = pNumberOfHomes;
		mPanelType = pPanelType;
		mSystemStatusTextKeys = pSystemStatusTextKeys;
		mHomeButtonsTextKeys = pHomeButtonsTextKeys;
		mRemoteLookInZones = pRemoteLookInZones;
	}


	public boolean isLookIn() {
		return mLookIn;
	}


	public void setLookIn(boolean pLookIn) {
		mLookIn = pLookIn;
	}


	public int getNumberOfHomes() {
		return mNumberOfHomes;
	}


	public void setNumberOfHomes(int pNumberOfHomes) {
		mNumberOfHomes = pNumberOfHomes;
	}


	public int getPanelType() {
		return mPanelType;
	}


	public void setPanelType(int pPanelType) {
		mPanelType = pPanelType;
	}


	public SparseArray<String> getSystemStatusTextKeys() {
		return mSystemStatusTextKeys;
	}


	public void setSystemStatusTextKeys(SparseArray<String> pSystemStatusTextKeys) {
		mSystemStatusTextKeys = pSystemStatusTextKeys;
	}


	public Map<String, String> getHomeButtonsTextKeys() {
		return mHomeButtonsTextKeys;
	}


	public void setHomeButtonsTextKeys(Map<String, String> pHomeButtonsTextKeys) {
		mHomeButtonsTextKeys = pHomeButtonsTextKeys;
	}
	
	public int[] getRemoteLookInZones()
	{
		return mRemoteLookInZones;
	}
	
	public void setRemoteLookInZones(int[] pRemoteLookInZones)
	{
		mRemoteLookInZones = pRemoteLookInZones;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_LOOKIN, isLookIn());
		json.put(ATTRIBUTE_NUMBER_OF_HOMES, getNumberOfHomes());
		json.put(ATTRIBUTE_PANEL_TYPE, getPanelType());
		
		JSONObject systemStatusTextKeys = new JSONObject(); 
		if(null != getSystemStatusTextKeys())
		{
			for(int i=0; i<getSystemStatusTextKeys().size(); i++)
			{
				int key = getSystemStatusTextKeys().keyAt(i);
				String val = getSystemStatusTextKeys().get(key);
				
				systemStatusTextKeys.put("" + key, val );
			}
		}
		json.put(ATTRIBUTE_SYSTEM_STATUS_KEYS, systemStatusTextKeys);
		
		JSONObject homeButtonsTextKeys = new JSONObject();
		if(null != getHomeButtonsTextKeys())
		{
			Iterator<String> iter = getHomeButtonsTextKeys().keySet().iterator();
			while(iter.hasNext())
			{
				String key = iter.next();
				String val = getHomeButtonsTextKeys().get(key);
				
				homeButtonsTextKeys.put(key, val );
			}
		}
		json.put(ATTRIBUTE_HOME_BUTTONS_TEXT_KEYS, homeButtonsTextKeys);
		
		JSONArray remoteLookInZonesArr = new JSONArray();
		if(null != getRemoteLookInZones())
		{
			for(int i=0; i<getRemoteLookInZones().length; i++)
			{
				remoteLookInZonesArr.put(getRemoteLookInZones()[i]);
			}
		}
		json.put(ATTRIBUTE_REMOTE_LOOKIN_ZONES, remoteLookInZonesArr);
		return json;
	}
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemConfiguration fromJson(JSONObject pJson)
	{
		boolean lookIn = pJson.optBoolean(ATTRIBUTE_LOOKIN);
		int numberOfHomes = pJson.optInt(ATTRIBUTE_NUMBER_OF_HOMES);
		int panelType = pJson.optInt(ATTRIBUTE_PANEL_TYPE);
		
		JSONObject systemStatusTextKeys = pJson.optJSONObject(ATTRIBUTE_SYSTEM_STATUS_KEYS);
		SparseArray<String> systemStatusTextKeysMap = new SparseArray<String>();
		Iterator<String> keys = (Iterator<String>)systemStatusTextKeys.keys();
		while(keys.hasNext())
		{
			String key = keys.next();
			Integer keyInt = Integer.parseInt(key);
			
			String value = systemStatusTextKeys.optString(key);
			
			systemStatusTextKeysMap.put(keyInt, value);
		}
		
		
		JSONObject homeButtonsTextKeys = pJson.optJSONObject(ATTRIBUTE_HOME_BUTTONS_TEXT_KEYS);
		Map<String, String> homeButtonsTextKeysMap = new HashMap<String, String>();
		keys = (Iterator<String>)homeButtonsTextKeys.keys();
		while(keys.hasNext())
		{
			String key = keys.next();
			String value = systemStatusTextKeys.optString(key);
			
			homeButtonsTextKeysMap.put(key, value);
		}
		
		
		JSONArray remoteLookInZonesArr = pJson.optJSONArray(ATTRIBUTE_REMOTE_LOOKIN_ZONES);
		int[]  remoteLookInZones = new int[remoteLookInZonesArr.length()];
		
		for(int i=0; i<remoteLookInZonesArr.length(); i++)
		{
			int n = remoteLookInZonesArr.optInt(i);
			remoteLookInZones[i] = n;
		}
		
		
		SystemConfiguration configuration = new SystemConfiguration(lookIn, 
																	numberOfHomes, 
																	panelType, 
																	systemStatusTextKeysMap, 
																	homeButtonsTextKeysMap,
																	remoteLookInZones);
		
		return configuration;
		
	}
	
}

