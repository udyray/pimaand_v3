package com.pima.apps.ipima.v3.views;

public class Lookin {

	private int mNumber = -1;
	private int mZoneNumber = -1;
	private String mZoneName = null;

	
	public Lookin(int pNumber, int pZoneNumber, String pZoneName) {
		mNumber = pNumber;
		mZoneNumber = pZoneNumber;
		mZoneName = pZoneName;
		
	}


	public int getNumber() {
		return mNumber;
	}


	public int getZoneNumber() {
		return mZoneNumber;
	}


	public String getZoneName() {
		return mZoneName;
	}

	
	
}
