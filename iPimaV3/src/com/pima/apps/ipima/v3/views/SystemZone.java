package com.pima.apps.ipima.v3.views;

import org.json.JSONObject;

public class SystemZone {

	public static String ATTRIBUTE_NUMBER						= "number";
	public static String ATTRIBUTE_NAME							= "name";
	public static String ATTRIBUTE_ALARM						= "alarm";
	public static String ATTRIBUTE_OPEN							= "open";
	public static String ATTRIBUTE_BYPASS						= "bypass";
	
	
	private int mNumber = -1;
	private String mName = null;
	private boolean mIsAlarm;
	private boolean mIsOpen;
	private boolean mIsBypass;
//	public TYPES_ZONE mStatus = null;
	
	public SystemZone(int pNumber, String pName) {
		mNumber = pNumber;
		mName = pName;
		mIsAlarm = false;
		mIsOpen = false;
		mIsBypass = false;
	}

	public void setAlarm(boolean pIsAlarm)
	{
		mIsAlarm = pIsAlarm;
	}
	
	public void setOpen(boolean pIsOpen)
	{
		mIsOpen = pIsOpen;
	}
	
	public void setBypass(boolean pIsBypass)
	{
		mIsBypass = pIsBypass;
	}
	
	public int getNumber()
	{
		return mNumber;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public boolean getIsAlarm()
	{
		return mIsAlarm;
	}
	
	public boolean getIsOpen()
	{
		return mIsOpen;
	}
	
	public boolean getIsBypass()
	{
		return mIsBypass;
	}
	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_NUMBER, getNumber());
		json.put(ATTRIBUTE_NAME, getName());
		json.put(ATTRIBUTE_ALARM, getIsAlarm());
		json.put(ATTRIBUTE_BYPASS, getIsBypass());
		json.put(ATTRIBUTE_OPEN, getIsOpen());
		
		return json;
	}
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemZone fromJson(JSONObject pJson)
	{
		int number = pJson.optInt(ATTRIBUTE_NUMBER);
		String name = pJson.optString(ATTRIBUTE_NAME);
		boolean alarm = pJson.optBoolean(ATTRIBUTE_ALARM);
		boolean bypass = pJson.optBoolean(ATTRIBUTE_BYPASS);
		boolean open = pJson.optBoolean(ATTRIBUTE_OPEN);
		
		SystemZone zone = new SystemZone(number, name);
		zone.setAlarm(alarm);
		zone.setBypass(bypass);
		zone.setOpen(open);
		
		return zone;
		
	}
}
