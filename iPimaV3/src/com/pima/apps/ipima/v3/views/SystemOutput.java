package com.pima.apps.ipima.v3.views;

import org.json.JSONObject;

public class SystemOutput {

	public static String ATTRIBUTE_ID						= "id";
	public static String ATTRIBUTE_NAME						= "name";
	public static String ATTRIBUTE_ACTIVE					= "active";
	public static String ATTRIBUTE_ENABLED					= "enabled";
	
	private int mId = -1;
	private String mName = null;
	private boolean mIsActive = false;
	private boolean mIsEnabled = false;
	
	public SystemOutput(int pId, String pName, boolean pIsActive, boolean pIsEnabled) {
		mId = pId;
		mName = pName;
		mIsActive = pIsActive;
		mIsEnabled = pIsEnabled;
	}

	public int getId() {
		return mId;
	}

	public String getName() {
		return mName;
	}

	public boolean isActive() {
		return mIsActive;
	}

	public boolean isEnabled() {
		return mIsEnabled;
	}

	public void setId(int pId) {
		mId = pId;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public void setIsActive(boolean pIsActive) {
		mIsActive = pIsActive;
	}

	public void setIsEnabled(boolean pIsEnabled) {
		mIsEnabled = pIsEnabled;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_ID, getId());
		json.put(ATTRIBUTE_NAME, getName());
		json.put(ATTRIBUTE_ACTIVE, isActive());
		json.put(ATTRIBUTE_ENABLED, isEnabled());
	
		return json;
	}
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemOutput fromJson(JSONObject pJson)
	{
		int id = pJson.optInt(ATTRIBUTE_ID);
		String name = pJson.optString(ATTRIBUTE_NAME);
		boolean active = pJson.optBoolean(ATTRIBUTE_ACTIVE);
		boolean enabled = pJson.optBoolean(ATTRIBUTE_ENABLED);
		
		SystemOutput output = new SystemOutput(id, name, active, enabled);
		
		return output;
		
	}

	
}

