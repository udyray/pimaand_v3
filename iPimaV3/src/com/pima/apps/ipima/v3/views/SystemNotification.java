package com.pima.apps.ipima.v3.views;


import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.network.PimaProtocol;

public class SystemNotification {

	public static String ATTRIBUTE_ROW_ID							= "RowId";
	public static String ATTRIBUTE_TYPE								= "Type";
	public static String ATTRIBUTE_MESSAGE_SENT						= "MessageSent";
	public static String ATTRIBUTE_SENT_DATE						= "SentDate";
	public static String ATTRIBUTE_IMAGE_URLS						= "ImageUrls";

	private String mId = null;
	private int mType = -1;
	private String mMessage = null;
	private Date mDate = null;
	private String[] mImagesUrl = null;
	
	private boolean mIsEmpty = false; 
	
	public SystemNotification(String pId, int pType, String pMessage, Date pDate, String[] pImagesUrl) {
		mId = pId;
		mType = pType;
		if(pMessage != null)
		{
			if(pMessage.contains("\\"))
			{
				int index = pMessage.indexOf("\\");
				pMessage = pMessage.substring(0, index).trim();
			}
		}
		mMessage = pMessage;
		mDate = pDate;
		mImagesUrl = pImagesUrl;
	}
	
	
	public String getId() {
		return mId;
	}


	public int getType() {
		return mType;
	}


	public String getMessage() {
		return mMessage;
	}


	public Date getDate() {
		return mDate;
	}

	
	public String getFormatedDate() {
		
		if(null != mDate)
		{
			return PimaProtocol.formatDateSimple(mDate);
		}
		return "";
	}
	
	
	public String getFormatedTime() {
		
		if(null != mDate)
		{
			return PimaProtocol.formatTime(mDate);
		}
		return "";
	}


	public String[] getImagesUrl() {
		return mImagesUrl;
	}


	public void setIsEmpty(boolean pIsEmpty)
	{
		mIsEmpty = pIsEmpty;
	}
	
	public boolean getIsEmpty()
	{
		return mIsEmpty;
	}
	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_ROW_ID, getId());
		json.put(ATTRIBUTE_TYPE, getType());
		json.put(ATTRIBUTE_MESSAGE_SENT, getMessage());
		String date = PimaProtocol.formatDateFull(getDate());
		json.put(ATTRIBUTE_SENT_DATE, date);
	
		JSONArray imageUrlsArr = new JSONArray();
		if(null != getImagesUrl())
		{
			for(int i=0; i<getImagesUrl().length; i++)
			{
				String imageUrl = getImagesUrl()[i];
				imageUrlsArr.put(imageUrl);
			}
			
		}
		json.put(ATTRIBUTE_IMAGE_URLS, imageUrlsArr);
		return json;
	}
	
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemNotification fromJson(JSONObject pJson)
	{

		String rowId = pJson.optString(ATTRIBUTE_ROW_ID);
		int type = pJson.optInt(ATTRIBUTE_TYPE);
		String messageSent = pJson.optString(ATTRIBUTE_MESSAGE_SENT);
		String sentDate = pJson.optString(ATTRIBUTE_SENT_DATE);
		
		Date date = PimaProtocol.parseDate(sentDate);
		
		String[] imagesUrl = null;
		JSONArray imageUrlsArr = pJson.optJSONArray(ATTRIBUTE_IMAGE_URLS);
		if(null != imageUrlsArr)
		{
			imagesUrl = new String[imageUrlsArr.length()];
			for(int i=0; i<imageUrlsArr.length(); i++)
			{
				String imageUrl = imageUrlsArr.optString(i);
				imagesUrl[i] = imageUrl;
			}
		}
		
		
		SystemNotification notification = new SystemNotification(rowId, type, messageSent, date, imagesUrl);
		
		return notification;
	}
}

