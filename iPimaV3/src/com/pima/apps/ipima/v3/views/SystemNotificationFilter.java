package com.pima.apps.ipima.v3.views;

public class SystemNotificationFilter {

	private int mId = -1;
	private String mName = null;
	private boolean mIsActive = false;
	
	public SystemNotificationFilter(int pId, String pName, boolean pIsActive) {
		mId = pId;
		mName = pName;
		mIsActive = pIsActive;
	}

	public int getId() {
		return mId;
	}

	public void setId(int pId) {
		mId = pId;
	}

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public boolean isActive() {
		return mIsActive;
	}

	public void setIsActive(boolean pIsActive) {
		mIsActive = pIsActive;
	}

	
}

