package com.pima.apps.ipima.v3.views;

import org.json.JSONObject;

public class SystemLog {

	private static final String ATTRIBUTE_TEXT = "text";
	
	private String mText = null;
	private boolean mIsEmpty = false;
	
	public SystemLog(String pText) {
		if(pText != null)
		{
			pText = pText.replaceAll("\t", "\n");
			pText = pText.trim();
		}
		mText = pText;
	}

	public void setIsEmpty(boolean pIsEmpty)
	{
		mIsEmpty = pIsEmpty;
	}
	
	public boolean getIsEmpty()
	{
		return mIsEmpty;
	}
	
	public void setText(String pText)
	{
		mText = pText;
	}
	
	public String getText()
	{
		return mText;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_TEXT, getText());
	
		return json;
	}
	
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemLog fromJson(JSONObject pJson)
	{

		String text = pJson.optString(ATTRIBUTE_TEXT);
		
		SystemLog log = new SystemLog(text);
		
		return log;
	}
}

