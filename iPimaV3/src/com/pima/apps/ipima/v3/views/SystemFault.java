package com.pima.apps.ipima.v3.views;


import org.json.JSONObject;

public class SystemFault {

	private static final String ATTRIBUTE_TEXT = "text";
	
	private String mText = null;
	private boolean mIsEmpty = false;
	
	public SystemFault(String pText) {
		mText = pText;
	}

	public void setIsEmpty(boolean pIsEmpty)
	{
		mIsEmpty = pIsEmpty;
	}
	
	public boolean getIsEmpty()
	{
		return mIsEmpty;
	}
	
	
	public void setText(String pText)
	{
		mText = pText;
	}
	
	public String getText()
	{
		return mText;
	}
	
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONObject toJson() throws Exception
	{
		JSONObject json = new JSONObject();
		json.put(ATTRIBUTE_TEXT, getText());
	
		return json;
	}
	
	
	/**
	 * 
	 * @param pJson
	 * @return
	 */
	public static SystemFault fromJson(JSONObject pJson)
	{

		String text = pJson.optString(ATTRIBUTE_TEXT);
		
		SystemFault fault = new SystemFault(text);
		
		return fault;
	}
}

