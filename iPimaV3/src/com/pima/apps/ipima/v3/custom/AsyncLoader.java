package com.pima.apps.ipima.v3.custom;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

public abstract class AsyncLoader<D> extends AsyncTaskLoader<D> {

	private D data;
	
	public AsyncLoader(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void deliverResult(D data) {
		if (isReset()) {
			// An async query came in while the loader is stopped
			return;
		}

		this.data = data;

		super.deliverResult(data);
	}

	@Override
	protected void onStartLoading() { // !important
		if (data != null) {
			deliverResult(data);
		}

		if (takeContentChanged() || data == null) {
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	@Override
	protected void onReset() {
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		data = null;
	}

	protected D getData() {
		return data;
	}
}
