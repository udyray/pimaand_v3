package com.pima.apps.ipima.v3.fragment;

import com.pima.apps.ipima.v3.activity.BaseActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class LoadingDialogFragment extends DialogFragment {
		
	public BaseActivity parentActivity = null;
	
	public void closeLoader() {
			parentActivity.mLoadginDlg = null;
			if(parentActivity != null) {
				parentActivity.hideDialog(BaseActivity.TAG_DIALOG_LOADING);
			}

	}

	public static LoadingDialogFragment newInstance(String text) {
		LoadingDialogFragment f = new LoadingDialogFragment();
		Bundle args = new Bundle();
		args.putString("text", text);
		f.setArguments(args);
//		f.setCancelable(false);

		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String text = getArguments().getString("text");
		
		ProgressDialog progressDialog = new ProgressDialog(getActivity());
		progressDialog.setIndeterminate(true);
		//progressDialog.setCancelable(false); // not working if set here
		
		if (text != null) {
			progressDialog.setMessage(text);
		}

		return progressDialog;
	}
	
}
