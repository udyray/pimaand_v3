package com.pima.apps.ipima.v3.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.pima.apps.ipima.v3.R;


abstract public class BaseFragment extends Fragment{

	public Boolean isDisconnected = false;
	 
	Dialog dialog = null;
	
	Runnable dismiss = new Runnable() {
		
		@Override
		public void run() {
			if(dialog != null)
			{
				try{
					dialog.dismiss();
				}
				catch(Exception e)
				{
					isDisconnected =true;
				}
			}
		}
	};
	
	Handler dismiss_handler = new Handler();
	
	public void dismissLoader() {
		dismiss_handler.removeCallbacks(dismiss);
		dismiss_handler.postDelayed(dismiss, 500);
	}
	
	public void startLoader() {
		if(dialog != null)
		{
			if(!isDisconnected) 
			{
				if(isVisible())
				{
					try
					{
						dialog.show();
					}
					catch(Exception e){
						isDisconnected = true;
					}
				}
				dismiss_handler.removeCallbacks(dismiss);
//				dismiss_handler.postDelayed(dismiss, 29000);
			}
		}
	}
	
	@Override
	public void onResume() {
		
		super.onResume();
		
		if(dialog == null) {
			LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View fl = (View)vi.inflate(R.layout.loader_overlay, null);
			
			dialog = new Dialog(getActivity(), R.style.NewDialog);
			dialog.setContentView(fl);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			
			
			Window window = dialog.getWindow();
			window.setContentView(fl);
		    window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
		            		WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
		    window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		}

	}

}
