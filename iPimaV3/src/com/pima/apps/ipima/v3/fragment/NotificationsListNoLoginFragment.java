package com.pima.apps.ipima.v3.fragment;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.NewLoginActivity;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView.OnUpdateTask;
import com.pima.apps.ipima.v3.util.NotificationsArrayAdapter;
import com.pima.apps.ipima.v3.views.NotificationType;
import com.pima.apps.ipima.v3.views.SystemNotification;

public class NotificationsListNoLoginFragment extends RefreshableFragment{
	
	private NotificationsArrayAdapter mAdapter;
	
	SystemNotification[] mNotifications = null;
	SparseArray<NotificationType> notificationsTypesMap = null;
	
	private TextView notificationsEmpty;
	
	
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{

//		createOverlay();
		
		View view = inflater.inflate(R.layout.fragment_notifications, container, false);
		mListView=(RefreshableListView) view.findViewById(R.id.notificationsrefreshview);
		notificationsEmpty = (TextView)view.findViewById(android.R.id.empty);
		notificationsEmpty.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("NoNotifications"));
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Notifications"));
		}
		
		mListView.setCacheColorHint(0);
		mListView.setOnUpdateTask(new OnUpdateTask() {
			
			public void updateBackground() {
				
				// simulate long times operation.
				try 
				{
					if(getActivity() == null || isResumed() == false || isRemoving()) {
						dismissLoader();
						return;
					}
//					mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
//					mUpdateHandler.removeMessages(REGULAR_UPDATE);
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							if(getActivity() == null || isResumed() == false || isRemoving()) {
								dismissLoader();
								return;
							}
							
							mUpdateHandler.sendEmptyMessage(REGULAR_UPDATE);
							
						}
					});
					while(isReloadPending) {
						Thread.sleep(1000);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			public void updateUI() {
				//aa.notifyDataSetChanged();
			}

			public void onUpdateStart() {
				
			}

		});
        return view;
	}

	
	

	@Override
	public void onResume() {
		
		mListView.setEmptyView(notificationsEmpty);
		notificationsEmpty.setVisibility(View.INVISIBLE);
		
		super.onResume();
	}
	
	
		
//	public void createOverlay() {
//		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//		View v = vi.inflate(R.layout.loader_overlay, null);
//		((MainScreenActivity)getActivity()).replaceLoader(v);
//		
//	}
 

	@Override
	protected void executeUpdate(boolean isBackground) {
		if(null == notificationsTypesMap)
		{
			((NewLoginActivity)getActivity()).getSystemNotificationsTypes();
		}
		else
		{
			((NewLoginActivity)getActivity()).getSystemNotificationsNoLogin(isBackground);
		}
	}


	@Override
	protected void refreshList() {
		
		if(isDisconnected)
		{
			return;
		}
		
		isReloadPending = false;
		
		
		if(null == mNotifications)
		{
			mUpdateHandler.sendEmptyMessageDelayed(REGULAR_UPDATE, 200);
		}
		else
		{
			if(null == mAdapter)
 			{
				mAdapter = new NotificationsArrayAdapter(this.getActivity(), mNotifications, false);
				mListView.setAdapter(mAdapter);
 			}
 			else
 			{
 				mAdapter.updateData(mNotifications);
 				getActivity().runOnUiThread(new Runnable() {
 				    public void run() {
 				    	mAdapter.notifyDataSetChanged();
 				    }
 				});
 			}
			
 			notificationsEmpty.setVisibility(View.VISIBLE);
			mListView.setEmptyView(notificationsEmpty);
		
		}
	}
	
	@Override
	public void onPause() {
		mAdapter = null;
		super.onPause();
	}
	
	public void setNotifications(SystemNotification[] pNotifications)
	{
		mNotifications = pNotifications;
		refreshList();
	}

	
	public void setNotificationsTypes(SparseArray<NotificationType> pNotificationsTypesMap)
	{
		notificationsTypesMap = pNotificationsTypesMap;
	}

}
