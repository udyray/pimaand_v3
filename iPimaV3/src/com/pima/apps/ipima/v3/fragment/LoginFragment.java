package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.NewLoginActivity;
import com.pima.apps.ipima.v3.activity.SystemsActivity;


public class LoginFragment extends BaseFragment implements  View.OnClickListener 
{

	public static final String KEY_SYSTEM_ID = "systemId";
	public static final String KEY_OUTPUT_EDIT = "outputId";
	
	public static final String KEY_IS_LOGGED = "isLogged";

	
	private Button okButton;
	private EditText mEtPassword;

	
	private String systemName="";
	public Boolean connecting = false;
	public Boolean canceledConnection = false;
		
	public static LoginFragment newInstance(Bundle args) {
		LoginFragment f = new LoginFragment();
		f.setArguments(args);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_login, container, false);
		
		systemName = ((NewLoginActivity)getActivity()).mApplication.getConnectedSystem().getSystemName();
		
		okButton = (Button)v.findViewById(R.id.btn_ok);
		okButton.setOnClickListener(LoginFragment.this);
		okButton.setEnabled(false);
		okButton.setText(((NewLoginActivity)getActivity()).mApplication.getLocalizedString("Connect"));
		
		Button cancelButton = (Button)v.findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(LoginFragment.this);
		cancelButton.setText(((NewLoginActivity)getActivity()).mApplication.getLocalizedString("Cancel"));
		
		
		mEtPassword = (EditText)v.findViewById(R.id.et_password);
		mEtPassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(mEtPassword.getText().toString().length() > 0) {
					okButton.setEnabled(true);
				}else {
					okButton.setEnabled(false);
				}
			}
		});
		mEtPassword.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE) {
					if (mEtPassword.getText().toString().length() == 0) {
						
						((NewLoginActivity)getActivity()).mApplication.disconnectLock = false;
						
						okButton.setEnabled(false);
						mEtPassword.setEnabled(false);
						mEtPassword.setFocusable(false);
						mEtPassword.setFocusableInTouchMode(false);
						
						canceledConnection = false;

						connecting = true;
						
					}
				}
				return false;
			}
		});
		
		TextView hintTextView = (TextView)v.findViewById(R.id.hintTextView);
		hintTextView.setText(((NewLoginActivity)getActivity()).mApplication.getLocalizedString("Enter-Code"));
		
		TextView txtAddition = (TextView)v.findViewById(R.id.connectToTextAddition);
		if(txtAddition == null)
		{
			TextView txt = (TextView)v.findViewById(R.id.connectToText);
			txt.setText(((NewLoginActivity)getActivity()).mApplication.getLocalizedString("Connectingto") + " " + systemName);
		}else {
			txtAddition.setText(((NewLoginActivity)getActivity()).mApplication.getLocalizedString("Connectingto") + " " + systemName + " ");

		}
		
		getActivity().setTitle(((NewLoginActivity)getActivity()).mApplication.getLocalizedString("Connect"));
		return v;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				try{
					mEtPassword.requestFocus();
					InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(mEtPassword, InputMethodManager.SHOW_FORCED);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}, 800);
		
		//imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId())
		{
			case R.id.btn_ok:
				
				if (mEtPassword.getText().length() > 0) {
					
					((NewLoginActivity)getActivity()).mApplication.disconnectLock = false;
					
					InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
					
					okButton.setEnabled(false);
					mEtPassword.setFocusable(false);
					mEtPassword.setFocusableInTouchMode(false);
					mEtPassword.setEnabled(false);
					
					canceledConnection = false;
	
					login();
					
					connecting = true;
				}
				break; 
				
			case R.id.btn_cancel:
				if(connecting) {
					
					InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mEtPassword.getWindowToken(), 0);
	
					
					((NewLoginActivity)getActivity()).mApplication.action = SpaApplication.CUR_ACTION.CANCEL_LOGIN;
					
					connecting = false;
					canceledConnection = true;
					
					okButton.setEnabled(true);
					mEtPassword.setEnabled(true);
					mEtPassword.setFocusable(true);
					mEtPassword.setFocusableInTouchMode(true);
	
				}else {

					Intent intent = new Intent(getActivity(), SystemsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				
				}
				break;
	
			default:
				break;
		}
	
	}
	
	private void login() {
		if(null != dialog)
		{
			startLoader();
		}
		((NewLoginActivity)getActivity()).login(mEtPassword.getText().toString());
	}
	
	/**
	 * 
	 * @param enabled
	 */
	public void setEnabled(boolean enabled)
	{
		okButton.setEnabled(true);
		mEtPassword.setEnabled(true);
		mEtPassword.setFocusable(true);
		mEtPassword.setFocusableInTouchMode(true);
	}
}
