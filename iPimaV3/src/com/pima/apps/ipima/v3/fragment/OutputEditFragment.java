package com.pima.apps.ipima.v3.fragment;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.OutputEditActivity;
import com.pima.apps.ipima.v3.util.Utils;

public class OutputEditFragment extends Fragment implements  View.OnClickListener //LoaderCallbacks<OutputModel>
{

	public static final String KEY_SYSTEM_ID = "systemId";
	public static final String KEY_OUTPUT_EDIT = "outputId";
	
	public static final String KEY_IS_LOGGED = "isLogged";

	private static final int LOADER_OUTPUT = 1;
	private static final int TOKEN_OUTPUT_INSERT = 1;
	private static final int TOKEN_OUTPUT_UPDATE = 2;
	private static final int TOKEN_OUTPUT_DELETE = 3;
	private static final int TOKEN_OUTPUT_CHECK_UPDATE_NAME = 4;
	private static final int TOKEN_OUTPUT_CHECK_UPDATE_NUMBER = 5;
	private static final int TOKEN_OUTPUT_CHECK_INSERT_NAME = 6;
	private static final int TOKEN_OUTPUT_CHECK_INSERT_NUMBER = 7;
	
	private TextView nameText, numberText;
	private EditText mEtName, mEtNumber;
//	private AsyncQuery mAQ;
	private long mSystemId = 0, mOutputId= 0;

	public Button doneItem;
	
	private TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			if(validateOutput(mEtName.getText().toString(), mEtNumber.getText().toString())) {
				if(doneItem != null) {
					doneItem.setEnabled(true);
				}
			}else {
				if(doneItem != null) {
					doneItem.setEnabled(false);
				}
			}
		}
	};

	private static enum OUTPUT_MODE {
		ADD,
		EDIT,
		NONE
	}
	private OUTPUT_MODE mOutputMode;
		
	private AssetManager assetManager;
	
	public static OutputEditFragment newInstance(Bundle args) {
		OutputEditFragment f = new OutputEditFragment();
		f.setArguments(args);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_output_edit, container, false);
		
		nameText = (TextView)v.findViewById(R.id.name_text);
		nameText.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs.Add.Name"));
		
		numberText = (TextView)v.findViewById(R.id.number_text);
		numberText.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs.Add.Number"));

		assetManager = getActivity().getAssets();
		
		return v;
	}
	
	
	
	public void onPIMACreateOptionsMenu() {
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = null;
		if (mOutputId != 0) {
			menu = vi.inflate(R.layout.aa_fragments_outputs_edit, null);
		}else {
			menu = vi.inflate(R.layout.aa_fragments_outputs_add, null);
		}
		
//		doneItem = (Button)menu.findViewById(R.id.menu_done);
//		doneItem.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Done"));

		Button help = (Button)menu.findViewById(R.id.menu_help);
		help.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Help"));
		help.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((OutputEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_help);
			}
		});

		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((OutputEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
		((OutputEditActivity)getActivity()).replaceMoreView(menu);
		
		doneItem.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(nameText.getWindowToken(), 0);
				imm.hideSoftInputFromWindow(numberText.getWindowToken(), 0);
				
				((OutputEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_done);
			}
		});
		
		if (mOutputId != 0) {
			mOutputMode = OUTPUT_MODE.EDIT;
			
			getActivity().setTitle( ((BaseActivity)getActivity()).mApplication.getLocalizedString("Edit"));
			
			Button delete = (Button)menu.findViewById(R.id.menu_delete);
			delete.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Delete"));
			delete.setOnClickListener(new Button.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((OutputEditActivity)getActivity()).dismissMoreView();
					onPIMAOptionsItemSelected(R.id.menu_delete);
				}
			});
		}
		else {
			mOutputMode = OUTPUT_MODE.ADD;
			getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("Outputs_AddOutput"));
			
			if(doneItem != null) {
				doneItem.setEnabled(false);
			}
		}
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((OutputEditActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_done:
			switch (mOutputMode) {
			case EDIT:
				checkUpdateName();
				break;
			case ADD:
				checkInsertName();
				break;

			default:
				break;
			}
			break;
		case R.id.menu_help:
			String text = null;
			 InputStream input;
		        try 
		        {
		        	input = assetManager.open(((SpaApplication)getActivity().getApplication()).getLocalizedString("Help.Filename"));
		            
		            int size = input.available();
		            byte[] buffer = new byte[size];
		            input.read(buffer);
		            input.close();
		 
		            // byte buffer into a string
		            text = new String(buffer);
		             
		        } catch (IOException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
			
			((OutputEditActivity)getActivity()).showHelpDialog(text);
			break;
		case R.id.menu_discard:
			getActivity().finish();
			break;
		case R.id.menu_delete:
			processDelete();
			getActivity().finish();
			break;
		}
	}
	
	
	private Boolean validateOutput(String name, String number) {
		if(name == null || name.length() == 0) {
			return false;
		}
		if(number == null || number.length() == 0) {
			return false;
		}

		if(!number.matches("^[0-9]*$")) {
			return false;
		}

		return true;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mEtName   = (EditText)getActivity().findViewById(R.id.et_name);
		mEtNumber = (EditText)getActivity().findViewById(R.id.et_number);
		
		mEtName.addTextChangedListener(textWatcher);
		mEtNumber.addTextChangedListener(textWatcher);
		
		mEtNumber.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE) {
					if (mEtName.getText().length() > 0 && mEtNumber.getText().length() > 0) {
						switch (mOutputMode) {
						case EDIT:
							checkUpdateName();
							break;
						case ADD:
							checkInsertName();
							break;

						default:
							break;
						}
					}
				}
				return false;
			}
		});

		
//		mAQ = new AsyncQuery(getActivity().getContentResolver());
		
		mSystemId = getArguments() != null ? getArguments().getLong(KEY_SYSTEM_ID) : 0;
		mOutputId = getArguments() != null ? getArguments().getLong(KEY_OUTPUT_EDIT) : 0;
		
		if (mSystemId == 0) {
			getActivity().finish();
			return;
		}
		
//		
//		if (mOutputId != 0) {
//			mOutputMode = OUTPUT_MODE.EDIT;
//			getLoaderManager().initLoader(LOADER_OUTPUT, getArguments(), OutputEditFragment.this);
//			
//			getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("Edit"));
//		}
//		else {
//			mOutputMode = OUTPUT_MODE.ADD;
//			getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("Outputs_AddOutput"));
//		}
		
		onPIMACreateOptionsMenu();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_ok:
			switch (mOutputMode) {
			case EDIT:
				processUpdate();
				break;
			case ADD:
				processInsert();
				break;

			default:
				break;
			}
			
			break;
			
		case R.id.btn_cancel:
			getActivity().finish();
			break;

		default:
			break;
		}
	}
	
	
	private void checkUpdateName() {
		String name;
		name = mEtName.getText().toString().trim();
		if (Utils.isEmpty(name)) {
			return;
		}
		
//		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
//		mAQ.startQuery(TOKEN_OUTPUT_CHECK_UPDATE_NAME, null, uri, new String[] {TableOutputs.COL_NAME, TableOutputs.COL_SYSTEM_ID, TableOutputs.COL_ID}, TableOutputs.COL_NAME + " LIKE ? AND " + TableOutputs.COL_SYSTEM_ID + " = ?", new String[] {name+"", mSystemId + ""}, null);
	}
	
//	private void checkUpdateNumber() {
//		String number;
//		number = mEtNumber.getText().toString().trim();
//		if (Utils.isEmpty(number)) {
//			return;
//		}
//		
////		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
////		mAQ.startQuery(TOKEN_OUTPUT_CHECK_UPDATE_NUMBER, null, uri, new String[] {TableOutputs.COL_NAME, TableOutputs.COL_NUMBER, TableOutputs.COL_SYSTEM_ID, TableOutputs.COL_ID}, TableOutputs.COL_NUMBER + " = ? AND " + TableOutputs.COL_SYSTEM_ID + " = ?" , new String[] {number+"", mSystemId + ""}, null);
//	}
	
	private void checkInsertName() {
		String name;
		name = mEtName.getText().toString().trim();
		if (Utils.isEmpty(name)) {
			return;
		}
		
//		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
//		mAQ.startQuery(TOKEN_OUTPUT_CHECK_INSERT_NAME, null, uri, new String[] {TableOutputs.COL_NAME, TableOutputs.COL_SYSTEM_ID, TableOutputs.COL_ID}, TableOutputs.COL_NAME + " LIKE ? AND " + TableOutputs.COL_SYSTEM_ID + " = ?", new String[] {name+"", mSystemId + ""}, null);
	}
	
//	private void checkInsertNumber() {
//		String number;
//		number = mEtNumber.getText().toString().trim();
//		if (Utils.isEmpty(number)) {
//			return;
//		}
//		
////		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
////		mAQ.startQuery(TOKEN_OUTPUT_CHECK_INSERT_NUMBER, null, uri, new String[] {TableOutputs.COL_NAME, TableOutputs.COL_NUMBER, TableOutputs.COL_SYSTEM_ID, TableOutputs.COL_ID}, TableOutputs.COL_NUMBER + " = ? AND " + TableOutputs.COL_SYSTEM_ID + " = ?", new String[] {number+"", mSystemId + ""}, null);
//	}
	
	private void processUpdate() {
		String name, number;
		name = mEtName.getText().toString().trim();
		number = mEtNumber.getText().toString().trim();
		if (Utils.isEmpty(name) || Utils.isEmpty(number)) {
			return;
		}
		
//		ContentValues cv = new ContentValues();
//		cv.put(TableOutputs.COL_NAME, name);
//		cv.put(TableOutputs.COL_NUMBER, number);
//		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
//		mAQ.startUpdate(TOKEN_OUTPUT_UPDATE, null, uri, cv, 
//				TableOutputs.COL_ID + " = ? AND " + TableOutputs.COL_SYSTEM_ID + " = ? ", 
//				new String[] {mOutputId+"", mSystemId+""});
	}
	
	private void processInsert() {
		String name, number;
		name = mEtName.getText().toString().trim();
		number = mEtNumber.getText().toString().trim();
		if (Utils.isEmpty(name) || Utils.isEmpty(number)) {
			return;
		}
		
//		ContentValues cv = new ContentValues();
//		cv.put(TableOutputs.COL_NAME, name);
//		cv.put(TableOutputs.COL_NUMBER, number);
//		cv.put(TableOutputs.COL_SYSTEM_ID, mSystemId);
//		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
//		mAQ.startInsert(TOKEN_OUTPUT_INSERT, null, uri, cv);
	}
	
	private void processDelete() {

//		Uri uri = SpaContentProvider.CONTENT_URI_OUTPUTS;
//		mAQ.startDelete(TOKEN_OUTPUT_DELETE, null, uri, TableOutputs.COL_ID + " = ? AND " + TableOutputs.COL_SYSTEM_ID + " = ? ", 
//				new String[] {mOutputId+"", mSystemId+""});
	}
	
//	private void populateViews(OutputModel model) {
//		mEtName.setText(model.getName());
//		mEtNumber.setText(model.getNumber()+"");
//	}
	
//	private class AsyncQuery extends AsyncQueryHandler {
//
//		public AsyncQuery(ContentResolver cr) {
//			super(cr);
//		}
//		
//		@Override
//		protected void onInsertComplete(int token, Object cookie, Uri uri) {
//			if (token == TOKEN_OUTPUT_INSERT && uri != null) {
//				getActivity().finish();
//			}
//		}
//		
//		@Override
//		protected void onUpdateComplete(int token, Object cookie, int result) {
//			if (token == TOKEN_OUTPUT_UPDATE && result > 0) {
//				getActivity().finish();
//			}
//		}
//		
//		@Override
//		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
//			if (token == TOKEN_OUTPUT_CHECK_UPDATE_NAME)
//			{
//				if(cursor == null || cursor.moveToFirst() == false) {
//					checkUpdateNumber();
//					return;
//				}else if(cursor.getInt(cursor.getColumnIndex(TableOutputs.COL_ID)) == mOutputId || 
//						cursor.getInt(cursor.getColumnIndex(TableOutputs.COL_SYSTEM_ID)) != mSystemId) {
//					checkUpdateNumber();
//					cursor.close();
//					return;
//				}else
//				{
//					Bundle bundle = new Bundle();
//					bundle.putString("text", ((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs.Add.Error.DuplicateName"));
//					((BaseActivity)getActivity()).showDialog("dialogInfo", bundle);
//					cursor.close();
//					return;
//				}
//			}
//			if (token == TOKEN_OUTPUT_CHECK_UPDATE_NUMBER)
//			{
//				if(cursor == null || cursor.moveToFirst() == false) {
//					processUpdate();
//					return;
//				} else if(cursor.getInt(cursor.getColumnIndex(TableOutputs.COL_ID)) == mOutputId  || 
//						cursor.getInt(cursor.getColumnIndex(TableOutputs.COL_SYSTEM_ID)) != mSystemId) {
//					processUpdate();
//					cursor.close();
//					return;
//				}else
//				{
//					Bundle bundle = new Bundle();
//					bundle.putString("text", ((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs.Add.Error.DuplicateNumber"));
//					((BaseActivity)getActivity()).showDialog("dialogInfo", bundle);
//					cursor.close();
//					return;
//				}
//			}
//			
//			if (token == TOKEN_OUTPUT_CHECK_INSERT_NAME)
//			{
//				if(cursor == null || cursor.moveToFirst() == false) {
//					checkInsertNumber();
//					return;
//				}else if(cursor.getInt(cursor.getColumnIndex(TableOutputs.COL_SYSTEM_ID)) != mSystemId) {
//					checkInsertNumber();
//					cursor.close();
//					return;
//				}else
//				{
//					Bundle bundle = new Bundle();
//					bundle.putString("text", ((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs.Add.Error.DuplicateName"));
//					((BaseActivity)getActivity()).showDialog("dialogInfo", bundle);
//					cursor.close();
//					return;
//				}
//			}
//			if (token == TOKEN_OUTPUT_CHECK_INSERT_NUMBER)
//			{
//				if(cursor == null || cursor.moveToFirst() == false) {
//					processInsert();
//					return;
//				}else if(cursor.getInt(cursor.getColumnIndex(TableOutputs.COL_SYSTEM_ID)) != mSystemId) {
//					processInsert();
//					cursor.close();
//					return;
//				}else
//				{
//					Bundle bundle = new Bundle();
//					bundle.putString("text", ((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs.Add.Error.DuplicateNumber"));
//					((BaseActivity)getActivity()).showDialog("dialogInfo", bundle);
//					cursor.close();
//					return;
//				}
//			}
//		}
//
//	}

	/*** Loader callbacks ***
	
	@Override
	public Loader<OutputModel> onCreateLoader(int id, Bundle args) {
		if (id == LOADER_OUTPUT) {
			return new OutputLoader(getActivity(), args.getLong(KEY_SYSTEM_ID), args.getLong(KEY_OUTPUT_EDIT));
		}
		
		return null;
	}

	@Override
	public void onLoadFinished(Loader<OutputModel> loader, OutputModel model) {
		if (model != null) {
			populateViews(model);
		}
	}

	@Override
	public void onLoaderReset(Loader<OutputModel> loader) {
		// TODO Auto-generated method stub
		
	}
	
	private static class OutputLoader extends AsyncLoader<OutputModel> {

		private long mOutputId, mSystemId;
		private Context mCtx;
		
		public OutputLoader(Context context, long systemId, long outputId) {
			super(context);
			
			this.mOutputId = outputId;
			this.mSystemId = systemId;
			this.mCtx = context;
		}

		@Override
		public OutputModel loadInBackground() {
			Cursor cursor = mCtx.getContentResolver().query(SpaContentProvider.CONTENT_URI_OUTPUTS, 
					null, TableOutputs.COL_ID + " = ? AND " + TableOutputs.COL_SYSTEM_ID + " = ?", 
					new String[] {mOutputId+"", mSystemId+""}, null);
			OutputModel outputModel = null; 
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					outputModel = OutputModel.fromCursor(cursor);
				}
				
				cursor.close();
			}
			
			return outputModel;
		}
	}
	******/
}
