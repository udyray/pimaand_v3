package com.pima.apps.ipima.v3.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup; 
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ToggleButton;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaPreference;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.SystemEditActivity;
import com.pima.apps.ipima.v3.gcm.RegistrationIntentService;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.views.SystemNotificationFilter;

public class NotificationsFiltersListFragment extends BaseFragment {
	
	private ListView mListView;	
	
	ToggleButton tButton;
	public Button doneItem;
//	private View empty_view = null;
	
	public static final String LOG_TAG = "NotificationsFiltersListFragment";
	
	private NotificationsFiltersArrayAdapter mAdapter;
	private String mSystemId;
//	private SystemModel mSystemModel;
	
	
	private SystemNotificationFilter[] filtersToUpdate;
	
	public static NotificationsFiltersListFragment newInstance(Bundle args) {
		NotificationsFiltersListFragment fragment = new NotificationsFiltersListFragment();
		fragment.setArguments(args);
		return fragment;
	}	
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		createScreen();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_notifications_filters, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mSystemId = getArguments() != null ? getArguments().getString(SystemEditActivity.KEY_SYSTEM_EDIT) : null;
		if(mSystemId == null)
		{
			getActivity().finish();
			return;
		}
		
//		mSystemModel = getSystemById(mSystemId);
		if (((BaseActivity)getActivity()).mApplication.getConnectedSystem() == null) {
			getActivity().finish();
			return;
		}
		
		mListView=(ListView)getActivity().findViewById(R.id.notificationsFilterList);
		mListView.setCacheColorHint(0);
		
		((SystemEditActivity)getActivity()).setTitle("Notifications");
		
		TextView notificationsOnOffTitle = (TextView)getActivity().findViewById(R.id.notificationsOnOffTitle);
		notificationsOnOffTitle.setText(((BaseActivity)getActivity()).mApplication.getLocalizedString("ActivateNotifications"));
		
		TextView selectNotificationsTypeTitle = (TextView)getActivity().findViewById(R.id.selectNotificationsTypeTitle);
		selectNotificationsTypeTitle.setText(((BaseActivity)getActivity()).mApplication.getLocalizedString("SelectEvents"));
		
		
		Button switchButton = (Button)getActivity().findViewById(R.id.notificationsOnOffTouchButton);
		tButton = (ToggleButton) getActivity().findViewById(R.id.notificationsOnOffToggleButton);
		
		switchButton.setVisibility(View.INVISIBLE);
		tButton.setVisibility(View.VISIBLE);
		
		boolean isEnable = SpaPreference.getEnableNotifications(getActivity());
		if(isEnable)
		{
			tButton.setChecked(true);
		}
		else
		{
			tButton.setChecked(false);
		}
		
		
		tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
				
				//SET AS Active/Not Active
				boolean isEnable = SpaPreference.getEnableNotifications(getActivity());
				SpaPreference.setEnableNotifications(getActivity(), isChecked );
				
				if(null != dialog)
				{
					startLoader();
				}
				if(!isEnable)
				{
					//check if we have token
					if(null == SpaPreference.getGCMToken(getContext()))
					{
						//start GCM register service
						Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
	    				intent.putExtra(RegistrationIntentService.EXTRAS_ACTION, RegistrationIntentService.ACTION_REGISTER);
	    				getActivity().startService(intent);
					}
					
    				
    				((SystemEditActivity)getActivity()).sendRegistrationToServer();
    				mListView.setEnabled(true);
				}
				else
				{
					//start GCM unregister service
//					Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
//    				intent.putExtra(RegistrationIntentService.EXTRAS_ACTION, RegistrationIntentService.ACTION_UNREGISTER);
//    				getActivity().startService(intent);
    				
					((SystemEditActivity)getActivity()).setNotificationsFiltersOff();
					
    				mListView.setEnabled(false);
				}
				
			}
		});
		
//		switchButton.setTag(tButton);
//		switchButton.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {	
//				System.out.println("switchButton onClick");
//				ToggleButton tb = (ToggleButton)v.getTag();
//				
//				if(tb.isChecked())
//				{
//					tb.setChecked(false);
//				}
//				else
//				{
//					tb.setChecked(true);
//				}
//			}
//		});
		

        
		//mListView.setOnItemClickListener(mListItemClickListener);
		registerForContextMenu(mListView);
		mListView.setBackgroundResource(R.drawable.background);
		
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new NotificationsFiltersArrayAdapter(getActivity(), null);
		mListView.setAdapter(mAdapter);

		onPIMACreateOptionsMenu();
	}
	
	public void setFilters(SystemNotificationFilter[] pFilters)
	{
		filtersToUpdate = pFilters;
		fillData();
	}
	
	private void fillData() {	
		
		boolean isEnable = SpaPreference.getEnableNotifications(getActivity());
		if(isEnable)
		{
			if(null == mAdapter)
	 		{
				mAdapter = new NotificationsFiltersArrayAdapter(getActivity(), filtersToUpdate);
	 			mListView.setAdapter(mAdapter);
	 		}
	 		else
	 		{
	 			mAdapter.updateData(filtersToUpdate);
	 			mAdapter.notifyDataSetChanged();
	 			mListView.setAdapter(mAdapter);
	 		}
		}
		else
		{
			mAdapter = new NotificationsFiltersArrayAdapter(getActivity(), null);
 			mListView.setAdapter(mAdapter);
		}
		
		ViewGroup parentGroup = (ViewGroup)mListView.getParent();
		View empty = getActivity().getLayoutInflater().inflate(R.layout.empty_notifications_filters_list,
		                                                       parentGroup,
		                                                       false);
		
		TextView emptyText = (TextView)empty.findViewById(R.id.notifications_filter_empty_id);
		emptyText.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("empty_filters"));
		
		parentGroup.addView(empty);
		mListView.setEmptyView(empty);
		
		if(dialog != null)
		{
			dismissLoader();
		}
	}

	
	/*** Options Menu ***/
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = vi.inflate(R.layout.aa_fragment_notifications_filters_list, null);
		
		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SystemEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
		((SystemEditActivity)getActivity()).replaceMoreView(menu);
		
		doneItem.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(null != dialog)
				{
					startLoader();
				}
				
				List<Integer> ids = new ArrayList<Integer>(); 
				for(int i=0; i<filtersToUpdate.length; i++)
				{
					SystemNotificationFilter sn = filtersToUpdate[i];
					if(sn.isActive())
					{
						ids.add(sn.getId());
					}
				}
				
				((SystemEditActivity)getActivity()).setNotificationsFiltersEnable(ids);
			}
		});
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((SystemEditActivity)getActivity()).dismissMoreView();
				}
			});
		}
		
	}

	
	
	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		
			case R.id.menu_discard:
				((SystemEditActivity)getActivity()).swapToEdit();
				break;
		
		}
	}
	
	
	/**
	 * 
	 */
	private void createScreen() {
		
		if(null != dialog)
		{
			startLoader();
		}
		
		((SystemEditActivity)getActivity()).getNotificationsFilters();
	}
	
	public class NotificationsFiltersArrayAdapter extends BaseAdapter
	{
		SystemNotificationFilter[] mObjects;
		Context mContext;
		
		public NotificationsFiltersArrayAdapter(Context pContext, SystemNotificationFilter[] pObjects){
			super();
			mContext = pContext;
			mObjects = pObjects;
		}

		@Override
		public int getCount() {
			if(null != mObjects)
			{
				return mObjects.length;
			}
			return 0;
		}

		@Override
		public Object getItem(int position) {
			if(position < getCount())
			{
				return mObjects[position];
			}
			
			return null;
		}

		@Override
		public long getItemId(int position) {
			
			if(null != getItem(position))
			{
				SystemNotificationFilter so = (SystemNotificationFilter)getItem(position);
				return so.getId();
			}
			return -1;
		}

		public void updateData(SystemNotificationFilter[] pObjects)
		{
			mObjects = pObjects;
		}
		
		@SuppressLint("ResourceAsColor")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			FrameLayout rowView = (FrameLayout)convertView;
			if(rowView == null)
			{
				rowView = (FrameLayout)mInflater.inflate(R.layout.notifications_filter_list_item, null);
			}
			
			final SystemNotificationFilter row = (SystemNotificationFilter)getItem(position);
			
			Integer id = row.getId();
			
			TextView tv = (TextView)rowView.findViewById(R.id.notifications_filter_text);
			tv.setText(((BaseActivity)getActivity()).mApplication.getLocalizedString(row.getName()));
			
			Button switchButton = (Button)rowView.findViewById(R.id.touchButton);
			ToggleButton tButton = (ToggleButton) rowView.findViewById(R.id.toggleButton1);
			tButton.setTag(id);
			
			switchButton.setVisibility(View.VISIBLE);
			tButton.setVisibility(View.VISIBLE);
			
			tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
					//SET AS Active/Not Active
					filtersToUpdate[position].setIsActive(isChecked);
					row.setIsActive(isChecked);
					doneItem.setEnabled(true);
				}
			});
			Boolean isActive = row.isActive();
			tButton.setChecked(isActive);	
			
			switchButton.setTag(tButton);
			switchButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {	
					ToggleButton tb = (ToggleButton)v.getTag();
					
					if(tb.isChecked())
					{
						tb.setChecked(false);
					}else
					{
						tb.setChecked(true);
					}
				}
			});
			
			return rowView;
		
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}
	}

}
