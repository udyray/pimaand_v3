package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.pulltorefresh.PullToRefreshListView;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView.OnUpdateTask;
import com.pima.apps.ipima.v3.util.ZonesArrayAdapter;
import com.pima.apps.ipima.v3.views.SystemZone;

public class ZonesFragment extends RefreshableFragment{

	SystemZone[] zoneNames = null;
	ZonesArrayAdapter mAdapter;
	private TextView zonesEmpty;
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mAdapter = null;
		((SpaApplication)getActivity().getApplication()).isBypassZonesFragment = false;
		
		zoneNames = ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getActiveSystemZones();
		
		onPIMACreateOptionsMenu();
		
		View view = inflater.inflate(R.layout.fragment_zones, container, false);		
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Zones"));
		}
		
		TextView zoneOpen = (TextView)view.findViewById(R.id.zoneOpen);
		if(zoneOpen != null) {
			zoneOpen.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Open"));
		}
		
		TextView zoneAlarm = (TextView)view.findViewById(R.id.zoneAlarm);
		if(zoneAlarm != null) {
			zoneAlarm.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Alarmed"));
		}
		
		TextView zoneBypass = (TextView)view.findViewById(R.id.zoneBypass);
		if(zoneBypass != null) {
			zoneBypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypassed"));
		}
		
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int titleWidth = (int)Math.round((width*0.55));
		int columnWidth = (int)Math.round((width*0.15));

		LayoutParams titleParams = titleView.getLayoutParams();
		titleParams.width = titleWidth;
		titleView.setLayoutParams(titleParams);
		
		LayoutParams columnParams = zoneOpen.getLayoutParams();
		columnParams.width = columnWidth;
		zoneOpen.setLayoutParams(columnParams);
		zoneAlarm.setLayoutParams(columnParams);
		zoneBypass.setLayoutParams(columnParams);
		

		mListView = (PullToRefreshListView) view.findViewById(R.id.zonesrefreshview);
		zonesEmpty = (TextView)view.findViewById(android.R.id.empty);
		zonesEmpty.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("NoZones"));
		
		
		mListView.setBackgroundColor(getResources().getColor(R.color.clear));
		mListView.setCacheColorHint(0);
		

		
		((RefreshableListView)mListView).setOnUpdateTask((new OnUpdateTask() {
			
			public void updateBackground() {
				
				// simulate long times operation.
				try 
				{
					if(getActivity() == null || isResumed() == false || isRemoving()) {
						dismissLoader();
						return;
					}
					isReloadPending = true;
					
//					mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
//					mUpdateHandler.removeMessages(REGULAR_UPDATE);
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if(getActivity() == null || isResumed() == false || isRemoving()) {
								dismissLoader();
								return;
							}
							
							mUpdateHandler.sendEmptyMessage(REGULAR_UPDATE);	
						}
					});
					while(isReloadPending) {
						Thread.sleep(1000);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			public void updateUI() {
				//aa.notifyDataSetChanged();
			}

			public void onUpdateStart() {

			}

		}));
	
        return view;
	}
	
	
	@Override
	public void onResume() {

		super.onResume();
		
		mListView.setEmptyView(zonesEmpty);
		zonesEmpty.setVisibility(View.INVISIBLE);
	}

	/**
	 * 
	 */
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View menu = vi.inflate(R.layout.aa_zones_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button log = (Button)menu.findViewById(R.id.menu_log);
		log.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		log.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_log);
			}
		});
		
		Button faults = (Button)menu.findViewById(R.id.menu_faults);
		faults.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Faults"));
		faults.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_faults);
			}
		});
		
		Button lookin = (Button)menu.findViewById(R.id.menu_lookin);
		lookin.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		lookin.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_lookin);
			}
		});
		
		if(	((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration() != null &&
			((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().isLookIn())
		{
			lookin.setVisibility(View.VISIBLE);
		}
		else
		{
			lookin.setVisibility(View.GONE);
		}
		
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
//		Button refresh = (Button)menu.findViewById(R.id.menu_refresh_zones_names);
		Button bypass = (Button)menu.findViewById(R.id.menu_bypass_zones);
//			refresh.setVisibility(View.VISIBLE);
//			refresh.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_refresh_zones_names"));
//			refresh.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((MainScreenActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_refresh_zones_names);
//				}
//			});
		bypass.setVisibility(View.VISIBLE);
		bypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		bypass.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_bypass_zones);
			}
		});
		
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	/**
	 * 
	 * @param menu_id
	 */
	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_logout:
			((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
			((BaseActivity)getActivity()).disconnect();
			break;
			
		case R.id.menu_bypass_zones:
			((MainScreenActivity)getActivity()).loadBypassZonesFragment();
			break;
			
		case R.id.menu_log:
			((MainScreenActivity)getActivity()).selectLogs();
			break;
			
		case R.id.menu_faults:
			((MainScreenActivity)getActivity()).selectFaults();
			break;
			
		case R.id.menu_lookin:
			((MainScreenActivity)getActivity()).selectLookin();
			break;
		}
	}

	
	@Override
	protected void executeUpdate(boolean isBackground) {
		((MainScreenActivity)getActivity()).getSystemStatus(isBackground);
	}


	@Override
	protected void refreshList() {

		if(isDisconnected)
		{
			return;
		}
		
		isReloadPending = false;
		
		if(null == mAdapter)
 		{
			mAdapter = new ZonesArrayAdapter(getActivity(), new SystemZone[0]);
			mListView.setAdapter(mAdapter);
 			mUpdateHandler.sendEmptyMessageDelayed(REGULAR_UPDATE, 200);
 		}
 		else
 		{
 			mAdapter.updateData(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getActiveSystemZones());
 			getActivity().runOnUiThread(new Runnable() {
			    public void run() {
			    	mAdapter.notifyDataSetChanged();
			    }
			});
 			
 			zonesEmpty.setVisibility(View.VISIBLE);
			mListView.setEmptyView(zonesEmpty);
 		}
		
	
	}

	@Override
	public void onPause() {
		mAdapter = null;
		super.onPause();
	}
}
