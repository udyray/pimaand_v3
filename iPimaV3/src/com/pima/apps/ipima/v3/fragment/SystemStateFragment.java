package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.views.PanelSystem;
import com.pima.apps.ipima.v3.views.PanelSystem.TYPES_SYSTEM_STATUS;


public class SystemStateFragment extends RefreshableFragment
{
	private static final int NOT_SELECTED = -2;
	private static final int WHOLE_SYSTEM = -1;
	
	int selectedItem = NOT_SELECTED;
	TYPES_SYSTEM_STATUS selectedItemStatus = null;
	
	int partitionNumber = WHOLE_SYSTEM;
	String partitionName = null;
	
	TextView currentStatus;

	TextView armingOptionsAddition;
	TextView currentStateText;
	TextView currentStateTextAddition;
	
	TextView homeText1;
	TextView homeText2;
	TextView homeText3;
	TextView homeText4;
	TextView textFull;
	
	ImageView lockImageView;
	Button disarmBtn;
	ImageButton fullBtn;
	ImageButton home1Btn;
	ImageButton home2Btn;
	ImageButton home3Btn;
	ImageButton home4Btn;
//	Button enableRemoteBtn;
	
	FrameLayout full;
	FrameLayout home1;
	FrameLayout home2;
	FrameLayout home3;
	FrameLayout home4;
	
	BaseActivity callingActivity;
	LinearLayout armingOptionsLayout;
	
	PanelSystem panelSystem;
	
	TextView loaderText = null;
	
	
	public SystemStateFragment(BaseActivity activity) {
		super();
		callingActivity=activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_system_state, container, false);
		onPIMACreateOptionsMenu();
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getSystemName());
		}
		
		if(	((SpaApplication)getActivity().getApplication()).getLocalization() != null && 
			((SpaApplication)getActivity().getApplication()).getLocalization().equals("ru")) {
			LinearLayout currentStateLayout = (LinearLayout)view.findViewById(R.id.currentStateLayout);
			currentStateLayout.setPadding(0, 40, 0, 0);
			RelativeLayout.LayoutParams cslp = (RelativeLayout.LayoutParams)currentStateLayout.getLayoutParams();
			cslp.setMargins(-11, 0, 0, 0);
			currentStateLayout.setLayoutParams(cslp);
		}
		
		currentStatus=(TextView) view.findViewById(R.id.currentStateTV);
		currentStateTextAddition = (TextView)view.findViewById(R.id.currentStateTextTVAddition);
		
		
		currentStateText = (TextView)view.findViewById(R.id.currentStateTextTV);
		
		if(selectedItem >= 0)
		{
			String part = callingActivity.mApplication.getLocalizedString("label_partition_short_m");
			currentStateTextAddition.setText(part + partitionNumber);
			
			currentStateText.setText(partitionName + ":");
			currentStateTextAddition.setVisibility(View.VISIBLE);
			
		} 
		else 
		{
			//DO PARTITIONS FROM JSON
			currentStateText.setText(callingActivity.mApplication.getLocalizedString("SystemStatus"));
		}
		
		
//		armingOptions=(TextView) view.findViewById(R.id.armingOptionsTextView);
//		armingOptions.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("System.Options"));
		armingOptionsAddition=(TextView) view.findViewById(R.id.armingOptionsTextViewAddition);
		armingOptionsLayout= (LinearLayout) view.findViewById(R.id.layoutArmingOptions);
		
		textFull = (TextView)view.findViewById(R.id.textFull);
		textFull.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Full"));
//		textFull.setText(getSystemStatusText(TYPES_SYSTEM_STATUS.Full));
		homeText1 = (TextView)view.findViewById(R.id.homeText1);
//		homeText1.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("HOME1_1"));
		homeText1.setText(getSystemStatusText(TYPES_SYSTEM_STATUS.Home1));
		homeText2 = (TextView)view.findViewById(R.id.homeText2);
//		homeText2.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("HOME2_1"));
		homeText2.setText(getSystemStatusText(TYPES_SYSTEM_STATUS.Home2));
		homeText3 = (TextView)view.findViewById(R.id.homeText3);
//		homeText3.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("HOME3_1"));
		homeText3.setText(getSystemStatusText(TYPES_SYSTEM_STATUS.Home3));
		homeText4 = (TextView)view.findViewById(R.id.homeText4);
//		homeText4.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("HOME4_1"));
		homeText4.setText(getSystemStatusText(TYPES_SYSTEM_STATUS.Home4));
		
		
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float logicalDensity = (metrics.ydpi / 160f);
		
		int screen_height = getResources().getDisplayMetrics().heightPixels;
		double screen_height_dp = ((double) screen_height) / logicalDensity;

		
		disarmBtn = (Button) view.findViewById(R.id.disarmBtn);
		if(screen_height_dp <= 480) { //fixed 13dp margin
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)disarmBtn.getLayoutParams();
			lp.bottomMargin = (int)(13*logicalDensity);
		}else if(screen_height_dp <= 511) { //floating 14dp-44dp margin
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)disarmBtn.getLayoutParams();
			lp.bottomMargin = (int)((screen_height_dp - 467)*logicalDensity);
		}//in the xml it is 45dp
		disarmBtn.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disarm"));
		disarmBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Disarm
				mUpdateHandler.removeMessages(REGULAR_UPDATE);
				mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
				
				selectedItem = partitionNumber;
				selectedItemStatus = TYPES_SYSTEM_STATUS.Disarm;
				
				if(dialog != null)
				{
					startLoader();
				}
				
				if(selectedItem == NOT_SELECTED || partitionNumber == WHOLE_SYSTEM)
				{
					int partNum = Integer.parseInt(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getPartition(0).getId());
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partNum, selectedItemStatus);
				}
				else
				{
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partitionNumber, selectedItemStatus);
				}
				
				
			}
		});
		disarmBtn.setVisibility(View.GONE);
		
		
		full = (FrameLayout) view.findViewById(R.id.full);
		fullBtn=(ImageButton) view.findViewById(R.id.fullBtn);
		fullBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//full
				mUpdateHandler.removeMessages(REGULAR_UPDATE);
				mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
				
				selectedItem = partitionNumber;
				selectedItemStatus = TYPES_SYSTEM_STATUS.Full;
				
				if(dialog != null)
				{
					startLoader();
				}
				
				if(selectedItem == NOT_SELECTED || partitionNumber == WHOLE_SYSTEM)
				{
					int partNum = Integer.parseInt(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getPartition(0).getId());
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partNum, selectedItemStatus);
				}
				else
				{
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partitionNumber, selectedItemStatus);
				}
			
				
			}
		});
		
		home1 = (FrameLayout) view.findViewById(R.id.home1);
		home1Btn=(ImageButton) view.findViewById(R.id.home1Btn);
		home2 = (FrameLayout) view.findViewById(R.id.home2);
		home2Btn=(ImageButton) view.findViewById(R.id.home2Btn);
		home3 = (FrameLayout) view.findViewById(R.id.home3);
		home3Btn=(ImageButton) view.findViewById(R.id.home3Btn);
		home4 = (FrameLayout) view.findViewById(R.id.home4);
		home4Btn=(ImageButton) view.findViewById(R.id.home4Btn);
		
		home1Btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Home1
				mUpdateHandler.removeMessages(REGULAR_UPDATE);
				mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
				
				selectedItem = partitionNumber;
				selectedItemStatus = TYPES_SYSTEM_STATUS.Home1;
				
				if(dialog != null)
				{
					startLoader();
				}
				
				if(selectedItem == NOT_SELECTED || partitionNumber == WHOLE_SYSTEM)
				{
					int partNum = Integer.parseInt(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getPartition(0).getId());
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partNum, selectedItemStatus);
				}
				else
				{
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partitionNumber, selectedItemStatus);
				}
			
			}
		});
		

		home2Btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Home2
				mUpdateHandler.removeMessages(REGULAR_UPDATE);
				mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
				
				selectedItem = partitionNumber;
				selectedItemStatus = TYPES_SYSTEM_STATUS.Home2;
				
				if(dialog != null)
				{
					startLoader();
				}
				
				if(selectedItem == NOT_SELECTED || partitionNumber == WHOLE_SYSTEM)
				{
					int partNum = Integer.parseInt(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getPartition(0).getId());
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partNum, selectedItemStatus);
				}
				else
				{
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partitionNumber, selectedItemStatus);
				}
			
			}
		});
	
		

		home3Btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Home3
				mUpdateHandler.removeMessages(REGULAR_UPDATE);
				mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
				
				selectedItem = partitionNumber;
				selectedItemStatus = TYPES_SYSTEM_STATUS.Home3;
				
				if(dialog != null)
				{
					startLoader();
				}
				
				if(selectedItem == NOT_SELECTED || partitionNumber == WHOLE_SYSTEM)
				{
					int partNum = Integer.parseInt(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getPartition(0).getId());
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partNum, selectedItemStatus);
				}
				else
				{
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partitionNumber, selectedItemStatus);
				}
				
			}
		});
	
		home4Btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Home4
				mUpdateHandler.removeMessages(REGULAR_UPDATE);
				mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
				
				selectedItem = partitionNumber;
				selectedItemStatus = TYPES_SYSTEM_STATUS.Home4;
				
				if(dialog != null)
				{
					startLoader();
				}
				
				if(selectedItem == NOT_SELECTED || partitionNumber == WHOLE_SYSTEM)
				{ 
					int partNum = Integer.parseInt(((SpaApplication)getActivity().getApplication()).getConnectedSystem().getPartition(0).getId());
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partNum, selectedItemStatus);
				}
				else
				{
					((MainScreenActivity)getActivity()).setPartitionStatus("" + partitionNumber, selectedItemStatus);
				}
				
			}
		});
	
		lockImageView=(ImageView)view.findViewById(R.id.lockImgV);
		
        return view;
	}
	
	@Override
	public void onResume() {
		
		armingOptionsLayout.setVisibility(View.INVISIBLE);
		fullBtn.setVisibility(View.INVISIBLE);
		home1Btn.setVisibility(View.INVISIBLE);
		home2Btn.setVisibility(View.INVISIBLE);
		home3Btn.setVisibility(View.INVISIBLE);
		home4Btn.setVisibility(View.INVISIBLE);
//		armingOptions.setVisibility(View.INVISIBLE);
		if(armingOptionsAddition != null) {
			armingOptionsAddition.setVisibility(View.INVISIBLE);
		}		
		disarmBtn.setVisibility(View.GONE);
//		enableRemoteBtn.setVisibility(View.GONE);
		lockImageView.setVisibility(View.INVISIBLE);
		currentStateText.setVisibility(View.INVISIBLE);
//		if(currentStateTextAddition != null) {
//			currentStateTextAddition.setVisibility(View.INVISIBLE);
//		}
		super.onResume();
		
	}

	/**
	 * 
	 * @param numOfHomes
	 */ 
	private void showRelevanIconsAccordingToNumOfHomes(int numOfHomes) {
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		width = (int)Math.round((width*0.8)/(numOfHomes+1));

		LayoutParams params = full.getLayoutParams();
		params.width = width;
		full.setLayoutParams(params);
		
		params = home1.getLayoutParams();
		params.width = width;
		home1.setLayoutParams(params);
		
		params = home2.getLayoutParams();
		params.width = width;
		home2.setLayoutParams(params);
		
		params = home3.getLayoutParams();
		params.width = width;
		home3.setLayoutParams(params);
		
		params = home4.getLayoutParams();
		params.width = width;
		home4.setLayoutParams(params);
		
//		DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();

//        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
//        System.out.println("dpWidth=" + dpWidth);
//		System.out.println("showRelevanIconsAccordingToNumOfHomes " + numOfHomes);
		
		boolean rtl = false;
		if(	((SpaApplication)getActivity().getApplication()).getLocalization() != null &&
			((SpaApplication)getActivity().getApplication()).getLocalization().equals("he")) {
			rtl = true;
		}else {
			rtl = false;
		}
		
		if(numOfHomes == 1)
		{
			home1.setVisibility(View.VISIBLE);
			home2.setVisibility(View.GONE);
			home3.setVisibility(View.GONE);
			home4.setVisibility(View.GONE);
		}
		else if(numOfHomes == 2)
		{
			home1.setVisibility(View.VISIBLE);
			home2.setVisibility(View.VISIBLE);
			if(rtl)
			{
				home2.setBackgroundResource(R.drawable.arm_option_background_left);
			}
			else
			{
				home2.setBackgroundResource(R.drawable.arm_option_background_right);
			}
			home3.setVisibility(View.GONE);
			home4.setVisibility(View.GONE);
		}
		else if(numOfHomes == 3)
		{
			home1.setVisibility(View.VISIBLE);
			home2.setVisibility(View.VISIBLE);
			home3.setVisibility(View.VISIBLE);
			if(rtl)
			{
				home3.setBackgroundResource(R.drawable.arm_option_background_left);
			}
			else
			{
				home3.setBackgroundResource(R.drawable.arm_option_background_right);
			}
			home4.setVisibility(View.GONE);
		}
		else if(numOfHomes == 4)
		{
			home1.setVisibility(View.VISIBLE);
			home2.setVisibility(View.VISIBLE);
			home3.setVisibility(View.VISIBLE);
			home4.setVisibility(View.VISIBLE);
			if(rtl)
			{
				home4.setBackgroundResource(R.drawable.arm_option_background_left);
			}
			else
			{
				home4.setBackgroundResource(R.drawable.arm_option_background_right);
			}
		}
		
	}

	/**
	 * 
	 */
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View menu = vi.inflate(R.layout.aa_zones_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button log = (Button)menu.findViewById(R.id.menu_log);
		log.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		log.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_log);
			}
		});
		
		Button faults = (Button)menu.findViewById(R.id.menu_faults);
		faults.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Faults"));
		faults.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_faults);
			}
		});
		
		Button lookin = (Button)menu.findViewById(R.id.menu_lookin);
		lookin.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		lookin.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_lookin);
			}
		});
		
		if(	((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration() != null &&
			((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().isLookIn())
		{
			lookin.setVisibility(View.VISIBLE);
		}
		else
		{
			lookin.setVisibility(View.GONE);
		}
		
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
//		Button refresh = (Button)menu.findViewById(R.id.menu_refresh_zones_names);
		Button bypass = (Button)menu.findViewById(R.id.menu_bypass_zones);
//			refresh.setVisibility(View.VISIBLE);
//			refresh.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_refresh_zones_names"));
//			refresh.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((MainScreenActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_refresh_zones_names);
//				}
//			});
		bypass.setVisibility(View.VISIBLE);
		bypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		bypass.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_bypass_zones);
			}
		});
		
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_logout:
			((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
			((BaseActivity)getActivity()).disconnect();
			break;
			
		case R.id.menu_bypass_zones:
			((MainScreenActivity)getActivity()).loadBypassZonesFragment();
			break;
			
		case R.id.menu_log:
			((MainScreenActivity)getActivity()).selectLogs();
			break;
			
		case R.id.menu_faults:
			((MainScreenActivity)getActivity()).selectFaults();
			break;
			
		case R.id.menu_lookin:
			((MainScreenActivity)getActivity()).selectLookin();
			break;
		}
	}
	
	
	
	/**
	 *  
	 */
	private void createScreen()
	{
		PanelSystem system = ((SpaApplication)getActivity().getApplication()).getConnectedSystem();
		TYPES_SYSTEM_STATUS status = system.getSystemStatusType();


		String systemStatus = getSystemStatusText();
		if(null != systemStatus)
		{
			currentStatus.setText(systemStatus);
		}
		else
		{
			currentStatus.setText("Error");
		}
	
		//in case it's disarm status
		if(status.compareTo(TYPES_SYSTEM_STATUS.Disarm)==0){
			armingOptionsLayout.setVisibility(View.VISIBLE);
			fullBtn.setVisibility(View.VISIBLE);
			home1Btn.setVisibility(View.VISIBLE);
			home2Btn.setVisibility(View.VISIBLE);
			home3Btn.setVisibility(View.VISIBLE);
			home4Btn.setVisibility(View.VISIBLE);
//			armingOptions.setVisibility(View.VISIBLE);
			if(armingOptionsAddition != null) {
				armingOptionsAddition.setVisibility(View.VISIBLE);
			}
			disarmBtn.setVisibility(View.GONE);
//			enableRemoteBtn.setVisibility(View.VISIBLE);
			lockImageView.setVisibility(View.VISIBLE);
			lockImageView.setImageResource(R.drawable.lock_open);
			currentStateText.setVisibility(View.VISIBLE);
//			if(currentStateTextAddition != null) {
//				currentStateTextAddition.setVisibility(View.VISIBLE);
//			}
		}else{
			armingOptionsLayout.setVisibility(View.INVISIBLE);
			fullBtn.setVisibility(View.INVISIBLE);
			home1Btn.setVisibility(View.INVISIBLE);
			home2Btn.setVisibility(View.INVISIBLE);
			home3Btn.setVisibility(View.INVISIBLE);
			home4Btn.setVisibility(View.INVISIBLE);
//			armingOptions.setVisibility(View.INVISIBLE);
			if(armingOptionsAddition != null) {
				armingOptionsAddition.setVisibility(View.INVISIBLE);
			}
			disarmBtn.setVisibility(View.VISIBLE);
//			enableRemoteBtn.setVisibility(View.GONE);
			lockImageView.setVisibility(View.VISIBLE);
			lockImageView.setImageResource(R.drawable.lock_closed);
			currentStateText.setVisibility(View.VISIBLE);
//			if(currentStateTextAddition != null) {
//				currentStateTextAddition.setVisibility(View.VISIBLE);
//			}
		}
		
		onPIMACreateOptionsMenu();
		
		int numOfHomes = ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().getNumberOfHomes();
		showRelevanIconsAccordingToNumOfHomes(numOfHomes);
	}
	

	/**
	 * 
	 * @return
	 */
	private String getSystemStatusText() {
		PanelSystem system = ((SpaApplication)getActivity().getApplication()).getConnectedSystem();
		
		return getSystemStatusText(system.getSystemStatusType());
		
	}
	
	
	private String getSystemStatusText(TYPES_SYSTEM_STATUS pStatus) {
		
		PanelSystem system = ((SpaApplication)getActivity().getApplication()).getConnectedSystem();
		SparseArray<String> textKeys = system.getConfiguration().getSystemStatusTextKeys();
		String key = textKeys.get(pStatus.ordinal());
		String status = "";
		try
		{
			status = ((SpaApplication)getActivity().getApplication()).getLocalizedString(key);
		}
		catch(Exception e){}
		return status;
	}
	
	/**
	 * 
	 * @param partitionNumber
	 * @param partitionName
	 */
	public void setPartition(int partitionNumber, String partitionName){
		this.partitionNumber = partitionNumber;
		this.partitionName = partitionName;
	}
	
	
	/**
	 * 
	 */
	@Override
	public void onStop() {
//		refreshZonesName = -1;
		super.onStop();
	}
	
	@Override
	protected void executeUpdate(boolean isBackground) {
		((MainScreenActivity)getActivity()).getSystemStatus(isBackground);
	}

	@Override
	protected void refreshList() {
		if(isDisconnected)
		{
			return;
		}
		
		if( null == panelSystem || null == panelSystem.getSystemStatusType()	)
		{
			mUpdateHandler.sendEmptyMessageDelayed(REGULAR_UPDATE, 200);	
		}
		else
		{
			createScreen();
		}
	}

	@Override
	public void onPause() {
		panelSystem = null;
		super.onPause();
	}
	
	public void setPanelSystem(PanelSystem panelSystem)
	{
		this.panelSystem = panelSystem;
	}
}
