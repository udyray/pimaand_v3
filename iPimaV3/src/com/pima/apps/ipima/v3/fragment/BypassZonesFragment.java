package com.pima.apps.ipima.v3.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.util.BypassZonesArrayAdapter;
import com.pima.apps.ipima.v3.util.BypassZonesArrayAdapter.ZonesRow;
import com.pima.apps.ipima.v3.views.SystemZone;


public class BypassZonesFragment extends RefreshableFragment{
	BypassZonesArrayAdapter mAdapter;
	
	private ListView mListView;
	
	boolean isReloadPending;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mAdapter = null;

		((SpaApplication)getActivity().getApplication()).isBypassZonesFragment = true;
		onPIMACreateOptionsMenu();
		
		View view = inflater.inflate(R.layout.fragment_bypass_zones, container, false);
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		}
		
		TextView textView = (TextView)view.findViewById(R.id.textView);
		if(textView != null) {
			textView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("updating_zones_names"));
		}
		
		mListView = (ListView) view.findViewById(R.id.bypasszonesview);
		mListView.setBackgroundColor(getResources().getColor(R.color.clear));
		mListView.setCacheColorHint(0);

		return view;
	}
	
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		if(	null != ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getAllSystemZones())
		{
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					refreshList();
				}
			});
			return;
		}
	}



	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View menu = vi.inflate(R.layout.aa_zones_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button log = (Button)menu.findViewById(R.id.menu_log);
		log.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		log.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_log);
			}
		});
		
		Button faults = (Button)menu.findViewById(R.id.menu_faults);
		faults.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Faults"));
		faults.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_faults);
			}
		});
		
		Button lookin = (Button)menu.findViewById(R.id.menu_lookin);
		lookin.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		lookin.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_lookin);
			}
		});
		
		if(	((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration() != null &&
			((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().isLookIn())
		{
			lookin.setVisibility(View.VISIBLE);
		}
		else
		{
			lookin.setVisibility(View.GONE);
		}
		
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
//		Button refresh = (Button)menu.findViewById(R.id.menu_refresh_zones_names);
		Button bypass = (Button)menu.findViewById(R.id.menu_bypass_zones);
//			refresh.setVisibility(View.VISIBLE);
//			refresh.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_refresh_zones_names"));
//			refresh.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((MainScreenActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_refresh_zones_names);
//				}
//			});
		bypass.setVisibility(View.VISIBLE);
		bypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		bypass.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_bypass_zones);
			}
		});
		
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_logout:
			((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
			((BaseActivity)getActivity()).disconnect();
			break;
			
		case R.id.menu_bypass_zones:
			((MainScreenActivity)getActivity()).loadBypassZonesFragment();
			break;
			
		case R.id.menu_log:
			((MainScreenActivity)getActivity()).selectLogs();
			break;
			
		case R.id.menu_faults:
			((MainScreenActivity)getActivity()).selectFaults();
			break;
			
		case R.id.menu_lookin:
			((MainScreenActivity)getActivity()).selectLookin();
			break;
		}
	}

	
	
	
	@Override
	protected void refreshList() {
		if(isDisconnected)
		{
			return;
		}
		
		if(dialog != null)
		{
			dismissLoader();
		}
		
		SystemZone[] zoneNames = null;
		ZonesRow row = null;
		ArrayList<ZonesRow> list = new ArrayList<ZonesRow>();
		if(null != ((SpaApplication)getActivity().getApplication()).getConnectedSystem())
		{
			zoneNames = ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getAllSystemZones();
		}
		
		int numOfZones = zoneNames.length;
		for(int i=0; i<numOfZones; i++)
		{
			SystemZone zn = zoneNames[i];
			if(null != zn)
			{
				boolean isBypassed = zn.getIsBypass();
				
				row = new ZonesRow(zn.getNumber(), zn.getName(), isBypassed);
				list.add(row);
			}
			
		}
		
		if(null == mAdapter)
		{
			mAdapter = new BypassZonesArrayAdapter(this.getActivity(), list);
			mListView.setAdapter(mAdapter);
		}
		else
		{
			mAdapter.updateData(list);
			getActivity().runOnUiThread(new Runnable() {
			    public void run() {
			    	mAdapter.notifyDataSetChanged();
			    }
			});
		}
	}


	@Override
	public void onPause() {
		mAdapter = null;
		super.onPause();
	}

	@Override
	protected void executeUpdate(boolean isBackground) {
		((MainScreenActivity)getActivity()).getSystemStatus(isBackground);
	}
}
