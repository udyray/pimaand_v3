package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pima.apps.ipima.v3.PIMACommonSettings;
import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.PartitionsNamesEditActivity;
import com.pima.apps.ipima.v3.activity.SystemEditActivity;
import com.pima.apps.ipima.v3.views.SystemPartition;


public class PartitionsNamesFragment extends BaseFragment{
	
	String mSystemId;
	public Button doneItem;
	
	SparseArray<TextView> partitionsNums = null;
	SparseArray<EditText> partitionsNames = null;
	
	SparseArray<SystemPartition> partitionsSavedNames = null;
	
	public static PartitionsNamesFragment newInstance(Bundle args) {
		PartitionsNamesFragment f = new PartitionsNamesFragment();
		f.setArguments(args);
		return f;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.fragment_partitions_names, container, false);
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mSystemId = getArguments() != null ? getArguments().getString(SystemEditActivity.KEY_SYSTEM_EDIT) : null;
		
		partitionsNums = new SparseArray<TextView>();
		partitionsNames = new SparseArray<EditText>();
		
//		OnClickListener ocl = new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				
//				new Thread(new Runnable() {
//					
//					@Override
//					public void run() {
//						startLoader();
//						savePartitionsNames();
//						((PartitionsNamesEditActivity)getActivity()).finish();
//						dismissLoader();
//					}
//				}).start();
//				
//			}
//		};
//		doneItem.setOnClickListener(ocl);
		
		
//		OnClickListener cancel = new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				((PartitionsNamesEditActivity)getActivity()).finish();
//			}
//		};
		
		
		setMaps();
		
		onPIMACreateOptionsMenu();
       
	}
	
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = vi.inflate(R.layout.aa_fragment_partitions_names_edit, null);

		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((PartitionsNamesEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
		((PartitionsNamesEditActivity)getActivity()).replaceMoreView(menu);
		
		doneItem.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				((PartitionsNamesEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_done);
			}
		});
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((PartitionsNamesEditActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
			case R.id.menu_done:
				startLoader();
				savePartitionsNames();
				((PartitionsNamesEditActivity)getActivity()).finish();
				dismissLoader();
				break;
				
			case R.id.menu_discard:
				getActivity().finish();
				break;
		
		}
	}
	
	/**
	 * 
	 */
	private void setMaps() {
		TextView num1 = (TextView)getActivity().findViewById(R.id.partitionNumber_1);
		partitionsNums.put(1, num1);
		EditText name1 = (EditText)getActivity().findViewById(R.id.partitionName_1);
		partitionsNames.put(1, name1);
		
		TextView num2 = (TextView)getActivity().findViewById(R.id.partitionNumber_2);
		partitionsNums.put(2, num2);
		EditText name2 = (EditText)getActivity().findViewById(R.id.partitionName_2);
		partitionsNames.put(2, name2);
		
		TextView num3 = (TextView)getActivity().findViewById(R.id.partitionNumber_3);
		partitionsNums.put(3, num3);
		EditText name3 = (EditText)getActivity().findViewById(R.id.partitionName_3);
		partitionsNames.put(3, name3);
		
		TextView num4 = (TextView)getActivity().findViewById(R.id.partitionNumber_4);
		partitionsNums.put(4, num4);
		EditText name4 = (EditText)getActivity().findViewById(R.id.partitionName_4);
		partitionsNames.put(4, name4);
		
		TextView num5 = (TextView)getActivity().findViewById(R.id.partitionNumber_5);
		partitionsNums.put(5, num5);
		EditText name5 = (EditText)getActivity().findViewById(R.id.partitionName_5);
		partitionsNames.put(5, name5);
		
		TextView num6 = (TextView)getActivity().findViewById(R.id.partitionNumber_6);
		partitionsNums.put(6, num6);
		EditText name6 = (EditText)getActivity().findViewById(R.id.partitionName_6);
		partitionsNames.put(6, name6);
		
		TextView num7 = (TextView)getActivity().findViewById(R.id.partitionNumber_7);
		partitionsNums.put(7, num7);
		EditText name7 = (EditText)getActivity().findViewById(R.id.partitionName_7);
		partitionsNames.put(7, name7);
		
		TextView num8 = (TextView)getActivity().findViewById(R.id.partitionNumber_8);
		partitionsNums.put(8, num8);
		EditText name8 = (EditText)getActivity().findViewById(R.id.partitionName_8);
		partitionsNames.put(8, name8);
		
		TextView num9 = (TextView)getActivity().findViewById(R.id.partitionNumber_9);
		partitionsNums.put(9, num9);
		EditText name9 = (EditText)getActivity().findViewById(R.id.partitionName_9);
		partitionsNames.put(9, name9);
		
		TextView num10 = (TextView)getActivity().findViewById(R.id.partitionNumber_10);
		partitionsNums.put(10, num10);
		EditText name10 = (EditText)getActivity().findViewById(R.id.partitionName_10);
		partitionsNames.put(10, name10);
		
		TextView num11 = (TextView)getActivity().findViewById(R.id.partitionNumber_11);
		partitionsNums.put(11, num11);
		EditText name11 = (EditText)getActivity().findViewById(R.id.partitionName_11);
		partitionsNames.put(11, name11);
		
		TextView num12 = (TextView)getActivity().findViewById(R.id.partitionNumber_12);
		partitionsNums.put(12, num12);
		EditText name12 = (EditText)getActivity().findViewById(R.id.partitionName_12);
		partitionsNames.put(12, name12);
		
		TextView num13 = (TextView)getActivity().findViewById(R.id.partitionNumber_13);
		partitionsNums.put(13, num13);
		EditText name13 = (EditText)getActivity().findViewById(R.id.partitionName_13);
		partitionsNames.put(13, name13);
		
		TextView num14 = (TextView)getActivity().findViewById(R.id.partitionNumber_14);
		partitionsNums.put(14, num14);
		EditText name14 = (EditText)getActivity().findViewById(R.id.partitionName_14);
		partitionsNames.put(14, name14);
		
		TextView num15 = (TextView)getActivity().findViewById(R.id.partitionNumber_15);
		partitionsNums.put(15, num15);
		EditText name15 = (EditText)getActivity().findViewById(R.id.partitionName_15);
		partitionsNames.put(15, name15);
		
		TextView num16 = (TextView)getActivity().findViewById(R.id.partitionNumber_16);
		partitionsNums.put(16, num16);
		EditText name16 = (EditText)getActivity().findViewById(R.id.partitionName_16);
		partitionsNames.put(16, name16);
		
	}

	@Override
	public void onResume() {
		super.onResume();
		
		String partition = ((SpaApplication)(getActivity()).getApplication()).getLocalizedString("label_partition");
		String part = ((SpaApplication)(getActivity()).getApplication()).getLocalizedString("label_partition_short_m");
		
		for(int i=1; i<=PIMACommonSettings.MAX_NUM_OF_PARTITIONS; i++)
		{
			TextView num = partitionsNums.get(i);
			num.setText(part + " " + i + ":"); 
			
			EditText name = partitionsNames.get(i);
			
			String savedName = null;
			if(null != partitionsSavedNames.get(i))
			{
				savedName = partitionsSavedNames.get(i).getName();
			}
			
			if(null != savedName)
			{
				name.setText(savedName);
			}
			else
			{
				name.setText(partition + " " + i);
			}
			
		}
		
	}



	/**
	 * 
	 */
	private void savePartitionsNames()
	{
		//UPDATE TO SERVER????
	}
}
