package com.pima.apps.ipima.v3.fragment;


import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.SystemEditActivity;
import com.pima.apps.ipima.v3.util.Utils;

public class SystemEditFragment extends BaseFragment implements OnClickListener //,LoaderCallbacks<SystemModel>
{
	
	private EditText mEtName, mEtPair;
	
	private RelativeLayout outputsExpand;
	private RelativeLayout contactsExpand;
	private RelativeLayout notificationsExpand;
	private String mSystemName = null;
	private String mSystemId = null;
	
	public Button doneItem;
	private TextWatcher textWatcher = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			if(mSystemMode == SYSTEM_MODE.ADD)
			{
				String name = TextUtils.isEmpty(mEtName.getText()) ? mEtName.getHint().toString().trim() : mEtName.getText().toString().trim();
				if(validateSystem(name, mEtPair.getText().toString())) {
					if(doneItem != null) {
						doneItem.setEnabled(true);
					}
				}else {
					if(doneItem != null) {
						doneItem.setEnabled(false);
					}
				}
			}
			else if(mSystemMode == SYSTEM_MODE.EDIT)
			{
				if(validateSystem(mEtName.getText().toString(), mEtPair.getText().toString())) 
				{
					if(!mEtName.getText().toString().equals(mSystemName))
					{
						if(doneItem != null) {
							doneItem.setEnabled(true);
						}
					}
					else 
					{
						if(doneItem != null) {
							doneItem.setEnabled(false);
						}
					}
					
				}
				else {
					if(doneItem != null) {
						doneItem.setEnabled(false);
					}
				}
			}
			
		}
	};
	
	private static enum SYSTEM_MODE {
		ADD,
		EDIT,
		NONE
	}
	private SYSTEM_MODE mSystemMode;
	
	public static SystemEditFragment newInstance(Bundle args) {
		SystemEditFragment f = new SystemEditFragment();
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.fragment_system_edit, container, false);
	}
	
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mSystemId = getArguments() != null ? getArguments().getString(SystemEditActivity.KEY_SYSTEM_EDIT) : null;
		mEtPair = (EditText)getActivity().findViewById(R.id.et_pair);
		
		mEtName = (EditText)getActivity().findViewById(R.id.et_name);
		mEtName.addTextChangedListener(textWatcher);
		
		TextView etNameTitle = (TextView)getActivity().findViewById(R.id.et_name_title);
		etNameTitle.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("SystemName"));
				
		outputsExpand = (RelativeLayout)getActivity().findViewById(R.id.OutputsExpand);
		outputsExpand.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) { 
				((SystemEditActivity)getActivity()).swapToOutputs();
			}
		});
		
		contactsExpand = (RelativeLayout)getActivity().findViewById(R.id.ContactsExpand);
		//check master code
		if(	((SpaApplication)getActivity().getApplication()).masterCodeInd != null &&
			((SpaApplication)getActivity().getApplication()).masterCodeInd.equals("1"))
		{
			contactsExpand.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((SystemEditActivity)getActivity()).swapToContacts();
				}
			});
			
			contactsExpand.setVisibility(View.VISIBLE);
		}
		else
		{
			contactsExpand.setVisibility(View.INVISIBLE);
		}
		
		notificationsExpand = (RelativeLayout)getActivity().findViewById(R.id.NotificationsExpand);
		notificationsExpand.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SystemEditActivity)getActivity()).swapToNotifications();
			}
		});

		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float logicalDensity = (metrics.ydpi / 160f);
		
		int screen_height = getResources().getDisplayMetrics().heightPixels;
		double screen_height_dp = ((double) screen_height) / logicalDensity;
		
		Button enableRemoteBtn = (Button) getActivity().findViewById(R.id.enableRemoteBtn);
		if(screen_height_dp <= 480) { //fixed 13dp margin
			
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)enableRemoteBtn.getLayoutParams();
			lp.bottomMargin = (int)(13*logicalDensity);
		}else if(screen_height_dp <= 511) { //floating 14dp-44dp margin
			
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)enableRemoteBtn.getLayoutParams();
			lp.bottomMargin = (int)((screen_height_dp - 467)*logicalDensity);
		}//in the xml it is 45dp
		enableRemoteBtn.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("EnableRemoteAccess"));
		enableRemoteBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Disarm
				startLoader();
				
				((SystemEditActivity)getActivity()).enableRemoteAccess();
			}
		});
		
		TextView outputs = (TextView)getActivity().findViewById(R.id.Outputs);
		outputs.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs"));
		
		TextView contacts = (TextView)getActivity().findViewById(R.id.Contacts);
		contacts.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Contacts"));
		
		TextView notifications = (TextView)getActivity().findViewById(R.id.Notifications);
		notifications.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Notifications"));
		
		if(mSystemId == null)//add
		{
			mSystemMode = SYSTEM_MODE.ADD;
			
			((SystemEditActivity)getActivity()).setTitle("Pairing");
			
			mEtPair.addTextChangedListener(textWatcher);
			
			TextView etPairTitle = (TextView)getActivity().findViewById(R.id.et_pair_title);
			etPairTitle.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("PairingCode"));
			
			mEtPair.setVisibility(View.VISIBLE);
			
			outputsExpand.setVisibility(View.GONE);
			notificationsExpand.setVisibility(View.GONE);
			contactsExpand.setVisibility(View.GONE);
			
			enableRemoteBtn.setVisibility(View.GONE);
			
			mEtName.setHint(((SpaApplication)getActivity().getApplication()).getLocalizedString("ALARM_SYSTEM_DEFAULT"));
			
			
			getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("Add"));
			
		}
		else//update
		{
			mSystemMode = SYSTEM_MODE.EDIT;
			
			((SystemEditActivity)getActivity()).setTitle("Edit");
			mSystemName = getArguments() != null ? getArguments().getString(SystemEditActivity.KEY_SYSTEM_NAME) : null;
			
			mEtName.setText(mSystemName);
			
			mEtPair.setVisibility(View.GONE);
			
			outputsExpand.setVisibility(View.VISIBLE);
			notificationsExpand.setVisibility(View.VISIBLE);
			if(	((SpaApplication)getActivity().getApplication()).masterCodeInd != null &&
				((SpaApplication)getActivity().getApplication()).masterCodeInd.equals("1"))
			{
				contactsExpand.setVisibility(View.VISIBLE);
			}
			
			enableRemoteBtn.setVisibility(View.VISIBLE);
			
			getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("Edit"));
		}
		
	
		
		onPIMACreateOptionsMenu();

	}
	
	
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = null;

		if(doneItem != null) {
			doneItem.setEnabled(false);
		}
		
		if(mSystemMode == SYSTEM_MODE.ADD) {
			
			menu = vi.inflate(R.layout.aa_fragment_systems_add, null);
		}else {
			menu = vi.inflate(R.layout.aa_fragment_systems_edit, null);
		}
		
		
//		Button mo = (Button)menu.findViewById(R.id.menu_manage_outputs);
//		if(mo != null) {
//			mo.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("AlarmSystem_Add_ManageOutput"));
//			mo.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((SystemEditActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_manage_outputs);
//				}
//			});
//		}
		
//		Button pn = (Button)menu.findViewById(R.id.menu_partitions_names);
//		if(pn != null) {
//			pn.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_partitions_names"));
//			pn.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((SystemEditActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_partitions_names);
//				}
//			});
//		}


		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SystemEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
//		Button menu_delete = (Button)menu.findViewById(R.id.menu_delete);
//		if(menu_delete != null) {
//			menu_delete.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Delete"));
//			menu_delete.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((SystemEditActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_delete);
//				}
//			});
//
//		}
		
		((SystemEditActivity)getActivity()).replaceMoreView(menu);
		
		doneItem.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mEtPair.getWindowToken(), 0);
				imm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
				
				((SystemEditActivity)getActivity()).dismissMoreView();
				
				onPIMAOptionsItemSelected(R.id.menu_done);
			}
		});
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((SystemEditActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_done:
			switch (mSystemMode) {
				case EDIT:
					processUpdate();
					break;
					
				case ADD:
					processInsert();
					break;
	
				default:
					break;
				}
			
			break;
//		case R.id.menu_manage_outputs:
//			Intent intent = new Intent(getActivity(),OutputsActivity.class);
//			intent.putExtra(OutputsListFragment.KEY_SYSTEM_ID, mSystemId);
//			startActivity(intent);
//			break;
			
//		case R.id.menu_partitions_names:
//			Intent pIntent = new Intent(getActivity(), PartitionsNamesEditActivity.class);
//			pIntent.putExtra(SystemEditActivity.KEY_SYSTEM_EDIT, mSystemId);
//			pIntent.putExtra(SystemEditActivity.KEY_SYSTEM_NAME, mEtName.getText().toString());
//			startActivity(pIntent);
//			break;
			
		case R.id.menu_discard:
			switch (mSystemMode) {
				case EDIT:
					((SystemEditActivity)getActivity()).disconnect();
					break;
				case ADD:
					getActivity().finish();
					break;
	
				default:
					break;
			}
			break;
			
//		case R.id.menu_delete:
//			processDelete();
//			getActivity().finish();
//			break;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View view) {
		
		switch (view.getId())
		{
			case R.id.btn_ok:
				switch (mSystemMode) {
				case EDIT:
					processUpdate();
					break;
				case ADD:
					processInsert();
					break;
	
				default:
					break;
				}
				
				break;
				
			case R.id.btn_cancel:
				getActivity().finish();
				break;
	
			default:
				break;
		}
	}
	
	private Boolean validateSystem(String title, String pair) {
		if(title == null || title.length() == 0) {
			return false;
		}
		if(mSystemMode == SYSTEM_MODE.ADD || mSystemMode == SYSTEM_MODE.NONE)
		{
			if(pair == null || pair.length() == 0) {
				return false;
			}
		}
		return true;
	}
	
	private void processUpdate() {
		String name;
		
		name = mEtName.getText().toString().trim();
		
		if (Utils.isEmpty(name)) {
			return;
		}
		
		startLoader();
		
		((SystemEditActivity)getActivity()).updateSystemName(name);
		
	}
	
//	private void processDelete() {
//		
//		//delete from outputs
//
//		//delete from partition names
//		
//		//delete from zone names
//
//	}
	
	private void processInsert() {
		String name, pair;
		name = TextUtils.isEmpty(mEtName.getText()) ? mEtName.getHint().toString().trim() : mEtName.getText().toString().trim();
//		name = mEtName.getText().toString().trim();
		pair = mEtPair.getText().toString().trim();
		
		if (Utils.isEmpty(name) || Utils.isEmpty(pair)) {
			return;
		}
		
		startLoader();
		
		((SystemEditActivity)getActivity()).addSystem(name, pair);
	}

}
