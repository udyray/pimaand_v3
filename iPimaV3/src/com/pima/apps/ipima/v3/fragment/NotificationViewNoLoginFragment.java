package com.pima.apps.ipima.v3.fragment;

import java.io.InputStream;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;


public class NotificationViewNoLoginFragment extends BaseFragment{
	
	private long mInterPicturesTime;
	private long mInterClipTime;


	public static String EXTRA_NOTIFICATION_TITLE = "EXTRA_NOTIFICATION_TITLE";
	public static String EXTRA_NOTIFICATION_IMAGES = "EXTRA_NOTIFICATION_IMAGES";
	
	private static int mImageIndex;
	
	private String	 mTitle;
	private String[] mImagesUrl;
	private Bitmap[] mImages;
	
	private ImageView mImage;
	
	private boolean mIsPlaying;
	private ImageView mPlayBtn;
	private ImageView mPauseBtn;
	
	
	final Handler mUpdateHandler = new Handler(Looper.getMainLooper()){
    	
        @Override
        public void handleMessage(Message inputMessage) {
        	if(isHidden() || !isVisible())//stop 
        	{
        		return;
        	}

        	if(((SpaApplication)getActivity().getApplication()).getConnectedSystem() != null) {
        	

    			getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						loadNextImage();
					}
				});
    		}
        	else {
				dismissLoader();
			}
        }
    };
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		if(null != getArguments())
		{
			mImageIndex = 0;
			mTitle = getArguments().getString(EXTRA_NOTIFICATION_TITLE);
			mImagesUrl = getArguments().getStringArray(EXTRA_NOTIFICATION_IMAGES);
			
			mImages = new Bitmap[mImagesUrl.length];
		}
		
		mInterPicturesTime = 800;
		mInterClipTime = 2000;
		
		mIsPlaying = false;
		mUpdateHandler.removeMessages(0);
	
//		createOverlay();
		
		View view = inflater.inflate(R.layout.fragment_view_notification, container, false);
		mImage = (ImageView) view.findViewById(R.id.imageView);
		
		mPlayBtn = (ImageView) view.findViewById(R.id.playBtn);
		mPauseBtn = (ImageView) view.findViewById(R.id.pauseBtn);
		
		
		mPlayBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				play();
			}
		});
		
		
		mPauseBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				pause();
			}

			
		});
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(mTitle);
		}
		
		return view;
	}

	private void play() {
		mIsPlaying = true;
		showPlayPause();
		loadNextImage();
	}
	
	
	private void pause() {
		mIsPlaying = false;
		mUpdateHandler.removeMessages(0);
		
		showPlayPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		
		mImageIndex = 0;
		new LoadImage().execute(mImagesUrl[mImageIndex]);
	}
	
		
//	public void createOverlay() {
//		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//		View v = vi.inflate(R.layout.loader_overlay, null);
//		((NewLoginActivity)getActivity()).replaceLoader(v);
//		
//	}

	
	private void hidePlayPause()
	{
		mPlayBtn.setVisibility(View.GONE);
		mPauseBtn.setVisibility(View.GONE);
	}
	
	private void showPlayPause()
	{
		if(mImagesUrl != null && mImagesUrl.length==1)
		{
			hidePlayPause();
		}
		else if(mIsPlaying)
		{
			mPlayBtn.setVisibility(View.GONE);
			mPauseBtn.setVisibility(View.VISIBLE);
		}
		else
		{
			mPlayBtn.setVisibility(View.VISIBLE);
			mPauseBtn.setVisibility(View.GONE);
		}
	}
	
	private void loadNextImage() {

		if(mImageIndex < mImagesUrl.length)
		{
			if(mImages[mImageIndex] == null)
			{
				new LoadImage().execute(mImagesUrl[mImageIndex]);
			}
			else
			{
				loadImage();
			}
		}
		else
		{
			mUpdateHandler.sendEmptyMessageDelayed(0, mInterClipTime);
			mImageIndex = 0;
		}
		
	
	}

	private class LoadImage extends AsyncTask<String, String, Bitmap>
	{
		@Override
		protected void onPreExecute() {
			if(!mIsPlaying)
			{
				hidePlayPause();
				if(dialog != null)
				{
					startLoader();
				}
			}
		}
		
		protected Bitmap doInBackground(String... args) 
		{
			Bitmap bitmap = null;
			try{
				bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}
		
		protected void onPostExecute(Bitmap image) 
		{
			if(image != null)
			{
				int newWidth = mImage.getBackground().getIntrinsicWidth();
				float scaleFactor = (float)newWidth/(float)image.getWidth();
				int newHeight = (int)(image.getHeight() * scaleFactor);
		//
				image = Bitmap.createScaledBitmap(image, newWidth, newHeight, true);
				
				mImages[mImageIndex] = image;
				loadImage();
			}
			else
			{
				System.out.println("Image Does Not exist or Network Error");
				mUpdateHandler.sendEmptyMessageDelayed(0, mInterPicturesTime);//try again
			}
			
			if(dialog != null)
			{
				dismissLoader();
			}
			showPlayPause();
		}
	}

	/**
	 * 
	 */
	private void loadImage()
	{
		Bitmap bitmap = mImages[mImageIndex];
		
		mImage.setImageBitmap(bitmap);
		
		
		if(mIsPlaying)
		{
			mImageIndex++;
			mUpdateHandler.sendEmptyMessageDelayed(0, mInterPicturesTime);
		}
	}

}


