package com.pima.apps.ipima.v3.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.SystemEditActivity;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.views.Contact;


public class ContactsListFragment extends BaseFragment {
	
	private ListView mListView;	
		
	public Button doneItem;
//	private View empty_view = null;
	
	public static final String LOG_TAG = "ContactsListFragment";
	
	private ContactsArrayAdapter mAdapter;
	private String mSystemId;
//	private SystemModel mSystemModel;
	
	
	private Contact[] contacts;
	
	public static ContactsListFragment newInstance(Bundle args) {
		ContactsListFragment fragment = new ContactsListFragment();
		fragment.setArguments(args);
		return fragment;
	}	
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		createScreen();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_contacts, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mSystemId = getArguments() != null ? getArguments().getString(SystemEditActivity.KEY_SYSTEM_EDIT) : null;
		if(mSystemId == null)
		{
			getActivity().finish();
			return;
		}
		
//		mSystemModel = getSystemById(mSystemId);
		if (((BaseActivity)getActivity()).mApplication.getConnectedSystem() == null) {
			getActivity().finish();
			return;
		}
		
		mListView=(ListView)getActivity().findViewById(R.id.contactsList);
		mListView.setCacheColorHint(0);
		
		((SystemEditActivity)getActivity()).setTitle("Contacts");
		
//		TextView titleView = (TextView)getActivity().findViewById(R.id.titleView);
//		titleView.setText(((BaseActivity)getActivity()).mApplication.getLocalizedString("Contacts"));
		
		registerForContextMenu(mListView);
		mListView.setBackgroundResource(R.drawable.background);
		
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new ContactsArrayAdapter(getActivity(), null);
		mListView.setAdapter(mAdapter);

		onPIMACreateOptionsMenu();
	}
	
	public void setContacts(Contact[] pContacts)
	{
		contacts = pContacts;
		fillData();
	}
	
	private void fillData() {	
		if(null != contacts)
		{
			if(null == mAdapter)
	 		{
				mAdapter = new ContactsArrayAdapter(getActivity(), contacts);
	 			mListView.setAdapter(mAdapter);
	 		}
	 		else
	 		{
	 			mAdapter.updateData(contacts);
	 			mAdapter.notifyDataSetChanged();
	 			mListView.setAdapter(mAdapter);
	 		}
		}
		
		if(dialog != null)
		{
			dismissLoader();
		}
	}

	
	/*** Options Menu ***/
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = vi.inflate(R.layout.aa_fragment_contacts_list, null);
		
		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SystemEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
		((SystemEditActivity)getActivity()).replaceMoreView(menu);
		
		doneItem.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				System.out.println("Contacts Done!");
			}
		});
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((SystemEditActivity)getActivity()).dismissMoreView();
				}
			});
		}
		
	}

	/**
	 * 
	 * @param menu_id
	 */
	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		
			case R.id.menu_discard:
				((SystemEditActivity)getActivity()).swapToEdit();
				break;
		
		}
	}
	
	
	/**
	 * 
	 */
	private void createScreen() {
	
		if(null != dialog)
		{
			startLoader();
		}
		
		((SystemEditActivity)getActivity()).getContacts();
	}
	
	
	
	/**
	 * 
	 * @author Udy
	 *
	 */
	public class ContactsArrayAdapter extends BaseAdapter
	{
		Contact[] mObjects;
		Context mContext;
		
		public ContactsArrayAdapter(Context pContext, Contact[] pObjects){
			super();
			mContext = pContext;
			mObjects = pObjects;
		}

		@Override
		public int getCount() {
			if(null != mObjects)
			{
				return mObjects.length;
			}
			return 0;
		}

		@Override
		public Object getItem(int position) {
			if(position < getCount())
			{
				return mObjects[position];
			}
			
			return null;
		}

		@Override
		public long getItemId(int position) {
			
			if(null != getItem(position))
			{
				return position;
			}
			return -1;
		}

		public void updateData(Contact[] pObjects)
		{
			mObjects = pObjects;
		}
		
		@SuppressLint("ResourceAsColor")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			LinearLayout rowView = (LinearLayout)convertView;
			final Contact row = (Contact)getItem(position);
			if(rowView == null)
			{
				rowView = (LinearLayout)mInflater.inflate(R.layout.contacts_list_item, null);
			}
			
			final Button deleteBtn = (Button)rowView.findViewById(R.id.doDeleteBtn);
	        deleteBtn.setVisibility(View.GONE);
	        deleteBtn.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Delete"));
	        deleteBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						((SystemEditActivity)getActivity()).deleteContact(row.getId());
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			});
	        
	        ImageView delete = (ImageView)rowView.findViewById(R.id.deleteContact);
	        delete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(deleteBtn.getVisibility() == View.VISIBLE)
					{
						deleteBtn.setVisibility(View.GONE);
					}
					else
					{
						deleteBtn.setVisibility(View.VISIBLE);
					}
					
				}
			});
	        
			TextView contactName = (TextView)rowView.findViewById(R.id.contactName);
			contactName.setText(row.getName());
			
			TextView contactPhone = (TextView)rowView.findViewById(R.id.contactPhone);
			contactPhone.setText(row.getPhone());
			
			return rowView;
		
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}
	}

}
