package com.pima.apps.ipima.v3.fragment;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

public class InfoDialogFragment extends DialogFragment {

	public interface InfoDialogCallbacks {
		public void onInfoOkClick(String tag);
	}

	private InfoDialogCallbacks mListener;
	private String mTag = "confirmDialog";
	
	private Boolean isOkClicked = false;

	/**
	 * 
	 * @param tag
	 * @param title
	 * @param text
	 * @return DialogFragment
	 */
	public static InfoDialogFragment newInstance(String tag, String title, String text) {
		InfoDialogFragment f = new InfoDialogFragment();
		Bundle args = new Bundle();
		args.putString("tag", tag);
		args.putString("title", title);
		args.putString("text", text);
		f.setArguments(args);

		return f;
	}
	
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (InfoDialogCallbacks) activity;
            context = activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement InfoDialogCallbacks");
        }
    }
	
	private Activity context;
	
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		
		if (getArguments().getString("tag") != null) {
			mTag = getArguments().getString("tag");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		String text = getArguments().getString("text");
		String tag = getArguments().getString("tag");
		
		String closeString = ((SpaApplication)context.getApplication()).getLocalizedString("OK");
		
//		if(tag != null && tag.equals(BaseActivity.TAG_DIALOG_WARNING)) {
//			closeString = ((SpaApplication)context.getApplication()).getLocalizedString("button_YES");
//		}
		
		Builder dialogBuilder = new AlertDialog.Builder(getActivity())
				.setPositiveButton(closeString, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						isOkClicked = true;
						if(mTag != null && mTag.equals(BaseActivity.TAG_DIALOG_INFO_WRONGPASSWORD)) {
							//If tag is wrongpassword onInfoOkClick is not called
							//and the system list activity is not redisplayed
							return;
						}
						
						try{
							mListener.onInfoOkClick(mTag);
						}
						catch(IllegalStateException ise){
							Log.e(mTag, "--ERROR CLOSING OK INFO--");
						}
					}
				});
		dialogBuilder.setCancelable(false);
		
		if(tag != null && tag.equals(BaseActivity.TAG_DIALOG_WARNING)) {
			dialogBuilder.setCancelable(true);
			String negativeButton = ((SpaApplication)context.getApplication()).getLocalizedString("Cancel");
			dialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			});
		}
		
		if (title != null) {
			
			LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = vi.inflate(R.layout.dialog_title, null);
			TextView tv = (TextView)v.findViewById(R.id.dialog_title_tv);
			
			tv.setText(title);
			
			//Warning dialog question mark
//			if(tag != null && tag.equals(BaseActivity.TAG_DIALOG_WARNING)) {
//				TextView tvExt = (TextView)v.findViewById(R.id.dialog_title_tv_ext);
//				tvExt.setText(((SpaApplication)context.getApplication()).getLocalizedString("question_extension"));
//			}
			
			dialogBuilder.setCustomTitle(v);

		}
		
		if (text != null) {
			if(tag != null && tag.equals(BaseActivity.TAG_DIALOG_HELP)) {
				WebView webview = new WebView(getActivity());
				webview.setBackgroundColor(getActivity().getResources().getColor(R.color.black));
				webview.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);
				dialogBuilder.setView(webview);
			}else {
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View v = vi.inflate(R.layout.dialog_message, null);
				TextView tv = (TextView)v.findViewById(R.id.dialog_title_tv);
				tv.setText(Html.fromHtml(text));

				dialogBuilder.setView(v);
			}
			
			//dialogBuilder.setMessage(Html.fromHtml(text));
		}
		
		AlertDialog alert_dialog = dialogBuilder.create();
		return alert_dialog;
	}


	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		if(mTag != null && mTag.equals(BaseActivity.TAG_DIALOG_WARNING)) {
			return;
		}
		if(mTag != null && mTag.equals(BaseActivity.TAG_DIALOG_INFO_WRONGPASSWORD)) {
			return;
		}
		if(!isOkClicked) {
			try
			{
				mListener.onInfoOkClick(mTag);
			}
			catch(IllegalStateException ise)
			{
				Log.e(mTag, "--ERROR CLOSING OK INFO");
			}
		}
	}
	
    
}
