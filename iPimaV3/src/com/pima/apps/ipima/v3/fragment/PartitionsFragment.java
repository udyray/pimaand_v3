package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.views.SystemPartition;


public class PartitionsFragment extends BaseFragment{
	
//	PartitionsArrayAdapter mAdapter;
	private ListView mListView;
	
	SparseArray<SystemPartition> partitionsNames = null;
	boolean isReloadPending;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		//partitionsNames = GET PARTITIONS NAMES!!!!
		
		onPIMACreateOptionsMenu();
		
		View view = inflater.inflate(R.layout.fragment_partitions, container, false);
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_partitions"));
		}
		
		TextView backView = (TextView)view.findViewById(R.id.backView);
		if(backView != null) {
			backView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("label_back"));
		}
		OnClickListener ocl = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).fragmentSystemState.setPartition(-1, null);
				((MainScreenActivity)getActivity()).swapFragments(((MainScreenActivity)getActivity()).fragmentSystemState);
			}
		};
		backView.setOnClickListener(ocl);
		
		mListView = (ListView) view.findViewById(R.id.partitions);
		mListView.setBackgroundColor(getResources().getColor(R.color.clear));
		mListView.setCacheColorHint(0);
		
        return view;
	}
	
	
	
	@Override
	public void onResume() {
		super.onResume();
		

//		if(null != ((SpaApplication)getActivity().getApplication()).latestSystemStatus)
//		{
//			refreshList();
//			return;
//		}
		
	}


	/**
	 * 
	 */
	public void onPIMACreateOptionsMenu() {
	
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View menu = vi.inflate(R.layout.aa_main_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
			case R.id.menu_logout:
				((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
				((BaseActivity)getActivity()).disconnect();
				break;
		}
		
	}

	
	
	/**
	 * 
	 *
	private void refreshList() {
		if(dialog != null)
		{
			dismissLoader();
		}
		
		boolean isWholeSystemDisarmed = false;
		Partition row = null;
		ArrayList<Partition> list = new ArrayList<Partition>();
		
		TYPES_SYSTEM_STATUS st = ((SpaApplication)getActivity().getApplication()).connectedSystem.getSystemStatusType();
		String statusKey = ((SpaApplication)getActivity().getApplication()).connectedSystem.getConfiguration().getSystemStatusTextKeys().get(st.ordinal()); 
		String status = ((SpaApplication)(getActivity()).getApplication()).getLocalizedString(statusKey);
		
		for(int i=0; i<status.length(); i++)
		{
			int partNum = i+1;
			int val = Integer.valueOf(status.substring(i, i+1));
			
			if(val>=0 && val<=5)
			{
				if(val==0)
				{
					isWholeSystemDisarmed = true;
				}
				
				TYPES_SYSTEM_STATUS partitionStatusType = TYPES_SYSTEM_STATUS.getAtIndex(val);
				
				row = new Partition(partNum, partitionStatusType);
				if(null != partitionsNames.get(partNum))
				{
					row.setName(partitionsNames.get(partNum).mName);
				}
				else
				{
					String partition = ((SpaApplication)(getActivity()).getApplication()).getLocalizedString("label_partition");
					row.setName(partition + " " + partNum);
				}
				list.add(row);
			}
		}
		
		
		//add whole system
		if(isWholeSystemDisarmed)
		{
			row = new Partition(-1, TYPES_SYSTEM_STATUS.Disarm);
		}
		else
		{
			row = new Partition(-1, TYPES_SYSTEM_STATUS.Full);
		}
		String whole = ((SpaApplication)(getActivity()).getApplication()).getLocalizedString("label_whole_system");
		row.setName(whole);
		list.add(row);

		PartitionsArrayAdapter mAdapter = new PartitionsArrayAdapter(this.getActivity(), list);
		mListView.setAdapter(mAdapter);
		
//		if(null == mAdapter)
//		{
//			PartitionsArrayAdaptermAdapter = new PartitionsArrayAdapter(this.getActivity(), list);
//			mListView.setAdapter(mAdapter);
//		}
//		else
//		{
//			mAdapter.updateData(list);
//			mAdapter.notifyDataSetChanged();
//		}
	}
	***/
}
