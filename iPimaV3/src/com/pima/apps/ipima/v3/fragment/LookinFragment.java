package com.pima.apps.ipima.v3.fragment;


import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.util.LookinArrayAdapter;
import com.pima.apps.ipima.v3.views.Lookin;
import com.pima.apps.ipima.v3.views.SystemZone;

public class LookinFragment extends BaseFragment{

	Lookin[] lookinObjects = null;
	private ListView mListView;
	
	
	LookinArrayAdapter mAdapter;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		((SpaApplication)getActivity().getApplication()).isBypassZonesFragment = false;
		
		onPIMACreateOptionsMenu();
		
		View view = inflater.inflate(R.layout.fragment_lookin, container, false);		
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		}
		
		mListView = (ListView) view.findViewById(R.id.lookinList);
		
		mListView.setBackgroundColor(getResources().getColor(R.color.clear));
		mListView.setCacheColorHint(0);
		
		
		
        return view;
	}
	
	
	@Override
	public void onResume() {
		
		super.onResume();
		
		int[] lookinIds = ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().getRemoteLookInZones();
		
		ArrayList<Lookin> tmp = new ArrayList<Lookin>();
		
		for(int i=0; i<lookinIds.length; i++)
		{
			int zoneNumber = lookinIds[i];
			SystemZone[] zones = ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getAllSystemZones();
			
			String zoneName = null;
			if(null != zones)
			{
				for(int j=0; j<zones.length; j++)
				{
					if(zones[j].getNumber() == zoneNumber)
					{
						zoneName = zones[j].getName();
						break;
					}
				} 
			}
			
			if(null == zoneName)
			{
				continue;
//				zoneName = ((SpaApplication)getActivity().getApplication()).getLocalizedString("Zone") + " " + zoneNumber;
			}
			
			Lookin l = new Lookin(i+1, zoneNumber, zoneName);
			tmp.add(l);
		}
		
		if(tmp.size() == 0)
		{
			String s = ((SpaApplication)getActivity().getApplication()).getLocalizedString("No-Lookin-Zones");
			Lookin l = new Lookin(0, -1, s);
			tmp.add(l);
		}
		
		lookinObjects = new Lookin[tmp.size()];
		for(int i=0; i<tmp.size(); i++)
		{
			lookinObjects[i] = tmp.get(i);
		}
		
		mAdapter = new LookinArrayAdapter(getActivity(), lookinObjects);
		mListView.setAdapter(mAdapter);
	}
	
	/**
	 * 
	 */
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View menu = vi.inflate(R.layout.aa_zones_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button log = (Button)menu.findViewById(R.id.menu_log);
		log.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		log.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_log);
			}
		});
		
		Button faults = (Button)menu.findViewById(R.id.menu_faults);
		faults.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Faults"));
		faults.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_faults);
			}
		});
		
		Button lookin = (Button)menu.findViewById(R.id.menu_lookin);
		lookin.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		lookin.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_lookin);
			}
		});
		
		if(	((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration() != null &&
			((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().isLookIn())
		{
			lookin.setVisibility(View.VISIBLE);
		}
		else
		{
			lookin.setVisibility(View.GONE);
		}
		
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
//		Button refresh = (Button)menu.findViewById(R.id.menu_refresh_zones_names);
		Button bypass = (Button)menu.findViewById(R.id.menu_bypass_zones);
//			refresh.setVisibility(View.VISIBLE);
//			refresh.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_refresh_zones_names"));
//			refresh.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((MainScreenActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_refresh_zones_names);
//				}
//			});
		bypass.setVisibility(View.VISIBLE);
		bypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		bypass.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_bypass_zones);
			}
		});
		
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	/**
	 * 
	 * @param menu_id
	 */
	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_logout:
			((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
			((BaseActivity)getActivity()).disconnect();
			break;
			
		case R.id.menu_bypass_zones:
			((MainScreenActivity)getActivity()).loadBypassZonesFragment();
			break;
			
		case R.id.menu_log:
			((MainScreenActivity)getActivity()).selectLogs();
			break;
			
		case R.id.menu_faults:
			((MainScreenActivity)getActivity()).selectFaults();
			break;
			
		case R.id.menu_lookin:
			((MainScreenActivity)getActivity()).selectLookin();
			break;
		}
	}

	
	
}
