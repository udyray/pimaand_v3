package com.pima.apps.ipima.v3.fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.SystemsActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SystemsListFragment extends BaseFragment 
{

	private JSONArray mSystemsArray = null;
	private SystemsAdapter mAdapter;
	
	private ListView mList = null;
//	private ImageView mSettingsBtn = null;
	private TextView mSettings = null;
	private View empty = null;
	
	public interface SystemsListCallbacks {
		public void onSystemAdd();
//		public void onSystemSelect(String id, String name);
		public void onConnectToSystem (JSONObject systemJson, boolean isEdit);
	}
	private SystemsListCallbacks mDelegate;
	
	
	public static SystemsListFragment newInstance() {
		return new SystemsListFragment();
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		try {
			mDelegate = (SystemsListCallbacks) context;
		}
		catch (Exception e) {
			throw new ClassCastException(context.toString() + " must implement SystemsListCallbacks");
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		onPIMACreateOptionsMenu();
	}
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.systems_list, container, false);
		
		mList = (ListView) v.findViewById(android.R.id.list);
		mList.setCacheColorHint(0);
		
		// set the empty view
//		View empty = getActivity().getLayoutInflater().inflate(R.layout.empty_list, null);
//		ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//		empty.setLayoutParams(lp);
//		mList.setEmptyView(empty);
		
		
		
//		((ViewGroup)mList.getParent()).addView(v);
				
//		mSettingsBtn = (ImageView) v.findViewById(R.id.settingsBtn);
		mSettings = (TextView)v.findViewById(R.id.settings);
		mSettings.setText(((BaseActivity)getActivity()).mApplication.getLocalizedString("Settings"));
//		mSettings.setVisibility(View.GONE);
		
		ViewGroup parentGroup = (ViewGroup)mList.getParent();
		empty = getActivity().getLayoutInflater().inflate(R.layout.system_empty,
		                                                  parentGroup,
		                                                  false);
		System.out.println("INVISIBLE1");
		empty.setVisibility(View.INVISIBLE);
		TextView t1 = (TextView)empty.findViewById(R.id.add_alarm_1);
		t1.setText(((SystemsActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add.Line1"));
		
		TextView t21 = (TextView)empty.findViewById(R.id.add_alarm_21);
		t21.setText(((SystemsActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add.Line21"));
		
		TextView t22 = (TextView)empty.findViewById(R.id.add_alarm_22);
		t22.setText(((SystemsActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add.Line22"));
		
		TextView t3 = (TextView)empty.findViewById(R.id.add_alarm_3);
		t3.setText(((SystemsActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add.Line3"));
		
		TextView t4 = (TextView)empty.findViewById(R.id.add_alarm_4);
		t4.setText(((SystemsActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add.Line4"));
		
		parentGroup.addView(empty);
//		mList.setEmptyView(empty);
		
		
		
		return v;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		 
//		mList.setOnItemClickListener(mListItemClickListener);
		registerForContextMenu(mList);
//		mList.setBackgroundResource(R.drawable.background);
		
		
//		mSettingsBtn.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				((SystemsActivity)getActivity()).swapToSettings();
//			}
//		});
		
		
		mSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(null != dialog)
				{
					startLoader();
				}
				((SystemsActivity)getActivity()).getClient();
			}
		});
		
		getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("AlarmSystems"));
		
		mAdapter = new SystemsAdapter(getActivity(), null);
		mList.setAdapter(mAdapter);
	}
	
	
	/**
	 * 
	 */
	public void getUserSystems()
	{
		if(dialog != null)
		{
			startLoader();
		}
		
		((SystemsActivity)getActivity()).getUserSystems();
	}
	
	public void deleteUserSystem(String pSystenId)
	{
		if(dialog != null)
		{
			startLoader();
		}
		
		((SystemsActivity)getActivity()).deleteUserSystem(pSystenId);
		
	}
	/**
	 * 
	 */
	private void fillData() {
		
		// load data.
//		mAdapter.setData(mSystemsArray);
		mAdapter.notifyDataSetChanged();
		mList.setAdapter(mAdapter);

		if(mList.getAdapter().isEmpty())
		{System.out.println("VISIBLE");
			empty.setVisibility(View.VISIBLE);
		}
		else
		{System.out.println("INVISIBLE");
			empty.setVisibility(View.INVISIBLE);
		}
		
		if(dialog != null)
		{
			dismissLoader();
		}
	}
	
	
	@Override
	public void onResume() {		

		super.onResume();
		
		// Start timer and launch main activity
		getUserSystems();
			
	}

	/*** Options Menu ***/
	
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		
		View menu = vi.inflate(R.layout.aa_fragment_systems_list, null);
				
		((SystemsActivity)getActivity()).replaceMoreView(menu);
		
		Button add = (Button)menu.findViewById(R.id.menu_add);
		add.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SystemsActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_add);
			}
		});
		
		add.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Add"));
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((SystemsActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_add:
			mDelegate.onSystemAdd();
			break;
		}
	}
	
	
	public void setSystemsArray(JSONArray pArray)
	{
		mSystemsArray = pArray;
		fillData();
	}
	
	public void setEdit(boolean pIsEdit)
	{
		mAdapter.setIsEdit(pIsEdit);
		fillData();
	}
	 
	/*** Custom Cursor Adapter ***/
	private class SystemsAdapter extends BaseAdapter {

		boolean mIsEdit;
	    Context mContext;

	    public SystemsAdapter(Context pContext, JSONArray pData) {
	        super();
	        mContext = pContext;
	        mIsEdit = false;
	    }

	    public void setIsEdit(boolean pIsEdit)
		{
	    	mIsEdit = pIsEdit;
		}
	    
	    @Override
	    public int getCount() {
	    	if(mSystemsArray == null)
	    	{
	    		return 0;
	    	}
	        return mSystemsArray.length();
	    }

	    @Override
	    public Object getItem(int arg0) {
	        try {
	            return mSystemsArray.getJSONObject(arg0);
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        return null;
	    }

	    @Override
	    public long getItemId(int arg0) {
	        return arg0;
	    }

	    @Override
	    public boolean hasStableIds(){
	        return true;
	    }

	    @Override
	    public boolean isEmpty(){
	        return mSystemsArray==null || mSystemsArray.length()==0;
	    }

	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View rowView = inflater.inflate(R.layout.row_system, parent, false);
	        
	        JSONObject jo = null;
	        try
	        {
	        	jo = (JSONObject) mSystemsArray.get(position);
	        }
	        catch(Exception e){
	        	e.printStackTrace();
	        }
	        final String currSystemId = jo.optString("systemId");
	        
	        final Button deleteBtn = (Button)rowView.findViewById(R.id.doDeleteBtn);
	        deleteBtn.setVisibility(View.GONE);
	        deleteBtn.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Delete"));
	        deleteBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						deleteUserSystem(currSystemId);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			});
	        

	        final ImageView expand = (ImageView)rowView.findViewById(R.id.expand);
	        if(mIsEdit)
	        {
	        	expand.setVisibility(View.VISIBLE);
	        }
	        else
	        {
	        	expand.setVisibility(View.GONE);
	        }
	        
//	        expand.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					String sysId = null;
//					String name = null;
//					try 
//					{
//						sysId = ((JSONObject)mAdapter.getItem(position)).getString("systemId");
//						name = ((JSONObject)mAdapter.getItem(position)).getString("name");
//					}
//					catch(Exception e){
//						e.printStackTrace();
//					}
//					
//					mDelegate.onSystemSelect(sysId, name);
//				}
//			});
	        
	        ImageView delete = (ImageView)rowView.findViewById(R.id.deleteSystem);
	        if(mIsEdit)
	        {
	        	delete.setVisibility(View.VISIBLE);
	        }
	        else
	        {
	        	delete.setVisibility(View.GONE);
	        }
	        delete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(deleteBtn.getVisibility() == View.VISIBLE)
					{
						deleteBtn.setVisibility(View.GONE);
						expand.setVisibility(View.VISIBLE);
					}
					else
					{
						deleteBtn.setVisibility(View.VISIBLE);
						expand.setVisibility(View.GONE);
					}
					
				}
			});
	        
	        TextView text1=(TextView)rowView.findViewById(R.id.text1);
						
	        RelativeLayout button = (RelativeLayout)rowView.findViewById(R.id.button1);
	        button.setOnClickListener(buttonClickListener);
	        //ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	        try {
	            text1.setText(jo.getString("name"));
	            button.setTag(jo);
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        return rowView;
	    }

	    private OnClickListener buttonClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (v.getId() == R.id.button1) {
					JSONObject systemJson = (JSONObject) v.getTag();
					Log.v("iPIMA","--- Connect to ID: " + systemJson.optString("systemId"));
//					Log.v("iPIMA","--- " + model.getPair());
//					
					mDelegate.onConnectToSystem(systemJson, mIsEdit);
				}
				/****
				if(!mIsEdit)
				{
					if (v.getId() == R.id.button1) {
						JSONObject systemJson = (JSONObject) v.getTag();
						Log.v("iPIMA","--- Connect to ID: " + systemJson.optString("systemId"));
//						Log.v("iPIMA","--- " + model.getPair());
//						
						mDelegate.onConnectToSystem(systemJson);
					}
				}
				else
				{
					if (v.getId() == R.id.button1) {
//						JSONObject systemJson = (JSONObject) v.getTag();
//						String sysId = null;
//						String name = null;
//						try 
//						{
//							sysId = systemJson.optString("systemId");
//							name = systemJson.optString("name");
//						}
//						catch(Exception e){
//							e.printStackTrace();
//						}
//					
//						Log.v("iPIMA","--- Edit ID: " + systemJson.optString("systemId"));
//						mDelegate.onSystemSelect(sysId, name);
					}
				}
				***/
				
			}
		};
		
        
	} 
	
}
