package com.pima.apps.ipima.v3.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ToggleButton;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView.OnUpdateTask;
import com.pima.apps.ipima.v3.views.SystemOutput;

public class OutputsListFragment extends RefreshableFragment {

	    
	private TextView outputEmpty = null;
	
	public static final String LOG_TAG = "OutputsListFragment";
	
	public static final String KEY_SYSTEM_ID = "systemId";

	private OutputArrayAdapter mAdapter;
	private String mSystemId;
	private Button stopSirenBtn;

	public interface OutputsListCallbacks {
		public void onOutputAdd(String systemId);
		public void onOutputSelect(String systemId, long id);
		
		public void replaceMoreView(View moreView);
		public void displayMoreView();
		public void dismissMoreView();
		
		public void replaceLoader(View moreView);
		public void displayLoader();
		public void dismissLoader();
		
	}
	private OutputsListCallbacks mDelegate;
	
	private SystemOutput[] mEnabledOutputs;
	private SystemOutput[] outputsToUpdate;
	
	public static OutputsListFragment newInstance(Bundle args) {
		OutputsListFragment fragment = new OutputsListFragment();
		fragment.setArguments(args);
		return fragment;
	}	
	
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		try {
			mDelegate = (OutputsListCallbacks) context;
		}
		catch (Exception e) {
			throw new ClassCastException(context.toString() + " must implement OutputsListCallbacks");
		}
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onPIMACreateOptionsMenu();
 	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = null;

		view = inflater.inflate(R.layout.fragment_outputs, container, false);
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Outputs"));
		}
		
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float logicalDensity = (metrics.ydpi / 160f);
		
		int screen_height = getResources().getDisplayMetrics().heightPixels;
		double screen_height_dp = ((double) screen_height) / logicalDensity;
		
		stopSirenBtn = (Button) view.findViewById(R.id.stopSirenBtn);
		
		if(screen_height_dp <= 480) { //fixed 13dp margin
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)stopSirenBtn.getLayoutParams();
			lp.bottomMargin = (int)(13*logicalDensity);
		}else if(screen_height_dp <= 511) { //floating 14dp-44dp margin
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)stopSirenBtn.getLayoutParams();
			lp.bottomMargin = (int)((screen_height_dp - 467)*logicalDensity);
		}//in the xml it is 45dp
		stopSirenBtn.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("StopSiren"));
		stopSirenBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {//Stop Siren
				if(dialog != null)
				{
					startLoader();
				}
				((MainScreenActivity)getActivity()).stopSiren();
			}
		});
		
		/***
		TextView internal = (TextView)view.findViewById(R.id.internal);
		if(internal != null) {
			internal.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Internal-Siren"));
		}
		
		TextView external = (TextView)view.findViewById(R.id.external);
		if(external != null) {
			external.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("External-Siren"));
		}
		
		ToggleButton internalToggle = (ToggleButton)view.findViewById(R.id.internalToggle);
		internalToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
				refreshHandler.removeCallbacks(refreshRunnable);
				if(isChecked)
				{
					//***** ACTIVATE INTERNAL *****
					if(dialog != null)
						startLoader();
					((MainScreenActivity)getActivity()).setOutputStatus(INTERNAL_SIREN_ID, true);
					//**********33**************

				}
				else
				{
					//***** DEACTIVATE INTERNAL *****
					if(dialog != null)
						startLoader();
					((MainScreenActivity)getActivity()).setOutputStatus(INTERNAL_SIREN_ID, false);
					//***********33***************
				}
				
			}
		});
		
		ToggleButton externalToggle = (ToggleButton)view.findViewById(R.id.externalToggle);
		externalToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
				
				refreshHandler.removeCallbacks(refreshRunnable);
				if(isChecked)
				{
					//***** ACTIVATE INTERNAL *****
					if(dialog != null)
						startLoader();
					((MainScreenActivity)getActivity()).setOutputStatus(EXTERNAL_SIREN_ID, true);
					//***********34************

				}
				else
				{
					//***** DEACTIVATE INTERNAL *****
					if(dialog != null)
						startLoader();
					((MainScreenActivity)getActivity()).setOutputStatus(EXTERNAL_SIREN_ID, false);
					//************34**************
				}
				
			}
		});
		
		****/
		
	
		mListView=(RefreshableListView) view.findViewById(R.id.outputsList);
		mListView.setCacheColorHint(0);
		
		// set the empty view
		outputEmpty = (TextView)view.findViewById(android.R.id.empty);
		outputEmpty.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("NoOutputs"));
				
		ListView lv = (ListView) view.findViewById(R.id.outputsListEnable);
		lv.setVisibility(View.GONE);
		
        return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		outputEmpty.setVisibility(View.INVISIBLE);
		mListView.setEmptyView(outputEmpty);
		
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mSystemId = getArguments() != null ? getArguments().getString(KEY_SYSTEM_ID) : null;
		
		if (mSystemId == null) {
			mSystemId = ((BaseActivity)getActivity()).mApplication.getConnectedSystem().getSystemId();
			if(mSystemId == null)
			{
				getActivity().finish();
				return;
			}
		}
		
//		mSystemModel = getSystemById(mSystemId);
		if (((BaseActivity)getActivity()).mApplication.getConnectedSystem() == null) {
			getActivity().finish();
			return;
		}
		
		
		//mListView.setOnItemClickListener(mListItemClickListener);
		registerForContextMenu(mListView);
		mListView.setBackgroundResource(R.drawable.background);
		
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new OutputArrayAdapter(getActivity(), null);
		mListView.setAdapter(mAdapter);

		
		
//		ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//		empty_view.setLayoutParams(lp);
						
		String systemName = ((BaseActivity)getActivity()).mApplication.getConnectedSystem().getSystemName(); 
		getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add_ManageOutput") + " - " + systemName);
		
		if(dialog == null) {
			LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View fl = (View)vi.inflate(R.layout.loader_overlay, null);
			
			dialog = new Dialog(getActivity(), R.style.NewDialog);
			dialog.setContentView(fl);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);				
		}
		
		mListView.setOnUpdateTask(new OnUpdateTask() {
			
			public void updateBackground() {
				
				// simulate long times operation.
				try {
					if(getActivity() == null || isResumed() == false || isRemoving()) {
						dismissLoader();
						return;
					}
					isReloadPending = true;
					
					mUpdateHandler.removeMessages(REGULAR_UPDATE);
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							if(getActivity() == null || isResumed() == false || isRemoving()) {
								dismissLoader();
								return;
							}
							mEnabledOutputs = getEnabledOutputs();
							outputsToUpdate = mEnabledOutputs;
							
							if(outputsToUpdate.length > 0) {
								mUpdateHandler.sendEmptyMessage(REGULAR_UPDATE);
							}
						}
					});
					while(isReloadPending) {
						Thread.sleep(1000);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			public void updateUI() {
				//aa.notifyDataSetChanged();
			}

			public void onUpdateStart() {

			}

		});
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	private SystemOutput[] getEnabledOutputs()
	{
		if(null == ((SpaApplication)getActivity().getApplication()).getConnectedSystem())
		{
			return null;
		}
		
		if(null == ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getSystemOutputs())
		{
			return null;
		}
			
		return ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getEnabledSystemOutputs();
	}
	/*** LoaderCallbacks ***/
	
	
	/*** Options Menu ***/
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View menu = vi.inflate(R.layout.aa_zones_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button log = (Button)menu.findViewById(R.id.menu_log);
		log.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		log.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_log);
			}
		});
		
		Button faults = (Button)menu.findViewById(R.id.menu_faults);
		faults.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Faults"));
		faults.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_faults);
			}
		});
		
		Button lookin = (Button)menu.findViewById(R.id.menu_lookin);
		lookin.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		lookin.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_lookin);
			}
		});
		
		if(	((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration() != null &&
			((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().isLookIn())
		{
			lookin.setVisibility(View.VISIBLE);
		}
		else
		{
			lookin.setVisibility(View.GONE);
		}
		
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
//		Button refresh = (Button)menu.findViewById(R.id.menu_refresh_zones_names);
		Button bypass = (Button)menu.findViewById(R.id.menu_bypass_zones);
//			refresh.setVisibility(View.VISIBLE);
//			refresh.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_refresh_zones_names"));
//			refresh.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((MainScreenActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_refresh_zones_names);
//				}
//			});
		bypass.setVisibility(View.VISIBLE);
		bypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		bypass.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_bypass_zones);
			}
		});
		
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_logout:
			((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
			((BaseActivity)getActivity()).disconnect();
			break;
			
		case R.id.menu_bypass_zones:
			((MainScreenActivity)getActivity()).loadBypassZonesFragment();
			break;
			
		case R.id.menu_log:
			((MainScreenActivity)getActivity()).selectLogs();
			break;
			
		case R.id.menu_faults:
			((MainScreenActivity)getActivity()).selectFaults();
			break;
			
		case R.id.menu_lookin:
			((MainScreenActivity)getActivity()).selectLookin();
			break;
		}
	}
	

	public class OutputArrayAdapter extends BaseAdapter
	{
		SystemOutput[] mObjects;
		Context mContext;
		
		public OutputArrayAdapter(Context pContext, SystemOutput[] pObjects){
			super();
			mContext = pContext;
			mObjects = pObjects;
		}

		@Override
		public int getCount() {
			if(null != mObjects)
			{
				return mObjects.length;
			}
			return 0;
		}

		@Override
		public Object getItem(int position) {
			if(position < getCount())
			{
				return mObjects[position];
			}
			
			return null;
		}

		@Override
		public long getItemId(int position) {
			
			if(null != getItem(position))
			{
				SystemOutput so = (SystemOutput)getItem(position);
				return so.getId();
			}
			return -1;
		}

		public void updateData(SystemOutput[] pObjects)
		{
			mObjects = pObjects;
		}
		
		@SuppressLint("ResourceAsColor")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			FrameLayout rowView = (FrameLayout)convertView;
			SystemOutput row = (SystemOutput)getItem(position);
			if(rowView == null)
			{
				rowView = (FrameLayout)mInflater.inflate(R.layout.outputs_list_item, null);
			}
			
			TextView tv = (TextView)rowView.findViewById(R.id.output_text);
			long id = row.getId();
			tv.setTag(Integer.valueOf((int)id));
			
			String name = row.getName();
			if(row.getName() == null || row.getName().equals("null"))
			{
				name = ((BaseActivity)getActivity()).mApplication.getLocalizedString("Output" + row.getId());
			}
			tv.setText(name);
			
			View bv = (View)rowView.findViewById(R.id.backView);
			
			bv.setTag(Integer.valueOf((int)id));
			bv.setOnClickListener(new OnClickListener() {
				
				@Override 
				public void onClick(View v) {
					Integer tag = (Integer)v.getTag();
					if(tag != null)
					{
						mDelegate.onOutputSelect(mSystemId, tag);
					}
				}
			});
			
			Button switchButton = (Button)rowView.findViewById(R.id.touchButton);
			ToggleButton tButton = (ToggleButton) rowView.findViewById(R.id.toggleButton1);
		
			switchButton.setVisibility(View.VISIBLE);
			tButton.setVisibility(View.VISIBLE);
			
			tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
				}
			});
			
			switchButton.setTag(tButton);
			switchButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {					
				}
			});
			
			SystemOutput so = (SystemOutput)this.getItem(position);
			if(null != so)
			{
				Integer number = so.getId();
				Boolean isActivated = so.isActive();
				if(isActivated != null && isActivated)
				{
					tButton.setChecked(true);
				}else
				{
					tButton.setChecked(false);
				}
				tButton.setTag(number);
			}
			tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
					
					mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
					mUpdateHandler.removeMessages(REGULAR_UPDATE);
					
					if(dialog != null)
					{
						startLoader();
					}
					
					if(isChecked)
					{
						((MainScreenActivity)getActivity()).setOutputStatus("" + ((Integer)buttonView.getTag()), true);
					}
					else
					{
						((MainScreenActivity)getActivity()).setOutputStatus("" + ((Integer)buttonView.getTag()), false);
					}
					
				}
			});
			
			switchButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {	
					ToggleButton tb = (ToggleButton)v.getTag();
					
					if(tb.isChecked())
					{
						tb.setChecked(false);
					}else
					{
						tb.setChecked(true);
					}
				}
			});
			
			return rowView;
		
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}
	}



	@Override
	protected void executeUpdate(boolean isBackground) {
		if(null == mEnabledOutputs || mEnabledOutputs.length ==0)
		{
			((MainScreenActivity)getActivity()).getSystemStatus(isBackground);
		}
		else
		{
			((MainScreenActivity)getActivity()).getOutputsStatus(isBackground); 
		}
	}


	@Override
	protected void refreshList() {
		
		if(isDisconnected)
		{
			return;
		}
		
		isReloadPending = false;
		
		mEnabledOutputs = getEnabledOutputs();
		if(null == mEnabledOutputs)
		{
			mUpdateHandler.sendEmptyMessageDelayed(REGULAR_UPDATE, 200);		
		}
		else
		{
			if(null == mAdapter)
 			{
				mAdapter = new OutputArrayAdapter(getActivity(), mEnabledOutputs);
				mListView.setAdapter(mAdapter);
 			}
 			else
 			{
 				mAdapter.updateData(mEnabledOutputs);
 				getActivity().runOnUiThread(new Runnable() {
 				    public void run() {
 				    	mAdapter.notifyDataSetChanged();
 				    }
 				});
 			}
			
			
			outputEmpty.setVisibility(View.VISIBLE);
			mListView.setEmptyView(outputEmpty);
			
		}
	}
	
	@Override
	public void onPause() {
		mAdapter = null;
		super.onPause();
	}
	
}
