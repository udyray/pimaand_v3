package com.pima.apps.ipima.v3.fragment;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView;


abstract public class RefreshableFragment extends BaseFragment{

	protected static final int REGULAR_UPDATE 		= 0;
	protected static final int BACKGROUND_UPDATE 	= 1;
	
	boolean isReloadPending;
	protected RefreshableListView mListView;
	
	long lastUpdate = -1;
	
	final Handler mUpdateHandler = new Handler(Looper.getMainLooper()){
    	
        @Override
        public void handleMessage(final Message inputMessage) {

        	mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
			mUpdateHandler.removeMessages(REGULAR_UPDATE);
			
        	lastUpdate =  System.currentTimeMillis();
        	
        	if(	null != getActivity() && 
        		null != ((SpaApplication)getActivity().getApplication()) &&
        		((SpaApplication)getActivity().getApplication()).getConnectedSystem() != null) {
            	
    			getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						
						if(isHidden() || !isVisible())//stop cache if not visible
			        	{
			        		return;
			        	}
						isReloadPending = true;
						if(inputMessage.what == REGULAR_UPDATE)
						{
							if(dialog != null)
							{
								startLoader();
							}
							executeUpdate(false);
						}
						else if(inputMessage.what == BACKGROUND_UPDATE)
						{
							executeUpdate(true);	
						}
					}
				});
        	}
        	else {
				dismissLoader();
			}
        }
    };
    
    
    @Override
	public void onResume() {
		super.onResume();
		
		if(this == null || this.getActivity() == null)
			return;
		
		refreshScreen();
		
	}

    
    abstract protected void executeUpdate(boolean isBackground);
	
	
	/**
	 * 
	 */
    abstract protected void refreshList();
	
	/**
	 * 
	 */
	private void requestUpdate()
	{
		if(this == null || this.getActivity() == null)
			return;
		
//		if(!isVisible())
//			return;
		
		if(isHidden())
			return;
			
		if(isDisconnected)
		{
			return;
		}
		
		mUpdateHandler.sendEmptyMessageDelayed(BACKGROUND_UPDATE, 15000); 
	}
	
	/**
	 * 
	 */
	public void refreshScreen() {
		
		if(dialog != null)
		{
			startLoader();
		}
		
		refreshList();
	
		if(dialog != null)
		{
			dismissLoader();
		}
		
		requestUpdate();
	}

	public void cancelCurrentUpdate() 
	{
		mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
		mUpdateHandler.removeMessages(REGULAR_UPDATE);
	}
	
	public void dismissLoader() {
		if(null != mListView)
		{
			mListView.closeLoading();
		}
		super.dismissLoader();
	}
}
