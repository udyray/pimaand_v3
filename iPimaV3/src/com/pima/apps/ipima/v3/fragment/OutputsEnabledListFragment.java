package com.pima.apps.ipima.v3.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ToggleButton;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.SystemEditActivity;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView;
import com.pima.apps.ipima.v3.views.SystemOutput;

public class OutputsEnabledListFragment extends BaseFragment {
	
	private ListView mListView;	
	boolean isReloadPending;
	
	Button stopSirenBtn;
	public Button doneItem;
	
	private TextView outputEmpty = null;
	
	public static final String LOG_TAG = "OutputsEnabledListFragment";
	
	private OutputArrayAdapter mAdapter;
	private String mSystemId;
//	private SystemModel mSystemModel;
	
	
	private SystemOutput[] outputsToUpdate;
	
	public static OutputsEnabledListFragment newInstance(Bundle args) {
		OutputsEnabledListFragment fragment = new OutputsEnabledListFragment();
		fragment.setArguments(args);
		return fragment;
	}	
	
	public static OutputsEnabledListFragment newInstance(Bundle args, boolean isLogged) {
		OutputsEnabledListFragment fragment = new OutputsEnabledListFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		outputEmpty.setVisibility(View.INVISIBLE);
		mListView.setEmptyView(outputEmpty);
		
		createScreen();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		onPIMACreateOptionsMenu();
 	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = null;

		view = inflater.inflate(R.layout.fragment_outputs, container, false);
		
		stopSirenBtn = (Button) view.findViewById(R.id.stopSirenBtn);
		stopSirenBtn.setVisibility(View.GONE);
		
		/***
		FrameLayout internal = (FrameLayout)view.findViewById(R.id.internalLayout);
		internal.setVisibility(View.GONE);
		
		FrameLayout external = (FrameLayout)view.findViewById(R.id.externalLayout);
		external.setVisibility(View.GONE);
		 ***/
		mListView=	(ListView) view.findViewById(R.id.outputsListEnable);
		mListView.setCacheColorHint(0);
		
		// set the empty view
		outputEmpty = (TextView)view.findViewById(android.R.id.empty);
		outputEmpty.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("NoOutputs"));
					
				
		RefreshableListView lv =(RefreshableListView) view.findViewById(R.id.outputsList);
		lv.setVisibility(View.GONE);
		
        return view;
	}
	
	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mSystemId = getArguments() != null ? getArguments().getString(SystemEditActivity.KEY_SYSTEM_EDIT) : null;
		if(mSystemId == null)
		{
			getActivity().finish();
			return;
		}
		
//		mSystemModel = getSystemById(mSystemId);
		if (((BaseActivity)getActivity()).mApplication.getConnectedSystem() == null) {
			getActivity().finish();
			return;
		}
		
		((SystemEditActivity)getActivity()).setTitle("Outputs");
		
		TextView titleView = (TextView)getActivity().findViewById(R.id.titleView);
		titleView.setVisibility(View.GONE);
		
		//mListView.setOnItemClickListener(mListItemClickListener);
		registerForContextMenu(mListView);
		mListView.setBackgroundResource(R.drawable.background);
		
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new OutputArrayAdapter(getActivity(), null);
		mListView.setAdapter(mAdapter);

		String systemName = ((BaseActivity)getActivity()).mApplication.getConnectedSystem().getSystemName(); 
		getActivity().setTitle(((BaseActivity)getActivity()).mApplication.getLocalizedString("AlarmSystem_Add_ManageOutput") + " - " + systemName);
		
	}
	
	/**
	 * 
	 * @param pOutputs
	 */
	private void setOutputs(SparseArray<SystemOutput> pOutputs)
	{
		outputsToUpdate = new SystemOutput[pOutputs.size()];
		for(int i=0; i<pOutputs.size(); i++)
		{
			int key = pOutputs.keyAt(i);
			SystemOutput so = pOutputs.get(key);
			outputsToUpdate[i] = new SystemOutput(so.getId(), so.getName(), so.isActive(), so.isEnabled());
		}
		fillData();
	}
	
	
	/**
	 * 
	 */
	private void fillData() {	
		
		if(null != outputsToUpdate)
		{
			if(null == mAdapter)
	 		{
				mAdapter = new OutputArrayAdapter(getActivity(), outputsToUpdate);
	 			mListView.setAdapter(mAdapter);
	 		}
	 		else
	 		{
	 			mAdapter.updateData(outputsToUpdate);
	 			mAdapter.notifyDataSetChanged();
	 			mListView.setAdapter(mAdapter);
	 		}
			
			outputEmpty.setVisibility(View.VISIBLE);
			mListView.setEmptyView(outputEmpty);
		}
		
		if(dialog != null)
		{
			dismissLoader();
		}
	}

	
	/*** Options Menu ***/
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = vi.inflate(R.layout.aa_fragment_outputs_list_no_login, null);
		
		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SystemEditActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
		doneItem.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(null != dialog)
				{
					startLoader();
				}
				
				List<String> ids = new ArrayList<String>(); 
				for(int i=0; i<outputsToUpdate.length; i++)
				{
					SystemOutput so = outputsToUpdate[i];
					if(so.isEnabled())
					{
						ids.add("" + so.getId());
					}
				}
				((SystemEditActivity)getActivity()).setOutputStatusEnable(ids);
			}
		});

		((SystemEditActivity)getActivity()).replaceMoreView(menu);
		
	}

	
	
	/*** Context Menu ***/
	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		
			case R.id.menu_discard:
				((SystemEditActivity)getActivity()).swapToEdit();
				break;
		
		}
	}
	
	private void createScreen() {
		
		
		if(null != dialog)
		{
			startLoader();
		}
		
		outputEmpty.setVisibility(View.INVISIBLE);
		mListView.setEmptyView(outputEmpty);
		
		setOutputs(((BaseActivity)getActivity()).mApplication.getConnectedSystem().getSystemOutputs());
	
	}
	
	public class OutputArrayAdapter extends BaseAdapter
	{
		SystemOutput[] mObjects;
		Context mContext;
		
		public OutputArrayAdapter(Context pContext, SystemOutput[] pObjects){
			super();
			mContext = pContext;
			mObjects = pObjects;
		}

		@Override
		public int getCount() {
			if(null != mObjects)
			{
				return mObjects.length;
			}
			return 0;
		}

		@Override
		public Object getItem(int position) {
			if(position < getCount())
			{
				return mObjects[position];
			}
			
			return null;
		}

		@Override
		public long getItemId(int position) {
			
			if(null != getItem(position))
			{
				SystemOutput so = (SystemOutput)getItem(position);
				return so.getId();
			}
			return -1;
		}

		public void updateData(SystemOutput[] pObjects)
		{
			mObjects = pObjects;
		}
		
		@SuppressLint("ResourceAsColor")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			FrameLayout rowView = (FrameLayout)convertView;
			SystemOutput row = (SystemOutput)getItem(position);
			if(rowView == null)
			{
				rowView = (FrameLayout)mInflater.inflate(R.layout.outputs_list_item, null);
			}
			
			TextView tv = (TextView)rowView.findViewById(R.id.output_text);
			long id = row.getId();
			tv.setTag(Integer.valueOf((int)id));
			
			String name = row.getName();
			if(row.getName() == null || row.getName().equals("null"))
			{
				name = ((BaseActivity)getActivity()).mApplication.getLocalizedString("Output" + row.getId());
			}
			tv.setText(name);
			
			Button switchButton = (Button)rowView.findViewById(R.id.touchButton);
			ToggleButton tButton = (ToggleButton) rowView.findViewById(R.id.toggleButton1);
			
			switchButton.setVisibility(View.VISIBLE);
			tButton.setVisibility(View.VISIBLE);
			
			tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
				}
			});
			
			switchButton.setTag(tButton);
			switchButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {					
				}
			});
			
			SystemOutput so = (SystemOutput)this.getItem(position);
			if(null != so)
			{
				Integer number = so.getId();
				Boolean isEnabled = so.isEnabled();
				if(isEnabled != null && isEnabled)
				{
					tButton.setChecked(true);
				}else
				{
					tButton.setChecked(false);
				}
				tButton.setTag(number);
			}
			tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
					
					//SET AS Enable/disabled
					outputsToUpdate[position].setIsEnabled(isChecked);
					
					boolean isChanged = false;
					for(int i=0; i<outputsToUpdate.length; i++)
					{
						SystemOutput so_ = ((BaseActivity)getActivity()).mApplication.getConnectedSystem().getSystemOutputs().get(outputsToUpdate[i].getId());
						
						if(outputsToUpdate[i].isEnabled() != so_.isEnabled())
						{
							isChanged = true;
							break;
						}
					}
					
					if(isChanged)
					{
						doneItem.setEnabled(true);
					}
					else
					{
						doneItem.setEnabled(false);
					}
					
				}
			});
			
			switchButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {	
					ToggleButton tb = (ToggleButton)v.getTag();
					
					if(tb.isChecked())
					{
						tb.setChecked(false);
					}else
					{
						tb.setChecked(true);
					}
				}
			});
			
			return rowView;
		
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}
	}

}
