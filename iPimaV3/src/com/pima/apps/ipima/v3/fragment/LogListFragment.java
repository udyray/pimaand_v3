package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.BaseActivity;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView;
import com.pima.apps.ipima.v3.pulltorefresh.RefreshableListView.OnUpdateTask;
import com.pima.apps.ipima.v3.util.LogsArrayAdapter;

public class LogListFragment extends RefreshableFragment{
	
	TextView logsEmpty;
	LogsArrayAdapter logsAdapter = null;
	
    
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_log, container, false);
		
		TextView titleView = (TextView)view.findViewById(R.id.titleView);
		if(titleView != null) {
			titleView.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		}
		
		mListView = (RefreshableListView) view.findViewById(R.id.refreshableListView);
		logsEmpty = (TextView) view.findViewById(android.R.id.empty);
		logsEmpty.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("NoLog"));
		
		mListView.setCacheColorHint(0);
		mListView.setFooterDividersEnabled(false);

        return view;
	}



	@Override
	public void onResume() {
		
		logsAdapter = null;
		
		mListView.setEmptyView(logsEmpty);
		logsEmpty.setVisibility(View.INVISIBLE);
		
		super.onResume();

	}



	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		onPIMACreateOptionsMenu();
		
		mListView.setOnUpdateTask(new OnUpdateTask() {
			
			public void updateBackground() {
				
				// simulate long times operation.
				try {
					if(getActivity() == null || isResumed() == false || isRemoving()) {
						dismissLoader();
						return;
					}
					
//					mUpdateHandler.removeMessages(BACKGROUND_UPDATE);
//					mUpdateHandler.removeMessages(REGULAR_UPDATE);
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if(getActivity() == null || isResumed() == false || isRemoving()) {
								dismissLoader();
								return;
							}
							
							logsAdapter = null;
							mUpdateHandler.sendEmptyMessage(REGULAR_UPDATE);
						}
					});
					while(isReloadPending) {
						Thread.sleep(1000);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			public void updateUI() {
				//aa.notifyDataSetChanged();
			}

			public void onUpdateStart() {

			}

		});
	}

	
	/**
	 * 
	 */
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View menu = vi.inflate(R.layout.aa_zones_screen_menu, null);
		
		((MainScreenActivity)getActivity()).replaceMoreView(menu);
		
		Button log = (Button)menu.findViewById(R.id.menu_log);
		log.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Log"));
		log.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_log);
			}
		});
		
		Button faults = (Button)menu.findViewById(R.id.menu_faults);
		faults.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Faults"));
		faults.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_faults);
			}
		});
		
		Button lookin = (Button)menu.findViewById(R.id.menu_lookin);
		lookin.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Lookin"));
		lookin.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_lookin);
			}
		});
		
		if(	((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration() != null &&
			((SpaApplication)getActivity().getApplication()).getConnectedSystem().getConfiguration().isLookIn())
		{
			lookin.setVisibility(View.VISIBLE);
		}
		else
		{
			lookin.setVisibility(View.GONE);
		}
		
		
		Button logout = (Button)menu.findViewById(R.id.menu_logout);
		logout.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Disconnect"));
		logout.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_logout);
			}
		});
		
//		Button refresh = (Button)menu.findViewById(R.id.menu_refresh_zones_names);
		Button bypass = (Button)menu.findViewById(R.id.menu_bypass_zones);
//			refresh.setVisibility(View.VISIBLE);
//			refresh.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_refresh_zones_names"));
//			refresh.setOnClickListener(new Button.OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					((MainScreenActivity)getActivity()).dismissMoreView();
//					onPIMAOptionsItemSelected(R.id.menu_refresh_zones_names);
//				}
//			});
		bypass.setVisibility(View.VISIBLE);
		bypass.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Bypass"));
		bypass.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainScreenActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_bypass_zones);
			}
		});
		
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_logout:
			((SpaApplication)getActivity().getApplication()).action = SpaApplication.CUR_ACTION.LOGOUT;
			((BaseActivity)getActivity()).disconnect();
			break;
			
		case R.id.menu_bypass_zones:
			((MainScreenActivity)getActivity()).loadBypassZonesFragment();
			break;
			
		case R.id.menu_log:
			((MainScreenActivity)getActivity()).selectLogs();
			break;
			
		case R.id.menu_faults:
			((MainScreenActivity)getActivity()).selectFaults();
			break;
			
		case R.id.menu_lookin:
			((MainScreenActivity)getActivity()).selectLookin();
			break;
		}
	}
	


	@Override
	protected void executeUpdate(boolean isBackground) {
		((MainScreenActivity)getActivity()).getSystemLog(isBackground);
	}



	@Override
	protected void refreshList() {

		if(isDisconnected)
		{
			return;
		}
		
		isReloadPending = false;
		
		if(null == logsAdapter)
 		{
			if(null == ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getSystemLog())
			{
				mUpdateHandler.sendEmptyMessageDelayed(REGULAR_UPDATE, 200);	
			}
			else
			{
				logsAdapter = new LogsArrayAdapter(this.getActivity(), ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getSystemLog());
	 			mListView.setAdapter(logsAdapter);
	 		
	 			logsEmpty.setVisibility(View.VISIBLE);
	 		}
 			
 		}
 		else
 		{
 			logsAdapter = new LogsArrayAdapter(this.getActivity(), ((SpaApplication)getActivity().getApplication()).getConnectedSystem().getSystemLog());
 			getActivity().runOnUiThread(new Runnable() {
			    public void run() {
			    	logsAdapter.notifyDataSetChanged();
			    }
			});
 			
 			logsEmpty.setVisibility(View.VISIBLE);
 		}
		
	}

	@Override
	public void onPause() {
		logsAdapter = null;
		super.onPause();
	}
}
