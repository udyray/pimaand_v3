package com.pima.apps.ipima.v3.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;


public class LoginProgressFragment extends Fragment {

	public static LoginProgressFragment newInstance(String text) {
		LoginProgressFragment f = new LoginProgressFragment();
		Bundle args = new Bundle();
		args.putString("text", text);
		f.setArguments(args);
		return f;
	}

	
	
	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		String text = getArguments().getString("text");
//		
//		View progressDialog = new ProgressDialog(getActivity());
//		progressDialog.setIndeterminate(true);
//		//progressDialog.setCancelable(false); // not workin if set here
//		
//		if (text != null) {
//			progressDialog.setMessage(text);
//		}
//
//		return progressDialog;
//	}

}
