package com.pima.apps.ipima.v3.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.SettingsActivity;
import com.pima.apps.ipima.v3.util.Utils;

public class SettingsFragment extends Fragment implements OnClickListener {

	private String mCurrentName;
	private String mCurrentPhone;
	
	private EditText mUserName, mUserPhone;// mServerUrl, mServerPort;
	
	public Button doneItem;
	private TextWatcher textWatcher = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			if(validateName(mUserName.getText().toString())) {
				if(doneItem != null) {
					doneItem.setEnabled(true);
				}
			}else {
				if(doneItem != null) {
					doneItem.setEnabled(false);
				}
			}
		}
	};
	
	
	/**
	 * 
	 * @param mCurrentName
	 * @param mCurrentPhone
	 */
	public SettingsFragment(String mCurrentName, String mCurrentPhone) {
		super();
		this.mCurrentName = mCurrentName;
		this.mCurrentPhone = mCurrentPhone;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.fragment_settings, container, false);
	}
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		TextView userNameTitle = (TextView)getActivity().findViewById(R.id.userNameTitle);
		userNameTitle.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("UserName"));
		
		TextView phoneTitle = (TextView)getActivity().findViewById(R.id.userPhoneTitle);
		phoneTitle.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("Telephone-Number"));
		
//		TextView serverUrlTitle = (TextView)getActivity().findViewById(R.id.serverUrlTitle);
//		serverUrlTitle.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("CloudURL"));
//		TextView serverPortTitle = (TextView)getActivity().findViewById(R.id.serverPortTitle);
//		serverPortTitle.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("CloudPort"));
		
		
		mUserName = (EditText)getActivity().findViewById(R.id.userName);
		mUserName.addTextChangedListener(textWatcher);
		
		mUserPhone = (EditText)getActivity().findViewById(R.id.userPhone);
		mUserPhone.addTextChangedListener(textWatcher);
		if(null != mCurrentPhone)
		{
			mUserPhone.setText(mCurrentPhone);
		}
		
		mUserPhone.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE) {
					if (mUserName.getText().length() > 0) {
						processUpdate();
					}
				}
				return false;
			}
		});

		
//		mServerUrl = (EditText)getActivity().findViewById(R.id.serverUrl);
//		mServerUrl.addTextChangedListener(textWatcher);
//		
//		mServerPort = (EditText)getActivity().findViewById(R.id.serverPort);
//		mServerPort.addTextChangedListener(textWatcher);
		
		
 
		onPIMACreateOptionsMenu();
		
		populateViews();
	}
	
	
	public void onPIMACreateOptionsMenu() {
		
		LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View menu = vi.inflate(R.layout.aa_fragment_settings, null);

		if(doneItem != null) {
			doneItem.setEnabled(false);
		}
		

		Button discard = (Button)menu.findViewById(R.id.menu_discard);
		discard.setText(((SpaApplication)getActivity().getApplication()).getLocalizedString("button_Discard"));
		discard.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((SettingsActivity)getActivity()).dismissMoreView();
				onPIMAOptionsItemSelected(R.id.menu_discard);
			}
		});
		
		
		((SettingsActivity)getActivity()).replaceMoreView(menu);
		
		if(doneItem != null) {
			doneItem.setOnClickListener(new Button.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mUserName.getWindowToken(), 0);
					imm.hideSoftInputFromWindow(mUserPhone.getWindowToken(), 0);
//					imm.hideSoftInputFromWindow(mServerUrl.getWindowToken(), 0);
//					imm.hideSoftInputFromWindow(mServerPort.getWindowToken(), 0);
					
					((SettingsActivity)getActivity()).dismissMoreView();
					onPIMAOptionsItemSelected(R.id.menu_done);
				}
			});
		}
		
		View fl = menu.findViewById(R.id.moreMenuOverflow);
		if(fl != null) {
			fl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((SettingsActivity)getActivity()).dismissMoreView();
				}
			});
		}
	}

	public void onPIMAOptionsItemSelected(int menu_id) {
		switch(menu_id) {
		case R.id.menu_done:
			processUpdate();
			
			break;
		
		case R.id.menu_discard:
			((SettingsActivity)getActivity()).finish();
			break;
		
		}
	}
	
	@Override
	public void onClick(View view) {
		
		switch (view.getId()) {
		case R.id.btn_ok:
			processUpdate();
			break;
			
		case R.id.btn_cancel:
			((SettingsActivity)getActivity()).finish();
			break;

		default:
			break;
		}
	}
	
	private Boolean validateName(String title) {
		if(title == null || title.length() == 0) {
			return false;
		}
//		if(nameIP == null || nameIP.length() == 0) {
//			return false;
//		}
//		if(port == null || port.length() == 0) {
//			return false;
//		}
//		if(!port.matches("^[0-9]*$")) {
//			return false;
//		}

		return true;
	}
	
	
	
	private void processUpdate() {
		String name, phone;
		
		name = mUserName.getText().toString().trim();
		phone = mUserPhone.getText().toString().trim();
//		host = mServerUrl.getText().toString().trim();
//		port = mServerPort.getText().toString().trim();
		if (Utils.isEmpty(name)) {
			return;
		}
		
		((SettingsActivity)getActivity()).addContact(name, phone);
		
	}
	
	private void populateViews() {
		
		mUserName.setText(mCurrentName!=null&&!mCurrentName.equals("null")?mCurrentName:"");
		mUserPhone.setText(mCurrentPhone!=null&&!mCurrentPhone.equals("null")?mCurrentPhone:"");
//		mServerUrl.setText(serverUrl!=null?serverUrl:PIMACommonSettings.DefaultServerUrl);
//		mServerPort.setText(serverPort!=null?serverPort:PIMACommonSettings.DefaultServerPort);
	}

}
