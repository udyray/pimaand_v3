package com.pima.apps.ipima.v3.util;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.views.SystemLog;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LogsArrayAdapter extends ArrayAdapter<SystemLog>{
	private Context context;
	private SystemLog[] logs;

	
	public LogsArrayAdapter(Context context, SystemLog[] objects) {
		super(context,R.layout.log_list_item, objects);
		this.context=context;
		
		if(objects == null || objects.length==0)
		{
			objects = new SystemLog[1];
			objects[0] = new SystemLog("");
			objects[0].setIsEmpty(true);
		}
		this.logs=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = null;
		
		SystemLog log=logs[position];
		if(log.getIsEmpty()) {
			rowView = inflater.inflate(R.layout.empty_logs_item, parent, false);
			TextView text = (TextView) rowView.findViewById(R.id.emptyLogs);
			text.setText(((SpaApplication)((Activity)context).getApplication()).getLocalizedString("Log.NoLogs"));
			return rowView;

		}
		
		if(null == rowView)
		{
			rowView = inflater.inflate(R.layout.log_list_item, parent, false);
		}
//		TextView date = (TextView) rowView.findViewById(R.id.dateTV);
//		date.setText(log.number+log.date);
//		TextView time = (TextView) rowView.findViewById(R.id.timeTV);
//		time.setText(log.hour);
		TextView text = (TextView) rowView.findViewById(R.id.logTV);
		text.setText(log.getText());

		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	public void updateData(SystemLog[] objects)
	{
		this.logs = objects;
	}

}
