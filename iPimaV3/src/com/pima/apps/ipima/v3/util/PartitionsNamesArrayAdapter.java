package com.pima.apps.ipima.v3.util;

import java.util.List;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class PartitionsNamesArrayAdapter extends BaseAdapter {
	
	public static class Partition extends Object {
		public int number;
		public String name;
		
		public Partition(int id) {
			super();
			this.number = id;
		}
		
		public void setName(String name)
		{
			this.name = name;
		}
	}
	
	List<Partition> objects;
	Context context;
	
	public PartitionsNamesArrayAdapter(Context context, List<Partition> objects){
		super();
		this.context = context;
		this.objects = objects;
	}

	public void updateData(List<Partition> objects)
	{
		this.objects = objects;
	}
	
	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public Object getItem(int arg0) {
		if(arg0 < objects.size())
		{
			return objects.get(arg0);
		}
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		if(arg0 < objects.size())
		{
			return objects.get(arg0).number;
		}
		return 0;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		LinearLayout rowView = (LinearLayout)convertView;
		Partition row = objects.get(position);
		if(rowView == null)
		{
			rowView = (LinearLayout)mInflater.inflate(R.layout.partition_name_list_item, null);			
		}
		
		String partition = ((SpaApplication)((Activity)context).getApplication()).getLocalizedString("label_partition_short");
		
		TextView partitionNumber = (TextView)rowView.findViewById(R.id.partitionNumber);		
		partitionNumber.setText(partition + " " + row.number + ":");
		
		EditText partitionName  = (EditText)rowView.findViewById(R.id.partitionName);	
		if(null != row.name)
		{
			partitionName.setText(row.name);
		}
		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
}

	