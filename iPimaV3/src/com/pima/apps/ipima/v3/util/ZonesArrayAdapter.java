package com.pima.apps.ipima.v3.util;


import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.views.SystemZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class ZonesArrayAdapter extends BaseAdapter {
	
	SystemZone[] objects;
	Context context;
	
	public ZonesArrayAdapter(Context context, SystemZone[] objects){
		super();
		this.context = context;
		this.objects = objects;
	}

	@Override
	public int getCount() {
		if(null == objects)
		{
			return 0;
		}
		return objects.length;
	}

	@Override
	public Object getItem(int position) {
		
		if(null != objects)
		{
			if(position < objects.length)
			{
				return objects[position];
			}
		}
		
		
		return null;
	}

	@Override
	public long getItemId(int position) {
		
		SystemZone sz = (SystemZone)getItem(position);
		if(null != sz)
		{
			return sz.getNumber();
		}
		return 0;
	}

	public void updateData(SystemZone[] objects)
	{
		this.objects = objects;
	}
	
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		LinearLayout rowView = (LinearLayout)convertView;
		SystemZone row = objects[position];
		if(rowView == null)
		{
			rowView = (LinearLayout)mInflater.inflate(R.layout.zones_list_item, null);
		}
		
		TableLayout textViewCont = (TableLayout)rowView.findViewById(R.id.tableRow1);
		
		TextView zoneName = (TextView)rowView.findViewById(R.id.zoneName);	
		FrameLayout openBackground = (FrameLayout)rowView.findViewById(R.id.openBackground);	
		ImageView open = (ImageView)rowView.findViewById(R.id.open);	
		FrameLayout alarmBackground = (FrameLayout)rowView.findViewById(R.id.alarmBackground);	
		ImageView alarm = (ImageView)rowView.findViewById(R.id.alarm);	
		FrameLayout bypassBackground = (FrameLayout)rowView.findViewById(R.id.bypassBackground);	
		ImageView bypass = (ImageView)rowView.findViewById(R.id.bypass);	
		
		Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int titleWidth = (int)Math.round((width*0.55));
		int columnWidth = (int)Math.round((width*0.15));

		LayoutParams zoneNameParams = zoneName.getLayoutParams();
		zoneNameParams.width = titleWidth;
		zoneName.setLayoutParams(zoneNameParams);
		
		LayoutParams columnParams = openBackground.getLayoutParams();
		columnParams.width = columnWidth;
		openBackground.setLayoutParams(columnParams);
		alarmBackground.setLayoutParams(columnParams);
		bypassBackground.setLayoutParams(columnParams);
		
//		TextView textViewExt = (TextView)rowView.findViewById(R.id.listTextViewEx);
//		textViewExt.setText("");
//		TextView textViewExtTwo = (TextView)rowView.findViewById(R.id.listTextViewExTwo);
//		textViewExtTwo.setText("");
		
		rowView.setBackgroundColor(context.getResources().getColor(R.color.list_item_color));
		zoneName.setTextColor(context.getResources().getColor(R.color.white));
		
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)textViewCont.getLayoutParams();
		
		if(	((SpaApplication)((Activity)context).getApplication()).getLocalization() != null && 
			((SpaApplication)((Activity)context).getApplication()).getLocalization().equals("he")) {
			String[] dataParts;
			dataParts = row.getName().split("-");
			String currentNumber = null;
			String currentName = null;

			if(dataParts != null && dataParts.length > 1) {
				currentNumber = dataParts[0];
				currentName = dataParts[1];
			}

			if(currentName != null) {
				zoneName.setText(currentName.trim());
			}else {
				zoneName.setText(row.getName().trim());
			}
			if(currentNumber != null) {
				
//				textViewExt.setText("-");
//				textViewExtTwo.setText(currentNumber.trim());
			}
			
			params.gravity = Gravity.RIGHT;
		}else {
			params.gravity = Gravity.LEFT;
			zoneName.setText(row.getName().trim());
		}
		
		if(row.getIsOpen() )
		{
			open.setVisibility(View.VISIBLE);
		}
		else
		{
			open.setVisibility(View.INVISIBLE);
		}
		
		if(row.getIsAlarm())
		{
			alarm.setVisibility(View.VISIBLE);
		}
		else
		{
			alarm.setVisibility(View.INVISIBLE);
		}
		
		
		if(row.getIsBypass() )
		{
			bypass.setVisibility(View.VISIBLE);
		}
		else
		{
			bypass.setVisibility(View.INVISIBLE);
		}
		
		
		/*if(row.type==TYPES_ZONE_ROWS.Header){
			rowView.setBackgroundColor(R.color.black);
			textView.setTextColor(R.color.zones_header);
		}else{
			rowView.setBackgroundColor(R.color.list_item_color);
			textView.setTextColor(R.color.white);
		}*/
		
		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	
 
}
