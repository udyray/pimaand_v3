package com.pima.apps.ipima.v3.util;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.views.SystemFault;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FaultsArrayAdapter extends ArrayAdapter<SystemFault>{
	private Context context;
	private SystemFault[] faults;
	
	public FaultsArrayAdapter(Context context, SystemFault[] objects) {
		super(context,R.layout.faults_list_item, objects);
		this.context=context;
		this.faults=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = null;

		Activity activityContext = (Activity)context;

		SystemFault fault = faults[position];
		
		if(fault.getIsEmpty()) {
			rowView = inflater.inflate(R.layout.empty_faults_item, parent, false);
			TextView text = (TextView) rowView.findViewById(R.id.empty_faults);
			text.setText(((SpaApplication)activityContext.getApplication()).getLocalizedString("No-Fault"));
			
			return rowView;
		}else {
			rowView = inflater.inflate(R.layout.faults_list_item, parent, false);
		}

		TextView text = (TextView) rowView.findViewById(R.id.faultTV);
		text.setText(fault.getText());

		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	public void updateData(SystemFault[] objects)
	{
		faults = objects;
	}

}
