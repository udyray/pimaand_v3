package com.pima.apps.ipima.v3.util;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.activity.NewLoginActivity;
import com.pima.apps.ipima.v3.fragment.BaseFragment;
import com.pima.apps.ipima.v3.fragment.NotificationViewFragment;
import com.pima.apps.ipima.v3.fragment.NotificationViewNoLoginFragment;
import com.pima.apps.ipima.v3.views.SystemNotification;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationsArrayAdapter extends ArrayAdapter<SystemNotification>{
	private boolean isLogin;
	private Context context;
	private SystemNotification[] notifications;
//	private SparseArray<NotificationType> notificationsTypes;

	public NotificationsArrayAdapter(	Context context, 
										SystemNotification[] objects,
										boolean pIsLogin) {
		super(context,R.layout.notification_list_item, objects);
		this.context=context;
		
		if(objects == null || objects.length==0)
		{
			objects = new SystemNotification[1];
			objects[0] = new SystemNotification("", -1, "", null, null);
			objects[0].setIsEmpty(true);
		}
		this.notifications = objects;
		
//		this.notificationsTypes = types;
		
		isLogin = pIsLogin;
	}
	
	public void updateData(SystemNotification[] objects)
	{
		this.notifications = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = null;
		
		final SystemNotification notification=notifications[position];
		if(notification.getIsEmpty()) {
			rowView = inflater.inflate(R.layout.empty_notification_item, parent, false);
			TextView text = (TextView) rowView.findViewById(R.id.emptyNotifications);
			text.setText(((SpaApplication)((Activity)context).getApplication()).getLocalizedString("NoNotifications"));
			return rowView;

		}
		
		if(null == rowView)
		{ 
			rowView = inflater.inflate(R.layout.notification_list_item, parent, false);
		}
//		TextView date = (TextView) rowView.findViewById(R.id.dateTV);
//		date.setText(notification.getFormatedDate());
		
//		TextView time = (TextView) rowView.findViewById(R.id.timeTV);
//		time.setText(notification.getFormatedTime());
		
//		TextView type = (TextView) rowView.findViewById(R.id.notificationType);
//		NotificationType nt = notificationsTypes.get(notification.getType());
//		type.setText(((SpaApplication)((Activity)context).getApplication()).getLocalizedString(nt.getName()));
		
		TextView text = (TextView) rowView.findViewById(R.id.notificationTV);
		text.setText(notification.getMessage());

		LinearLayout expand = (LinearLayout)rowView.findViewById(R.id.expand);
		if(notification.getImagesUrl() == null || notification.getImagesUrl().length==0)
		{
			expand.setVisibility(View.INVISIBLE);
		}
		else
		{
			expand.setVisibility(View.VISIBLE);
		}
		OnClickListener ocl = new OnClickListener() {
			 
			@Override
			public void onClick(View v) {
				BaseFragment fragment = null;
				if(isLogin)
				{
					fragment = new NotificationViewFragment();
				}
				else
				{
					fragment = new NotificationViewNoLoginFragment();
				}
				
				Bundle args = new Bundle();
				args.putString(NotificationViewFragment.EXTRA_NOTIFICATION_TITLE, notification.getMessage());
				args.putStringArray(NotificationViewFragment.EXTRA_NOTIFICATION_IMAGES, notification.getImagesUrl());
				fragment.setArguments(args);
				
				if(isLogin)
				{
					((MainScreenActivity)context).swapFragments(fragment);
				}
				else
				{
					((NewLoginActivity)context).swapFragments(fragment);
				}
				
			}
		};
		
		expand.setOnClickListener(ocl);
		
		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
}
