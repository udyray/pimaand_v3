package com.pima.apps.ipima.v3.util;

import java.util.List;

import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class BypassZonesArrayAdapter extends BaseAdapter {
	
	public static class ZonesRow extends Object {
		public int id;
		public String name;
		public boolean isBypasses;
		
		public ZonesRow(int id, String name, boolean isBypasses) {
			super();
			this.id = id;
			this.name = name;
			this.isBypasses = isBypasses;
		}
		
	}
	
	class ImageViewClickListener implements OnClickListener 
	{
		int position;
		int zoneNumber;
		boolean isBypassed;
		public ImageViewClickListener(int position, int zoneNumber, boolean isBypassed)
		{
			this.position = position;
			this.zoneNumber = zoneNumber;
			this.isBypassed = isBypassed;
		}

		public void onClick(View v) {
			((MainScreenActivity)context).bypassZonesFragment.cancelCurrentUpdate();
			
			doBypass(zoneNumber, !isBypassed, objects);
		}
	}
	
	List<ZonesRow> objects;
	Context context;
	
	public BypassZonesArrayAdapter(Context context, List<ZonesRow> objects){
		super();
		this.context = context;
		this.objects = objects;
	}

	public void updateData(List<ZonesRow> objects)
	{
		this.objects = objects;
	}
	
	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public Object getItem(int arg0) {
		if(arg0 < objects.size())
		{
			return objects.get(arg0);
		}
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		if(arg0 < objects.size())
		{
			return objects.get(arg0).id;
		}
		return 0;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		LinearLayout rowView = (LinearLayout)convertView;
		ZonesRow row = objects.get(position);
		if(rowView == null)
		{
			rowView = (LinearLayout)mInflater.inflate(R.layout.bypass_zones_list_item, null);
		}
		
		LinearLayout textViewCont = (LinearLayout)rowView.findViewById(R.id.textViewCont);
		if(position%2 == 0)
		{
			textViewCont.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bypass_bg_odd));
		}
		else
		{
			textViewCont.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bypass_bg_even));
		}
		
		TextView textView = (TextView)rowView.findViewById(R.id.listTextView);		
		TextView textViewExtTwo = (TextView)rowView.findViewById(R.id.listTextViewExTwo);
		
		OnClickListener ocl = new ImageViewClickListener(position, row.id, row.isBypasses);
		ImageView bypassImage = (ImageView)rowView.findViewById(R.id.bypassImage);
		bypassImage.setOnClickListener(ocl);
		
//		String zone = ((SpaApplication)((Activity)context).getApplication()).getLocalizedString("label_zone_short");
		
		String currentNumber = "" + row.id;
		String currentName = row.name.trim();

		textView.setText(currentNumber.trim());
		textView.setTypeface(null, Typeface.BOLD);
		textViewExtTwo.setText(currentName.trim());
		
		if(row.isBypasses)
		{
			bypassImage.setImageResource(R.drawable.bypass_on);
		}
		else
		{
			bypassImage.setImageResource(R.drawable.bypass_off);
		}
		
		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	/**
	 * 
	 * @param zoneNumber
	 * @param isBypassed
	 * @param objects
	 */
	private void doBypass(int zoneNumber, boolean isBypassed, List<ZonesRow> objects)
	{
		int[] ids = new int[objects.size()];
		boolean[] isByPassed = new boolean[objects.size()];
		
		for(int i=0; i<objects.size(); i++)
		{
			ZonesRow zr = objects.get(i);
			ids[i] = zr.id;
			if(zr.id != zoneNumber)
			{
				isByPassed[i] = zr.isBypasses;
			}
			else
			{
				isByPassed[i] = isBypassed;
			}
		}
		((MainScreenActivity)context).bypassZone(ids, isByPassed); 
		
	}

}

	