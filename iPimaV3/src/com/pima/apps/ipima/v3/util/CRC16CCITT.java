package com.pima.apps.ipima.v3.util;

public class CRC16CCITT {
	
	public static int of(String data) {
        // byte[] testBytes = "123456789".getBytes("ASCII");
		return CRC16CCITT.of(data.getBytes());
	}
	
	public static int of(byte[] bytes) {
		int crc = 0x0000;          // initial value - xModem 0x0000
        int polynomial = 0x1021;   // 0001 0000 0010 0001  (0, 5, 12) 
		
		
		for (byte b : bytes) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b   >> (7-i) & 1) == 1);
                boolean c15 = ((crc >> 15    & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= polynomial;
             }
        }

        crc &= 0xffff;
        
        return crc;
	}
}