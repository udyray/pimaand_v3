package com.pima.apps.ipima.v3.util;


import com.pima.apps.ipima.v3.R;
import com.pima.apps.ipima.v3.SpaApplication;
import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.views.Lookin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class LookinArrayAdapter extends BaseAdapter {
	
	Lookin[] objects;
	Context context;
	
	public LookinArrayAdapter(Context context, Lookin[] objects){
		super();
		this.context = context;
		this.objects = objects;
	}

	@Override
	public int getCount() {
		return objects.length;
	}

	@Override
	public Object getItem(int position) {
		
		if(position < objects.length)
		{
			return objects[position];
		}
		
		return null;
	}

	@Override
	public long getItemId(int position) {
		
		Lookin sz = (Lookin)getItem(position);
		if(null != sz)
		{
			return sz.getNumber();
		}
		return 0;
	}

	public void updateData(Lookin[] objects)
	{
		this.objects = objects;
	}
	
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		FrameLayout rowView = (FrameLayout)convertView;
		final Lookin row = objects[position];
		if(rowView == null)
		{
			rowView = (FrameLayout)mInflater.inflate(R.layout.list_item, null);
		}
		
		TextView listTextView = (TextView)rowView.findViewById(R.id.listTextView);	
		

		rowView.setBackgroundColor(context.getResources().getColor(R.color.list_item_color));
		listTextView.setTextColor(context.getResources().getColor(R.color.white));
		
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)listTextView.getLayoutParams();
		
		if(	((SpaApplication)((Activity)context).getApplication()).getLocalization() != null && 
			((SpaApplication)((Activity)context).getApplication()).getLocalization().equals("he")) {
			
			params.gravity = Gravity.RIGHT;
		}else {
			params.gravity = Gravity.LEFT;
			
		}
		listTextView.setLayoutParams(params);
		
		
		if(row.getZoneNumber() > 0)
		{
			listTextView.setText(row.getZoneNumber() + " - " + row.getZoneName().trim());
			
			listTextView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((MainScreenActivity)context).lookinZone(row.getZoneNumber());
				}
			});
		}
		else
		{
			listTextView.setText(row.getZoneName());
		}
		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	
 
}
