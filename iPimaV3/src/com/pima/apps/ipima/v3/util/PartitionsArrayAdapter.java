package com.pima.apps.ipima.v3.util;

import java.util.List;

import com.pima.apps.ipima.v3.R;

import com.pima.apps.ipima.v3.activity.MainScreenActivity;
import com.pima.apps.ipima.v3.views.PanelSystem.TYPES_SYSTEM_STATUS;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class PartitionsArrayAdapter extends BaseAdapter {
	
	public static class Partition extends Object {
		public int number;
		public String name;
		public TYPES_SYSTEM_STATUS state;
		
		public Partition(int id, TYPES_SYSTEM_STATUS state) {
			super();
			this.number = id;
			this.state = state;
		}
		
		public void setName(String name)
		{
			this.name = name;
		}
	}
	
	class RowClickListener implements OnClickListener 
	{
		Partition partition;
		public RowClickListener(Partition partition)
		{
			this.partition = partition;
		}

		public void onClick(View v) {
			openPatition(partition);
		}
	}
	
	List<Partition> objects;
	Context context;
	
	public PartitionsArrayAdapter(Context context, List<Partition> objects){
		super();
		this.context = context;
		this.objects = objects;
	}

	public void updateData(List<Partition> objects)
	{
		this.objects = objects;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return objects.size();
	}

	@Override
	public Object getItem(int arg0) {
		if(arg0 < objects.size())
		{
			return objects.get(arg0);
		}
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		if(arg0 < objects.size())
		{
			return objects.get(arg0).number;
		}
		return 0;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		LinearLayout rowView = (LinearLayout)convertView;
		Partition row = objects.get(position);
		if(rowView == null)
		{
			rowView = (LinearLayout)mInflater.inflate(R.layout.partition_list_item, null);
		}
		
		if(position==0) 
		{
			rowView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.part_list_top));
		}
		else if(position==objects.size()-1)
		{
			rowView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.part_list_bottom));
		}
		else
		{
			rowView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.part_list_body));
		}
		OnClickListener ocl = new RowClickListener(objects.get(position));
		rowView.setOnClickListener(ocl);
		
		TextView partitionNameView = (TextView)rowView.findViewById(R.id.partitionName);
		if(row.number != -1)
		{
//			String part = ((SpaApplication)((Activity)context).getApplication()).getLocalizedString("label_partition_short_m");
//			String number = part + row.number;
//			partitionNameView.setText( number + ": " + row.name);
			partitionNameView.setText(row.name);
		}
		else
		{
			partitionNameView.setText(row.name);
		}
		ImageView stateImage = (ImageView)rowView.findViewById(R.id.state);
		if(row.state.equals(TYPES_SYSTEM_STATUS.Disarm))
		{
			stateImage.setImageResource(R.drawable.lock_small_open);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.Full))
		{
			stateImage.setImageResource(R.drawable.lock_small_closed);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.Home1))
		{
			stateImage.setImageResource(R.drawable.home1);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.Home2))
		{
			stateImage.setImageResource(R.drawable.home2);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.Home3))
		{
			stateImage.setImageResource(R.drawable.home3);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.Home4))
		{
			stateImage.setImageResource(R.drawable.home4);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.SaturdayOff))
		{
			stateImage.setImageResource(R.drawable.saturday_off);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.SaturdayOn))
		{
			stateImage.setImageResource(R.drawable.saturday_on);
		}
		else if(row.state.equals(TYPES_SYSTEM_STATUS.Special))
		{
			stateImage.setImageResource(R.drawable.lock_small_open);
		}
		
		return rowView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	/**
	 * 
	 * @param partition
	 */
	private void openPatition(Partition partition)
	{
		if(partition.number == -1)
		{
			((MainScreenActivity)context).fragmentSystemState.setPartition(-1, null);
		}
		else
		{
			((MainScreenActivity)context).fragmentSystemState.setPartition(partition.number, partition.name);
		}
		((MainScreenActivity)context).swapFragments(((MainScreenActivity)context).fragmentSystemState);
	}

}

	